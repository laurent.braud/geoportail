from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect
from climap import query, chartquery
from climap.models import Metric
from mpl_toolkits.basemap import Basemap
from math import floor, ceil
#
# import matplotlib
# matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
from .forms import MapForm, PublicMapForm, FeedBack
import io
import base64
from matplotlib.colors import LinearSegmentedColormap

def map_plot(ax, params, model_list):
    qry = query.RasterQuery(params, model_list)
    rawim = qry.execute(output='dump')
    rmeta = qry.rmeta

    # flips image vertically because matplotlib plots from bottom to top
    # (opposite of postgis st_aspng())
    data = np.flip(np.array(rawim, dtype=np.float), 0)

    # postgis sometimes sends NaN rows and cols we have to remove
    llxoff = urxoff = llyoff = uryoff = 0
    if np.prod(np.isnan(data[0])):
        data = data[1:]
        llyoff = .5
    if np.prod(np.isnan(data[-1])):
        data = data[:-1]
        uryoff = .5
    if np.prod(np.isnan(data[:,0])):
        data = np.delete(data, 0, 1)
        llxoff = .5
    if np.prod(np.isnan(data[:,-1])):
        data = np.delete(data, -1, 1)
        urxoff = .5

    map = Basemap(
        resolution='l', # options are c (crude, the default), l (low), i (intermediate), h (high), f (full)
        llcrnrlon = rmeta[0] - rmeta[4]/2 + llxoff,
        urcrnrlat = rmeta[1] - rmeta[5]/2 - uryoff,
        urcrnrlon = rmeta[0] + (rmeta[2]-.5) * rmeta[4] - urxoff,
        llcrnrlat = rmeta[1] + (rmeta[3]-.5) * rmeta[5] + llyoff,
    )
    map.drawcoastlines(linewidth=.5)
    map.drawcountries()

    vmin, vmax = floor(qry.vmin), ceil(qry.vmax)
    # very slow !
    for x in range(data.shape[0]):
        for y in range(data.shape[1]):
            if data[x][y] < vmin or data[x][y] > vmax:
                data[x][y] = np.nan

    cmap = LinearSegmentedColormap(qry.cmap.name, qry.cmap.code2colormap())

    if params.get('output', 'pixel') == 'pixel':
        x = np.linspace(map.llcrnrx, map.urcrnrx, data.shape[1] + 1)
        y = np.linspace(map.llcrnry, map.urcrnry, data.shape[0] + 1)
        xx, yy = np.meshgrid(x, y)
        cm = map.pcolormesh(xx, yy, data, vmin=qry.vmin, vmax=qry.vmax, cmap=cmap)
    else:
        x = np.linspace(map.llcrnrx, map.urcrnrx, data.shape[1])
        y = np.linspace(map.llcrnry, map.urcrnry, data.shape[0])
        xx, yy = np.meshgrid(x, y)
        cm = map.contourf(xx, yy, data, vmin=qry.vmin, vmax=qry.vmax, cmap=cmap)
    if params.get('change', None) :
        cbar = map.colorbar(cm, location='right', format=ticker.FuncFormatter(
            lambda tick,_: '{:+{prec}}{unit}'.format(tick,prec=('.3','.0f')[tick-int(tick)==0],unit=qry.unit)))
    else:
        cbar = map.colorbar(cm, location='right', format=ticker.FormatStrFormatter('%d {}'.format(qry.unit)))
    #cbar.set_label(qry.unit)
    cbar.ax.tick_params(labelsize=8)
    ax.set_title(query.split_title(qry.title), fontsize=12)


def plot_all(params, model_list):
    """Plots all graphics into a figure."""
    getdisplay = params.get('export', None)
    if getdisplay:
        displays = list(map(int, getdisplay))
    else:
        displays = ( params.get('display_map', True), params.get('display_month', True), params.get('display_year', True))
    indexes = [ sum(displays[:i]) for i in (1,2,3) ]
    nbfig = indexes[2]
    fig = plt.figure(figsize=(8, 6*nbfig))
    scenarii = ( params.get('compare', 'scenarii') == 'scenarii' )

    if displays[0]:
        ax = plt.subplot(nbfig, 1, indexes[0])
        map_plot(ax, params, model_list)
    if displays[1] and Metric.objects.get(id=params['metric_id']).has_month :
        ax = plt.subplot(nbfig, 1, indexes[1])
        if scenarii:
            chartquery.month_by_scenarii(params, model_list, output='plt', ax=ax)
        else:
            chartquery.month_by_models(params, model_list, output='plt', ax=ax)
    if displays[2] :
        ax = plt.subplot(nbfig, 1, indexes[2])
        if scenarii:
            chartquery.year_by_scenarii(params, model_list, output='plt', ax=ax)
        else:
            chartquery.year_by_models(params, model_list, output='plt', ax=ax)

    if params.get('format', 'png') == 'svg':
        buf = io.StringIO()
    else:
        buf = io.BytesIO()

    fig.savefig(buf, format=params.get('format', 'png'),bbox_inches='tight', dpi=params.get('dpi', 100))
    buf.seek(0)
    plt.close(fig)
    return buf

def plot_all_pdf(params, model_list):
    """Pdf version of plot_all."""
    getdisplay = params.get('export', None)
    if getdisplay:
        displays = list(map(int, getdisplay))
    else:
        displays = ( params.get('display_map', True), params.get('display_month', True), params.get('display_year', True))
    scenarii = ( params.get('compare', 'scenarii') == 'scenarii' )
    buf = io.BytesIO()
    pp = PdfPages(buf)

    if displays[0]:
        fig, ax= plt.subplots(figsize=(11.69,8.27), dpi=100) # == A4 landscape format
        map_plot(ax, params, model_list)
        pp.savefig(fig)
    if displays[1] and Metric.objects.get(id=params['metric_id']).has_month  :
        fig, ax = plt.subplots(figsize=(11.69,8.27), dpi=100)
        if scenarii:
            chartquery.month_by_scenarii(params, model_list, output='plt', ax=ax)
        else:
            plt.subplots_adjust(right=.8)
            chartquery.month_by_models(params, model_list, output='plt', ax=ax)
        pp.savefig(fig)
    if displays[2] :
        fig, ax = plt.subplots(figsize=(11.69,8.27), dpi=100)
#        fig, ax = plt.subplots()
        if scenarii:
            chartquery.year_by_scenarii(params, model_list, output='plt', ax=ax)
        else:
            plt.subplots_adjust(right=.8)
            chartquery.year_by_models(params, model_list, output='plt', ax=ax)
        pp.savefig(fig)
    pp.close()
    buf.seek(0)
    return buf


def image(request, extension, model_list=None):
    model_list = model_list or request.GET.getlist('m')
    if extension == 'pdf':
        plot = plot_all_pdf(request.GET, model_list)
        return HttpResponse(plot.getvalue(), content_type="application/" + extension)
    plot = plot_all(request.GET, model_list)
    return HttpResponse(plot.getvalue(), content_type="image/"+extension)


def index(request):
    if request.method == 'POST':
        form = MapForm(request.POST) if request.user.is_authenticated else PublicMapForm(request.POST)
        context = { 'form' : form.as_ul() }
        if form.is_valid():
            params = form.cleaned_data
            params['scenario_id'] = params['scenario'].id
            params['metric_id'] = params['parameter'].id
            if params['zone']:
                params['zone'] = params['zone'].name
            model_list = [ cm.id for cm in params['climate_model'] ]

            format = params.get('format', 'png')
            if format != 'png':
                if format == 'pdf':
                    plot = plot_all_pdf(params, model_list)
                    return HttpResponse(plot.getvalue(), content_type="application/" + format)
                plot = plot_all(params, model_list)
                return HttpResponse(plot.getvalue(), content_type="image/" + format)
            else:
                bla = str(base64.b64encode(plot_all(params, model_list).read()))
                context['png'] = 'data:image/png;base64,'+bla[2:-1]

    else :
        if request.user.is_authenticated:
            context = {'form': MapForm().as_ul()}
        else:
            context = {'form': PublicMapForm().as_ul()}

    return render(request, 'climap/basic.html', context)


def feedback(request):
    return render(request, 'climap/feedback.html', { 'f': FeedBack() })

def feedbacksubmit(request):
    def get_client_ip(request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip

    if request.method == 'POST':
        form = FeedBack(request.POST)
        if form.is_valid():
#            form.ip = get_client_ip(request)
            form.save()
    return HttpResponseRedirect(reverse('main'))


def guide(request):
    return render(request, 'climap/guide.html')