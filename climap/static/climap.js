
/* TOP MENU */

function allModels(toggle){
    var ml = document.getElementsByClassName('modelCheck');
    for (i=0;i<ml.length;i++)
        ml[i].checked = toggle;
    document.getElementById('modelsNb').innerHTML = toggle?ml.length:0;
}

function updateModelList(){
    var ml = document.getElementsByClassName('modelCheck'),
        nb = 0;
    for (i=0;i<ml.length;i++)
        if (ml[i].checked) nb++;
    document.getElementById('modelsNb').innerHTML = nb;
}

function updateDrop(prefix, s){
    document.getElementById(prefix+'Name').innerHTML = s;
    document.getElementById('updateButton').classList.add('button-wait');
}

function updateDropNoUpdate(prefix, s){
    /* same as updateDrop but do not redding 'update' button */
    document.getElementById(prefix+'Name').innerHTML = s;
    }

var months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul',
      	       'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    fullmonths = [ 'Janunary', 'February', 'March', 'April', 'May', 'June', 'July',
      	       'August', 'September', 'October', 'November', 'December'];

function updateTime(form){
    document.getElementById('updateButton').classList.add('button-wait');
    var time = form.years.value;
    if (form.month.value >= 0)
        time = months[form.month.value] + ' ' + time;
    document.getElementById('timeDisplay').innerHTML = time;
}

/* MAP SETUP */

var NasaLayer = new ol.layer.Tile({
    source: new ol.source.XYZ({
	attribution: 'Nasa Imagery',
	url: "https://map1{a-c}.vis.earthdata.nasa.gov/wmts-webmerc/" +
            "BlueMarble_ShadedRelief_Bathymetry/default/%7BTime%7D/" +
            "GoogleMapsCompatible_Level8/{z}/{y}/{x}.jpg"
    }),
    opacity: 1,
    zIndex: -1,
    visible: false
});

var ESRILayer = new ol.layer.Tile({
    visible: true,
    source: new ol.source.XYZ()
    /*{
        attributions: [ new ol.Attribution({
            html: 'Tiles &copy; <a href="http://services.arcgisonline.com/ArcGIS/' +
		'rest/services/World_Topo_Map/MapServer">ArcGIS</a>'
	}) ],
        url: 'http://server.arcgisonline.com/ArcGIS/rest/services/' +
            'World_Topo_Map/MapServer/tile/{z}/{y}/{x}'
    })*/
});

var OSMLayer = new ol.layer.Tile({
    source: new ol.source.OSM(),
    opacity: 1,
    zIndex: -1,
    visible: false
});

var imageLayer = new ol.layer.Image({});

var attribution = new ol.control.Attribution({
    collapsible: false
});

var iconStyle = new ol.style.Style({
    image: new ol.style.Circle({
        radius: 7,
        fill: new ol.style.Fill({
            color: '#ffcc33'
        }),
        stroke: new ol.style.Stroke({
	    color: 'black',
	    width: 1
	    })
    })
});
// Markers are complicated in OL3
// see https://openlayers.org/en/latest/examples/icon.html?q=marker
var iconFeature = new ol.Feature({
    geometry: new ol.geom.Point([0,0]),
    name : 'probe'
});

iconFeature.setStyle(iconStyle);

var probeLayer = new ol.layer.Vector({
    source: new ol.source.Vector({
	features: [iconFeature]
    }),
    zIndex: 2,
    visible: false
});


/* confidence zone style : stroked
   code adapted from 
   https://openlayers.org/en/latest/examples/canvas-gradient-pattern.html 
*/
var canvas = document.createElement('canvas');
var context = canvas.getContext('2d'),
    pixelRatio = ol.has.DEVICE_PIXEL_RATIO;
var pattern = (function() {
    canvas.width = 8 * pixelRatio;
    canvas.height = 8 * pixelRatio;
    context.beginPath();
    context.moveTo(0, 8*pixelRatio);
    context.lineTo(8*pixelRatio, 0);
    context.stroke();
    return context.createPattern(canvas, 'repeat');
}());
var confidenceStyle = new ol.style.Style({
    fill: new ol.style.Fill({color:pattern}),
    stroke: new ol.style.Stroke({
        color: '#333',
        width: 2
    })
});
var confidenceLayer = new ol.layer.Vector({
    zIndex:100,
    style:confidenceStyle,
    visible: false
})


var map = new ol.Map({
    controls: ol.control.defaults(),
    target: 'map',
    layers: [ ESRILayer, OSMLayer, NasaLayer,
	      imageLayer, probeLayer, confidenceLayer ],
    view: new ol.View({
        projection: 'EPSG:4326',
        center: [-15,14],
        //center: ol.proj.fromLonLat([37.41, 8.82]),
        zoom: 6,
        minZoom:3
    }),
    //controls: ol.control.defaults({attribution: false}).extend([attribution])
});
/*
  var scaleline = new ol.control.ScaleLine();
  map.addControl(scaleline);
*/
function changeBackground(ovname){
    OSMLayer.setVisible(ovname === 'OSM');
    NasaLayer.setVisible(ovname === 'Nasa');
    ESRILayer.setVisible(ovname === 'ESRI');
}

function pixelize(evt) {
    /* pixels instead of interpolation */
    evt.context.imageSmoothingEnabled = false;
    evt.context.webkitImageSmoothingEnabled = false;
    evt.context.ImageSmoothingEnabled = false;
    evt.context.msImageSmoothingEnabled = false;
}

map.on('precompose', pixelize);

/* ZONES */

var zoneStyle = new ol.style.Style({
    stroke: new ol.style.Stroke({
	color: 'rgb(100,100,100)',
	width: 2
    })
});

var selectedZoneStyle = new ol.style.Style({
    stroke: new ol.style.Stroke({
	color: 'black',
	width: 4
    })
});


function styleInit(feature){
    if (feature.getProperties().name === 'Senegal'){
        selectedZone = feature;
        return selectedZoneStyle;
    }
    return zoneStyle;
}

function fetchZones(div){
    if(parseInt(div)>0){
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                zones.setSource(new ol.source.Vector({
                    features: (new ol.format.GeoJSON()).readFeatures(this.responseText),
                    projection: 'EPSG:4326'
		}));
		zones.setVisible(true);

            }
        }
        xhr.open('GET','zones/'+div, true);
        xhr.send(null);
    } else zones.setVisible(false);
}

/* MAP PANEL */

function toggleMapPanel(x){
    panel = x.parentNode.children[0];
    if (panel.style.display != 'none'){
	panel.style.display = 'none';
	x.innerHTML = '<img src='+imgEdit+'>';
    } else {
	panel.style.display = 'block';
	x.innerHTML = '<img src='+imgArrowUpLeft+'>';
    }
}

/* MAP DRAW */

var drawSource = new ol.source.Vector({wrapX: false});

var drawVector = new ol.layer.Vector({
    source: drawSource,
    visible: false,
    style: selectedZoneStyle

});

map.addLayer(drawVector);

var draw = new ol.interaction.Draw({
    type : 'Polygon',
    source : drawSource
});

draw.on('drawstart', function(){
    drawSource.forEachFeature(function(f){drawSource.removeFeature(f)});
});

function fixedCoords(coords){
    return [coords[0].toFixed(3), coords[1].toFixed(3)];
}


draw.on('drawend', function(e){
    if (selectedZone) selectedZone.setStyle(null);
    probeLayer.setVisible(false);
    // so ugly
    coords = e.feature.getGeometry().getCoordinates()[0].map(fixedCoords);
    setSelect('custom', '['+coords.toString()+']');
    map.removeInteraction(draw);
});

/* SELECTION */

var selected = {
    type:'zone',
    value:'Senegal',
    print : function() {
    switch(String(this.type)){
        case "point":
            return '('+this.value[0]+','+this.value[1]+')';
        case "zone":
            return this.value;
        case "custom":
            return "custom zone";
        default:
            return "unknown zone";
    }
}
};

var selectedZone = false;

function setSelect(item, ref){
    document.getElementById('updateButton').classList.add('button-wait');
    selected.type=item;
    selected.value=ref;
    if (item === 'point')
        document.getElementById('selectedDisplay').innerHTML = selected.type+' ('+selected.value+')';
    else if (item === 'custom')
        document.getElementById('selectedDisplay').innerHTML = 'custom';
    else
        document.getElementById('selectedDisplay').innerHTML = selected.value;
    map.on('singleclick', updatePopup);
}

function selectPoint(coords){
    drawVector.setVisible(false);
    //    document.getElementById('message').innerHTML = 'Click on the map.';
    setSelect('point', fixedCoords(coords));
    iconFeature.setGeometry(new ol.geom.Point(coords));
    probeLayer.setVisible(true);
    if (selectedZone) {selectedZone.setStyle(null); selectedZone=false;}
}

function selectPointEvt(evt){
    selectPoint(evt.coordinate)
}

function zoneZoom(feature){
        drawVector.setVisible(false);
        probeLayer.setVisible(false);
        map.removeInteraction(hoverInteraction);
        if (selectedZone) selectedZone.setStyle(null);
        selectedZone = feature;
        feature.setStyle(selectedZoneStyle);
        setSelect('zone', feature.getProperties().name);
        map.getView().fit(feature.getGeometry().getExtent());
}

function selectZoneByName(name){
    name = name.toLowerCase();
    var fname, i,feats = zones.getSource().getFeatures();
    for(i=0;i<feats.length;i++){
        fname = feats[i].getProperties().name.toLowerCase();
        if (fname.includes(name)){
            zoneZoom(feats[i]);
            return;
            }
    }
    console.log('Not found.')
}


function selectZone(evt){
    var selectedFeature = false;
    map.forEachFeatureAtPixel(evt.pixel,
			      function(feature, layer) {if (layer == zones) selectedFeature = feature;});

    if (selectedFeature) {
        zoneZoom(selectedFeature);

    }
}

function setZoneSelection(){
    map.un('singleclick', updatePopup);
    map.addInteraction(hoverInteraction);
    map.once('singleclick', selectZone);
}

function drawCustomZone(){
    map.un('singleclick', updatePopup);
    drawVector.setVisible(true);
    map.addInteraction(draw);
}

/* ERROR PRINTING */

function cleanError(){
    document.getElementById('message').innerHTML = '';
}
function addError(text){
    document.getElementById('message').innerHTML += ' '+text;
}

/* CONFIDENCE DISPLAY */

var confidenceIsUpToDate = false;

function toggleConfidence(bool){
    confidenceLayer.setVisible(confidenceIsUpToDate && bool);
}


/* UPDATE FORM */

function showPlus(n){
    if (n>0) return '+'+n;
    return n;
}

function grayedHistory(canvas, area, g) {
    canvas.fillStyle = "rgba(200, 200, 200, .5)";
    var canvas_left_x = g.toDomXCoord(1950);
    var canvas_right_x = g.toDomXCoord(2005);
    var canvas_width = canvas_right_x - canvas_left_x;
    canvas.fillRect(canvas_left_x, area.y, canvas_width, area.h);
}

var monChart, yeaChart;

function getArgs(form, metric){
    /* Builds the GET request from the form to use in print* function. */
    var args= 'metric_id='+metric.id
        + '&scenario_id='+topForm.scenario.value;
    var i, ml = document.getElementsByClassName('modelCheck');
    for (i=0;i<ml.length;i++){
        if (ml[i].checked) args += '&m='+ml[i].value;
    }

    if ( form.month.value >= 0 && metrics[form.metric.value].hasmonth )
        args += '&month='+form.month.value;

    var years = form.years.value.split('-')
    args += '&yr='+years[0];
    if (years.length > 1)
        args += '&yrend='+years[1]
        + '&method='+form.method.value;

    args += '&compare='+form.compare.value;

    if (form.data.value!='data')
        args += '&change='+form.data.value;

    args += '&refYrs='+form.refYrs.value;

    if (selected.type === 'custom')
        args += '&poly='+selected.value;
    else if (selected.type==='zone') args += '&zone='+selected.value;
    else args += '&lon='+selected.value[0]+'&lat='+selected.value[1];

    if (form.area.value != 'near')
        args += '&area='+form.area.value;

    if (form.scale.value!='auto'){
        if (form.scale.value==='man')
            args += '&scale=man,'+form.manMinVal.value+','+form.manMaxVal.value;
        else
            args += '&scale='+form.scale.value;
    }

    return args;
}

function legendFormatter(data) {
  if (data.x == null) {
    // This happens when there's no selection and {legend: 'always'} is set.
    return '<br>' + data.series.map(function(series) {
        return "<span style='color:"+series.color+";'>"+series.dashHTML + ' ' + series.labelHTML+"</span>" }).join('<br>');
  }

    var html = this.getLabels()[0] + ': ' + data.xHTML;
  data.series.forEach(function(series) {

    if (!series.isVisible) return;
    var labeledData = series.labelHTML + ': ' + series.yHTML;
    if (series.isHighlighted) {
      labeledData = '<b>' + labeledData + '</b>';
    }
    html += "<br><span style='color:"+series.color+";'>"+ series.dashHTML + ' ' + labeledData+"</span>" ;
  });
  return html;
}

function printMonth(form, args, metric){
    /* Fetches the csv file needed by Dygraphs and draws month chart. */

    var titleMethod = (form.years.value.indexOf('-')!=-1) ? ' ('+form.method.value+')' : ''

    monTitle = 'Monthly '+metric.name+' in '+form.years.value+titleMethod+" in "+selected.print();
    if (form.data.value != 'data')
        monTitle += ' compared to reference '+form.refYrs.value;

    var unit = (form.data.value==='relative'?'%':metrics[metric.id].unit),
        plusunit = (form.data.value==='data'?'':'+'),
        compareScenarii = (form.compare.value === 'scenarii');

    var yaxiswidth = unit.length*15+40;

    monChart = new Dygraph(monthChart, 'month.csv?'+args,{
	legend: 'always',
	legendFormatter: legendFormatter,
	labelsDiv: 'monthLegend',
	labelsSeparateLines: true,
	title: monTitle,
	errorBars: compareScenarii,
	sigma : 1,
	series: colors,
	strokeWidth: 2,
	axes: {
            y: {
                axisLabelWidth: yaxiswidth,
                axisLabelFormatter: function(d) {
                    if (d>=0) return plusunit+d+unit;
                    else return d+unit;
                }
            },
            x: {
                axisLabelWidth: 70,
                pixelsPerLabel: 30,
                axisLabelFormatter: function(d) { return months[d] },
                valueFormatter: function(d) { return months[d] }
            }
        }
    });
    monChart.ready(function (){
        document.getElementById('monthLoader').style.display = 'none';
    });

}

function printYear(form, args, metric){
    /* Fetches the csv files needed by Dygraphs and draws month chart. */
    var yeaTitle = 'Evolution of '+metric.name+" in "+selected.print();
    if (form.month.value >= 0 && metrics[form.metric.value].hasmonth)
        yeaTitle += ' in '+fullmonths[form.month.value];
    if (form.data.value != "data")
        yeaTitle += ' compared to reference '+form.refYrs.value;


    var unit = (form.data.value==='relative'?'%':metrics[metric.id].unit),
        plusunit = (form.data.value==='data'?'':'+'),
        compareScenarii = (form.compare.value === 'scenarii');

    var yaxiswidth = unit.length*15+40;

    yeaChart = new Dygraph(yearChart, 'year.csv?'+args,{
	legend: 'always',
	legendFormatter: legendFormatter,
	labelsDiv: 'yearLegend',
	labelsSeparateLines: true,
	title: yeaTitle,
	errorBars: compareScenarii,
	sigma : 1,
	showRoller: true,
	rollPeriod: compareScenarii?1:10,
	series: colors,
	strokeWidth: 2,
	axes: { y : {
        axisLabelWidth: yaxiswidth,
        axisLabelFormatter: function(d) {
                    if (d>=0) return plusunit+d+unit;
                    else return d+unit;
                }
//	    axisLabelFormatter: function (d) { return d+unit; }
	}},
	underlayCallback: grayedHistory
    });

    yeaChart.ready(function (){
        document.getElementById('yearLoader').style.display = 'none';
        /*
        leg = document.getElementById('yearLegend');
        h = Math.min(16,document.getElementById('yearChart').clientHeight / leg.innerHTML.split('<br>').length);
        leg.style.fontSize = String(parseInt(h))+'px';
        */
    });
}

var mapArgs, mapUnit, mapPlusUnit;

function printRaster(form, args, metric){
    /* Fetches
       - the extent of the mapped raster in JSON form
       - the png file
       - some JSON metadata containing title and scale info among others.
    */

    /* Disallows cache storage of the raster for metadata update */
    var imageURL = 'img.png?n='+(new Date()).getTime() + '&' + args;
    mapArgs = args;

    var prequest = new XMLHttpRequest();
    prequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
	    var predata = JSON.parse(this.responseText);

	    var source = new ol.source.ImageStatic({
		url: imageURL,
		imageExtent: predata.extent,
		projection: 'EPSG:4326'
	    });

	    var request = new XMLHttpRequest();
	    request.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
		    var meta = JSON.parse(this.responseText);
		    if(meta.error) {
		        document.getElementById('message').innerHTML = 'Error : '+meta.error;
			imageLayer.setVisible(false);
		    }
		    else {
			document.getElementById("scale").style.display = 'block';
			document.getElementById("title").style.display = 'block';
			for(var i=0;i<10;i++){
			    var val = meta.scale[i];
			    if (form.data.value != 'data') {
			        val = showPlus(val);
			        mapPlusUnit = '+';
			    } else mapPlusUnit = '';
			    document.getElementById("scale"+i).innerHTML = val + ((i==0||i==9)?meta.unit:'');
			    mapUnit = meta.unit;
			}
			document.getElementById('manMinVal').value = meta.scale[9];
			document.getElementById('manMaxVal').value = meta.scale[0];
			document.getElementById("title").innerHTML = meta.title;
		    }
		}
	    };

	    source.on('imageloadend', function(){
	        document.getElementById('mapLoader').style.display='none';
		/* only fetch metadata after map calculation are done to get relative bounds */
		if(this.getState()==='ready'){
                    request.open('GET','metadata/'+metric.id, true);
                    request.send(null);
                }
	    });
	    source.on('imageloaderror', function(){
	        imageLayer.setVisible(false);
	        document.getElementById('mapLoader').style.display='none';
		if(this.getState()==='ready'){
		    request.open('GET','metadata/'+metric.id, true);
		    request.send(null);
                }
	    });

	    imageLayer.setSource(source);
	    imageLayer.setOpacity(form.imageOpacity.value/100);
	}};

    /* Since we cannot modify source extent in ol3, we have to send a first query to determine it. */
    var prelink = 'pre/?metric='+form.metric.value;
    if (form.area.value!='all'){
        prelink += '&area='+form.area.value;
        if (selected.type === 'custom')
            prelink += '&poly='+selected.value;
        else if (selected.type != 'point')
            prelink += '&zone='+selected.value;
        else
            prelink += '&lon='+selected.value[0]+'&lat='+selected.value[1];
    }

    prequest.open('GET', prelink, true);
    prequest.send(null);

    document.getElementById('scaleimg').src="scale/"+topForm.metric.value+".png";

}

function printConfidence(form, args, metric){
    var poly, request = new XMLHttpRequest();
    request.onreadystatechange = function() {
	if (this.readyState == 4 && this.status == 200) {
	    var text = this.responseText;
	    if (text != 'None'){
		poly = JSON.parse(text);
		var source = new ol.source.Vector({
		    features: [(new ol.format.GeoJSON()).readFeature(poly)]
		});
		confidenceLayer.setSource(source);
		confidenceLayer.setVisible(true);
		confidenceIsUpToDate = true;
	    } else {
		addError('No confidence.');
	    }
	} else if (this.readyState == 4) {
	    addError('No confidence.');
	}
    }
    var link = 'confidence/?'+args;
    request.open('GET',link, true);
    request.send(null);
}

function runForm(form){
    popupOverlay.setPosition(undefined);
    document.getElementById('updateButton').classList.remove('button-wait');
    cleanError();
    var metric = {id:topForm.metric.value,
                  name:document.getElementById('paramName').innerHTML};
    var args = getArgs(form, metric);

    if (form.showMap.checked){
        document.getElementById('mapLoader').style.display='block';
        imageLayer.setVisible(true);
        confidenceIsUpToDate = false;
        confidenceLayer.setVisible(false);
        printRaster(form, args, metric);
        if (form.confidence.checked && form.data.value != 'data')
            printConfidence(form, args, metric);
    }
    if (!metrics[metric.id].hasmonth){
        document.getElementById('monthChartContainer').style.display='none';
    }
    else if (form.showMonth.checked){
        document.getElementById('monthChartContainer').style.display='block';
        document.getElementById('monthLoader').style.display='block';
        printMonth(form, args, metric);
    }
    if (form.showYear.checked){
        document.getElementById('yearLoader').style.display='block';
        printYear(form, args, metric);
    }
}

/* EXPORT */

function download(filename, url) {
    var element = document.createElement('a');
    element.setAttribute('href', url);
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

function exportFile(form){
    var metric = {id:form.metric.value,
                  name:document.getElementById('paramName').innerHTML};
    var args = getArgs(form, metric),
        extension = form.exportFormat.value;

    args += '&export='+(form.exportMap.checked?'1':'0')+(form.exportMonth.checked?'1':'0')+(form.exportYear.checked?'1':'0')
        +'&format='+form.exportFormat.value
        +'&smoothing='+yeaChart.roller_.value;


    if (extension.endsWith('csv')||extension.endsWith('xls')){
        download(extension, extension+'?'+args);
    } else {
        download('export.'+extension, 'export.'+extension+'?'+args);
    }
}


/* POPUP
https://openlayers.org/en/latest/examples/popup.html
*/


      /**
       * Elements that make up the popup.
       */
      var popupContainer = document.getElementById('popup');
      var popupContent = document.getElementById('popup-content');
      var popupCloser = document.getElementById('popup-closer');


      /**
       * Create an overlay to anchor the popup to the map.
       */
      var popupOverlay = new ol.Overlay({
        element: popupContainer,
        autoPan: true,
        autoPanAnimation: {
          duration: 250
        }
      });
       map.addOverlay(popupOverlay);

      /**
       * Add a click handler to hide the popup.
       * @return {boolean} Don't follow the href.
       */
      popupCloser.onclick = function() {
        popupOverlay.setPosition(undefined);
        popupCloser.blur();
        return false;
      };

      /**
       * Add a click handler to the map to render the popup.
       */

      function updatePopup(evt) {
        var coordinate = evt.coordinate;
        //var hdms = ol.coordinate.toStringHDMS(ol.proj.toLonLat(coordinate));
        //var metricId = topForm.metric.value;
        //var unit = topForm.data.value === 'relative'?'%':metrics[metricId].unit;

        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                j = JSON.parse(this.responseText);

                var plusunit = (j.value>0)? mapPlusUnit:'',
                    c = fixedCoords(coordinate);
                popupContent.innerHTML = '<span class="helptext">lon '+c[0]+', lat '+c[1]+'</span>';
                popupContent.innerHTML += "<br>"+plusunit+j.value.toFixed(2)+' '+mapUnit;
                popupContent.innerHTML += '<br><a class="helptext" href="#" onclick="selectPoint(['+coordinate+'])">select this point</a>';
                popupOverlay.setPosition(coordinate);
            } else popupOverlay.setPosition(undefined);
        }

        xhr.open('GET','pinpoint/?plon='+evt.coordinate[0].toFixed(3)
                +'&plat='+evt.coordinate[1].toFixed(3)
                +'&'+mapArgs, true);//getArgs(topForm, metric), true);
        xhr.send(null);
      }

      map.on('singleclick', updatePopup);

var maplonlat = document.getElementById('maplonlat');

map.on('pointermove', function(evt){
    maplonlat.innerHTML = fixedCoords(evt.coordinate);
})