from . import models as m
from django.contrib.gis import geos
from django.contrib.gis.gdal import GDALRaster, DataSource
from netCDF4 import Dataset, num2date
# import gdal, ogr, os, osr
import numpy as np
import zipfile
from os.path import dirname
import os
import math
import pandas as pd


def country_codes(file):
    df = pd.read_csv(file)
    for z in m.Zone.objects.all():
        row = df[df['name']==z.name]
        if not row.empty:
            z.iso_code = row.values[0,2]
            z.save()


def run_polygons(div, verbose=True):
    """Loads a zip file containing an ESRI shapefile into the Postgis database. Deletes all files afterwards."""
    zf = zipfile.ZipFile(div.file.path, 'r')
    for f in zf.filelist:
        if f.filename.endswith('.shp'):
            break
    zdir = dirname(div.file.path)
    zf.extractall(zdir)
    lyr = DataSource(zdir+'/'+f.filename)[0]
    for poly in lyr:
        if isinstance(poly.geom.geos, geos.MultiPolygon):
            mpoly = poly.geom.geos
        else:
            mpoly = geos.MultiPolygon(poly.geom.geos)
        zone = m.Zone(division=div, name=poly.get(div.zonename), mpoly=mpoly)
        zone.save()
    for f in zf.filelist:
        os.remove(zdir+'/'+f.filename)
    zf.close()
    div.file.delete()


datatypes = {'int8': 1, 'uint16': 2, 'int16': 3, 'uint32': 4, 'int32': 5, 'float32': 6, 'float64': 7, }

def nc2raster(ncds, var, i, lon='lon', lat='lat', data=None, mask=False, autoflip=True):
    """Converts a netcdf band to GDAL raster.
    See http://django.readthedocs.io/en/latest/ref/contrib/gis/gdal.html?highlight=gdalraster"""
    if data is None:
        data = ncds[var][i]
    # copy = data.copy() # avoids "not C-contiguous" case
    # copy.mask |= mask

    if autoflip and ncds[lat][0].data < ncds[lat][-1].data :
        # In postgis, ST_AsPNG draws from TOP to BOTTOM, so we have to flip latitudes
        data = np.flip(data, 0)
    r = GDALRaster({
        'srid': 4326,
        'width': ncds[lon].shape[0],
        'height': ncds[lat].shape[0],
        'origin': [ ncds[lon][0].data-.25, max(ncds[lat])+.25 ], # upperleft
        'scale': [.5, -.5],
        'datatype': datatypes[ ncds[var].dtype.name ],
        'bands': [{
            'data' : data.filled(data.fill_value).copy(),
            # 'nodata_value': copy.fill_value,
            'nodata_value': float(ncds[var][i].fill_value),
        }],
    })
    return r


def scaleraster(xmin,xmax):
    """'fake' raster for metric scale display.
    See http://django.readthedocs.io/en/latest/ref/contrib/gis/gdal.html?highlight=gdalraster"""
    r = GDALRaster({
        'srid': 4326,
        'width': 1,
        'height': 10,
        'origin': [0, 0],
        'scale': [1,-1],
        'datatype': 7, # float32 pourquoi ?
        'bands': [{
            'data' : np.linspace(xmax, xmin, 10).reshape((1,10)),
        }],
    })
    return r


def findlonlatvar(variables):
    for x in variables:
        if x.find('lon')!=-1:
            lon = x
        elif x.find('lat')!=-1:
            lat = x
        elif x != 'time':
            var = x
    return lon, lat, var


def run_yearlyvalueset(vs, file):
    ds = Dataset(file)
    date0 = num2date(ds['time'][0], units=ds['time'].units).year
    lon, lat, var = findlonlatvar(ds.variables)
    vs.metric.yearvmax = max(math.ceil(np.max(ds[var])), vs.metric.yearvmax)
    vs.metric.yearvmin = min(math.floor(np.min(ds[var])), vs.metric.yearvmin)
    for i in range(ds[var].shape[0]):
        r = nc2raster(ds, var, i, lon=lon, lat=lat)
        m.YearlyValue.objects.create(value_set=vs, year=i+date0, raster=r)


def run_monthlyvalueset(vs, file):
    ds = Dataset(file)
    date0 = num2date(ds['time'][0], units=ds['time'].units).year
    i = 0
    lon, lat, var = findlonlatvar(ds.variables)
    vs.metric.monthvmax = max(math.ceil(np.max(ds[var])), vs.metric.monthvmax)
    vs.metric.monthvmin = min(math.floor(np.min(ds[var])), vs.metric.monthvmin)
    for yr in range(date0, date0+ds['time'].shape[0]//12):
        for mon in range(12):
            r = nc2raster(ds, var, i, lon=lon, lat=lat)
            m.MonthlyValue.objects.create(value_set=vs, year=yr, month=mon, raster=r)
            i += 1


METRICS = {
    'prmax': 'max precipitation',
    'prprainday': 'pr/rainday',
    'raindays30': '# of raining days (>30mm)',
    'raindays50': '# of raining days (>50mm)',
    'raindays': '# of raining days',
    'cumul': 'total rainfall',
    'tasmax': 'max temperature',
    'tasmin': 'min temperature',
    'raindays80': '# of raining days (>80mm)',
    'tasavr': 'average temperature',
    'hotdays': '# of hot days (>40°C)',
}

def run_metric(path, cm, shortcm):
    """Loads all available netcdf files corresponding to climate model.

    path : str
        directory of netcdf files
    cm: m.ClimateModel
        (django) climate model
    shortcm : str
        model name in files
    """
    for s in 'rcp26', 'rcp45', 'rcp85':
        scen = m.Scenario.objects.get(name=s)
        for met, mname in METRICS.items():
            filename = '_'.join([ 'calc', shortcm, scen.name, met])+'.nc'
            if os.path.isfile(os.path.join(path, 'mon_'+filename)):
                try:
                    vs = m.ValueSet.objects.get(model=cm, scenario=scen, metric=m.Metric.objects.get(name=mname))
                except m.ValueSet.DoesNotExist :
                    vs = m.ValueSet(model=cm,
                                    scenario=scen,
                                    metric=m.Metric.objects.get(name=mname))
                year_file = os.path.join(path, 'yr_' + filename)
                month_file = os.path.join(path, 'mon_' + filename)

                vs.yearlyvalue_set.all().delete()
                vs.monthlyvalue_set.all().delete()
                vs.save()
                run_monthlyvalueset(vs, month_file)
                run_yearlyvalueset(vs, year_file)
                print('Value set {} saved.'.format(vs))
            else:
                print('File mon_{} not found.'.format(filename))


model_list = {'ACCESS1.0': 'ACCESS1-0', 'ACCESS1.3': 'ACCESS1-3',
              'BCC_CSM 1.1': 'bcc-csm1-1', 'BCC_CSM 1.1(m)': 'bcc-csm1-1-m',
              'BNU-ESM': 'BNU-ESM', 'CanESM2': 'CanESM2',
              'CMCC-CESM': 'CMCC-CESM', 'CMCC-CM': 'CMCC-CM', 'CMCC-CMS': 'CMCC-CMS',
              'CNRM-CM5': 'CNRM-CM5', 'CSIRO-Mk3': 'CSIRO-Mk3-6-0', 'GFDL-CM3': 'GFDL-CM3',
              'GFDL-ESM2G': 'GFDL-ESM2G', 'GFDL-ESM2M': 'GFDL-ESM2M', 'INMCM4': 'inmcm4'}
model_list.update({m: m for m in ['HadGEM2-CC', 'HadGEM2-AO', 'HadGEM2-ES']})
model_list.update({m: m for m in ['IPSL-CM5A-LR', 'IPSL-CM5A-MR', 'IPSL-CM5B-LR']})
model_list.update({m: m for m in ['MIROC5', 'MIROC-ESM', 'MIROC-ESM-CHEM']})
model_list.update({m: m for m in ['MPI-ESM-LR', 'MPI-ESM-MR']})
model_list.update({m: m for m in ['MRI-CGCM3', 'MRI-ESM1', 'NorESM1-M']})


testmodel_list = {m: m for m in ['IPSL-CM5A-LR', 'MIROC5', 'HadGEM2-CC']}
testchem = {'MIROC-ESM-CHEM':'MIROC-ESM-CHEM'}


def run_metric_list(path, model_list):
    """Loads all available netcdf files corresponding to the given list.

    path : str
        directory of netcdf files
    list : dict
        str:str dictionary of model names
         - left name is contained in file name
         - right name is database name

    Netcdf files are of the form
        period_model_scenario_metric.nc
    where
        period : one of {yr, mon}
        model : as above
        scenario : one of rcp{26, 45, 85}
        metric : see METRICS variable
    """
    for name, shortcm in model_list.items():
        try:
            cm = m.ClimateModel.objects.get(name=name)
        except m.ClimateModel.DoesNotExist:
            cm = m.ClimateModel(name=name)
            cm.save()
        run_metric(path, cm, shortcm)




## CROP YIELD SECTION

def make_rdt_metrics():
    prod = { '': 'average production rate',
             ' F1': 'very low production rate',
             ' F2': 'low production rate',
             ' F3': 'high production rate',
             ' F4': 'very high production rate',
             }

    for cer in 'maize', 'millet', 'sorghum':
        for d in 90, 120:
            for w,long in prod.items():
                name = '{} {}{}'.format(cer,d,w)
                shortname = '{}{}{}'.format(cer[:3],d,w.strip())
                description = 'Yield of {}-days variety {}, {}.'.format(d, cer, long)
                met = m.Metric(name=name,
                               unit='kg/ha',
                               shortname=shortname,
                               description=description,
                               has_month=False,
                               download_only=(w == ''))
                met.save()

RDTDIR='/home/laurent/rendements'
# RDTALLDIR='/home/laurent/cleanedrdt'
RDTALLDIR='/home/laurent/allrdt'

def look_rdt():
    for f in os.listdir(RDTDIR):
        if not '.nc' in f:
            continue
        with Dataset(os.path.join(RDTDIR, f)) as ds:
            s = ds['F1'].shape
            #if s[1] != 37:
            #if '45' in f:
            print('{} > {} ({},{}/{},{})'.format(f.ljust(40), ds['F1'].shape, ds['lon'][0],ds['lat'][0],ds['lon'][-1],ds['lat'][-1]))


def XdayslessthanY(a, y=300, x=50):
    return np.sum(a>=y, axis=0) <= x

def run_rdt_yearlyvalueset(vs, file, var=None, ponder=None, minrdt=None, mask=None):
    with Dataset(file) as ds:
        date0 = num2date(ds['time'][0], units=ds['time'].units).year
        if var:
            for i in range(ds[var].shape[0]):
                r = nc2raster(ds, var, i, mask=mask)
                m.YearlyValue.objects.create(value_set=vs, year=i+date0, raster=r)
        else:
            arrays = [ ds['F'+str(i)][:] for i in range(1,5) ]
            # data = np.ma.masked_equal(np.ma.average(arrays, weights=ponder, axis=0), 0)
            data = np.ma.average(arrays, weights=ponder, axis=0)
            nope = XdayslessthanY(data, y=minrdt)
            for i in range(ds['F1'].shape[0]):
                r = nc2raster(ds, 'F1', i, data=data[i][:], nodata=data.fill_value, mask=nope)
                m.YearlyValue.objects.create(value_set=vs, year=i+date0, raster=r)
            return nope


def run_rdt_metrics(model_list=model_list):
    metrics = {
        'Sor': 'sorghum',
        'Mil': 'millet',
        'Mais': 'maize',
    }
    minrdt = {
        'Sor': 100,
        'Mil': 50,
        'Mais': 300,
    }
    f2name = {
        'F1': 'very low fert.',
        'F2': 'low fert.',
        'F3': 'medium fert.',
        'F4': 'optimum fert.',
    }
    for cm, shortcm in model_list.items():
        for s in 'rcp26', 'rcp45', 'rcp85':
            for variety in '90', '120':
                scen = m.Scenario.objects.get(name=s)
                for met, fullname in metrics.items():
                    filename = os.path.join(RDTALLDIR, '_'.join([ shortcm, s, 'RDT', met, variety])+'.nc')
                    if not os.path.isfile(filename):
                        print('File {} not found.'.format(filename))
                        continue
                    metricname = '{} {}'.format(fullname, variety)
                    print(metricname)
                    print(filename)
                    with Dataset(filename) as ds:
                        date0 = num2date(ds['time'][0], units=ds['time'].units).year

                        try:
                            vs = m.ValueSet.objects.get(model=m.ClimateModel.objects.get(name=cm),
                                                        scenario=scen,
                                                        metric=m.Metric.objects.get(name=metricname))
                            vs.yearlyvalue_set.all().delete()
                        except m.ValueSet.DoesNotExist:
                            vs = m.ValueSet(model=m.ClimateModel.objects.get(name=cm),
                                            scenario=scen,
                                            metric=m.Metric.objects.get(name=metricname))
                        vs.save()

                        # repartition key for maize
                        ponder = None if met != 'Mais' else (.27, .53, .16, .05)
                        arrays = [ ds[ff][:] for ff in f2name.keys() ]
                        data = np.ma.average(arrays, weights=ponder, axis=0)
                        nope = XdayslessthanY(data, y=minrdt[met])
                        data.mask |= nope
                        for i in range(ds['F1'].shape[0]):
                            r = nc2raster(ds, 'F1', i, data=data[i], mask=nope)
                            # r = nc2raster(ds, 'F1', i, data=data[i][:], nodata=data.fill_value, mask=nope)
                            m.YearlyValue.objects.create(value_set=vs, year=i + date0, raster=r)

                        # mask = run_rdt_yearlyvalueset(vs, file=filename, ponder=ponder, minrdt=minrdt[met])

                        print('Value set {} saved.'.format(vs))
                        for ff in 'F1', 'F2', 'F3', 'F4':
                            metricname = '{} {} {}'.format(fullname, variety, f2name[ff])
                            try:
                                vs = m.ValueSet.objects.get(model=m.ClimateModel.objects.get(name=cm),
                                                            scenario=scen,
                                                            metric=m.Metric.objects.get(name=metricname))
                                vs.yearlyvalue_set.all().delete()
                            except m.ValueSet.DoesNotExist:
                                vs = m.ValueSet(model=m.ClimateModel.objects.get(name=cm),
                                                scenario=scen,
                                                metric=m.Metric.objects.get(name=metricname))
                            vs.save()
                            for i in range(ds[ff].shape[0]):
                                r = nc2raster(ds, ff, i, mask=nope)
                                m.YearlyValue.objects.create(value_set=vs, year=i + date0, raster=r)

                            # run_rdt_yearlyvalueset(vs, file=filename, var=ff, mask=mask)
                            print('Value set {} saved.'.format(vs))
