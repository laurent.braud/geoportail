from django.contrib import admin
from . import models

@admin.register(models.Metric)
class MetricAdmin(admin.ModelAdmin):
    search_fields = ('name', 'category__name')
    fieldsets = (
        (None, {
            'fields': ('category', 'name', 'shortname', 'unit', 'description', 'colormap', 'caseof')
        }),
    )

@admin.register(models.ValueSet)
class ValueSetAdmin(admin.ModelAdmin):
    search_fields = ('model__name', 'scenario__name', 'metric__name')
    fieldsets = (
        (None, {
            'fields': ('model', 'scenario', 'metric')
        }),
        ('Files', {
            'fields': ('year_file', 'month_file')
        })
    )


@admin.register(models.Division)
class DivisionAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name',)
        }),
        ('File', {
            'fields': ('file', 'zonename'),
            'description': "A .zip file containing an ESRI shapefile, in which 'zonename' is "
                           "the field naming each zone."
        })
    )

admin.site.register(models.Scenario)
@admin.register(models.ClimateModel)
class CMAdmin(admin.ModelAdmin):
    search_fields = ('name',)
#admin.site.register(models.ClimateModel)


@admin.register(models.Zone)
class ZoneAdmin(admin.ModelAdmin):
    search_fields = ('name',)
#admin.site.register(models.Zone)

admin.site.register(models.MetricCategory)
admin.site.register(models.FeedBack)
admin.site.register(models.Colormap)
admin.site.register(models.ModelGroup)
