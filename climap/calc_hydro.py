from . import models as m
from django.db import connection
import numpy as np
import os
import csv

qry = "select st_dumpvalues(clip, 1), st_metadata(clip) from " \
      "(select year, month, st_clip(raster, ST_MakeEnvelope(-18, -21, 51, 21, 4326)) as clip " \
      "from climap_monthlyvalue where value_set_id = %s order by year, month) as foo;"

path = '/home/laurent/hydro/'

def fetch(metric):
    with connection.cursor() as cursor:
        qs = m.ValueSet.objects.filter(metric=metric).exclude(model__name='EWEMBI')
        for vs in qs:
            dir = os.path.join(path, metric.shortname, vs.scenario.name, vs.model.name)
            os.makedirs(dir, exist_ok=True)
            print("Fetching {} ...".format(vs))
            cursor.execute(qry, [vs.id])
            data = cursor.fetchall()

            # Metadata : upperleftx | upperlefty | width | height | scalex | scaley | skewx | skewy | srid | numbands
            meta = eval(data[0][1])
            ulx, uly = meta[0]+.25, meta[1]-.25

            # time, lat, lon
            zdata = np.array([ row[0] for row in data ]).round(3)
            for yr in range(150):
                with open(os.path.join(dir, "{}.txt".format(yr+1950)), 'w+') as f:
                    writer = csv.writer(f, delimiter='\t')
                    # moveaxis time to end
                    # now lat, lon, time
                    bla = np.moveaxis(zdata[yr*12:(yr+1)*12], 0, -1)
                    for lat in range(bla.shape[0]):
                        for lon in range(bla.shape[1]):
                            writer.writerow([uly-.5*lat, ulx+.5*lon]+list(bla[lat][lon]))