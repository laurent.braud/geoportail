from django.http import HttpResponse, JsonResponse
from . import models as m
from .rasterfuns import RAvg, Clip, Value, Png, RUnion, Algebra
from django.db.models import F, Q
import pandas as pd
from django.db.models.expressions import RawSQL
from django.contrib.gis.db import models
import numpy as np
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt

## POLYGON CALCULATION

# When "exact" polygon is needed for charts calc

def custom_poly(string):
    parray = eval(string)
    poly_wkb = 'POLYGON((' + ','.join(['{} {}'.format(parray[2 * i],
                                                      parray[2 * i + 1]) for i in range(len(parray) // 2)]) \
               + '))'
    # SRID is hardcoded to 4326
    return RawSQL("select ST_GeomFromWKB(%s::geometry, 4326)",
                  (poly_wkb,),
                  output_field=models.PolygonField())

def zone_poly(name):
    return RawSQL("select mpoly from climap_zone where name = %s",
                  (name,),
                  output_field=models.MultiPolygonField())


def exact_point(lon, lat):
    # bad fun name, sorry
    return RawSQL("select ST_SetSRID(ST_Point(%s,%s),4326)",
                  (lon, lat),
                  output_field=models.PointField())

# When bbox is needed ; currently only for map display

def custom_poly_box(string):
    parray = eval(string)
    poly_wkb = 'POLYGON((' + ','.join(['{} {}'.format(parray[2 * i],
                                                      parray[2 * i + 1]) for i in range(len(parray) // 2)]) \
               + '))'
    # SRID is hardcoded to 4326
    return RawSQL("select ST_Expand(ST_GeomFromWKB(%s::geometry, 4326),1,1)",
                  (poly_wkb,),
                  output_field=models.PolygonField())


def zone_poly_box(name):
    return RawSQL("select ST_Expand(mpoly,1,1) from climap_zone where name = %s",
                  (name,),
                  output_field=models.MultiPolygonField())

def point_neighbourhood(lon, lat):
    return RawSQL("select ST_Buffer(ST_SetSRID(ST_Point(%s,%s),4326),5)",
                  (lon, lat),
                  output_field=models.PolygonField())


## CHART SECTION

def smoothing(x,window):
    """Smoothing (or 'rolling') of a vector according to 2x a window.
    smoothing(x,1) == x
    smoothing(x,2)[i] = average of x[i], x[i-1], x[i+1]."""
    r = np.empty(x.shape)
    n = x.shape[0]
    for i in range(n):
        r[i] = np.average(x[max(0,i-window+1):min(n,i+window)])
    return r

def split_title(t, maxwidth=55):
    """Splits long titles because matplotlib does not by itself."""
    if len(t) < maxwidth:
        return t
    s, res = 0, ['']
    for w in t.split():
        if s + len(w) > maxwidth:
            s = len(w)
            res.append(w)
        else:
            s += len(w)
            res[-1] += ' ' + w
    return '\n'.join(res)


CM_NAME = 'value_set__model__name'
SC_NAME = 'value_set__scenario__name'

def annotate_val(params, mv):
    if 'poly' in params:
        poly = custom_poly(params['poly'])
        mvplus = mv.annotate(poly=poly).annotate(val=RAvg(Clip('raster', 'poly')))
    elif 'zone' in params:
        poly = zone_poly(params['zone'])
        mvplus = mv.annotate(poly=poly).annotate(val=RAvg(Clip('raster', 'poly')))
    else:
        point = exact_point(params.get('lon'), params.get('lat'))
        mvplus = mv.annotate(point=point).annotate(val=Value('raster', 'point'))
    return mvplus


# POINT VAL


def pinpoint(params, model_list=None):
    model_list = model_list or params.getlist('m')
    metric = m.Metric.objects.get(id=params['metric_id'])
    scenario = m.Scenario.objects.get(id=params['scenario_id'])
    # TODO : catch DoesNotExist
    yr = params.get('yr',2050)
    yrend = params.get('yrend', yr)
    vs = m.ValueSet.objects.filter(scenario=scenario,
                                   metric=metric,
                                   model_id__in=model_list)
    month = int(params.get('month', -1))
    if month == -1:
        v = m.YearlyValue.objects.filter(value_set__in=vs, year__range=(yr, yrend))
    else:
        v = m.MonthlyValue.objects.filter(value_set__in=vs, month=month, year__range=(yr, yrend))

    point = exact_point(params.get('plon'), params.get('plat'))
    vplus = v.annotate(point=point).annotate(val=Value('raster', 'point'))
    df = pd.DataFrame.from_records(vplus.values('val'))
    method = params.get('method', 'mean')
    res = {'mean': df.mean(), 'max': df.max(), 'min': df.min(), 'sum': df.sum()}[method]

    change = params.get('change', None)
    if change :
        ref_yr, ref_yrend = get_ref_yrs(params)
        if month == -1:
            v = m.YearlyValue.objects.filter(value_set__in=vs, year__range=(ref_yr, ref_yrend))
        else:
            v = m.MonthlyValue.objects.filter(value_set__in=vs, month=month, year__range=(ref_yr, ref_yrend))
        vplus = v.annotate(point=point).annotate(val=Value('raster', 'point'))
        dfpast = pd.DataFrame.from_records(vplus.values('val'))
        past = {'mean': dfpast.mean(), 'max': dfpast.max(), 'min': dfpast.min(), 'sum': dfpast.sum()}[method]

        if change == 'absolute':
            res = res - past
        elif change == 'relative':
            res = 100*(res - past)/past


    return JsonResponse({'value': res.val})




# MONTH CHARTS

def get_ref_yrs(params):
    refyrs = params.get('refYrs', '1950-1980')
    if '-' in refyrs:
        s = refyrs.split('-')
        return int(s[0]), int(s[1])
    return 1950, 1980


def month(params, vs, values):
    mv = m.MonthlyValue.objects.filter(value_set__in=vs)

    yr = params.get('yr',2050)
    yrend = params.get('yrend', yr)
    mvplus = annotate_val(params, mv.filter(year__range=(yr, yrend)))
    vals = mvplus.values('month', 'val', *values)

    df = pd.DataFrame.from_records(vals)
    method = params.get('method', 'mean')
    groo = df.groupby(['month'] + values)
    df = {'mean': groo.mean(), 'max': groo.max(), 'min': groo.min(), 'sum': groo.sum()}[method]

    change = params.get('change', None)
    if change :
        ref_yr, ref_yrend = get_ref_yrs(params)
        mvplus = annotate_val(params, mv.filter(year__range=(ref_yr, ref_yrend)))
        past_vals = mvplus.values('month', 'val', *values)
        # past_vals = mvplus.filter(year__range=(ref_yr, ref_yrend)).values('month', 'val', *values)

        past = pd.DataFrame.from_records(past_vals)
        groo = past.groupby(['month'] + values)
        past = {'mean': groo.mean(), 'max': groo.max(), 'min': groo.min(), 'sum': groo.sum()}[method]
        if change == 'absolute':
            df = df - past
        elif change == 'relative':
            df = 100*(df - past)/past
    return df


def month_by_models(params, model_list=None, output='csv', ax=None):
    model_list = model_list or params.getlist('m')
    metric = m.Metric.objects.get(id=params['metric_id'])
    scenario = m.Scenario.objects.get(id=params['scenario_id'])
    # TODO : catch DoesNotExist
    vs = m.ValueSet.objects.filter(scenario_id=params['scenario_id'],
                                   metric=metric,
                                   model_id__in=model_list)
    df = month(params, vs, [CM_NAME]).unstack(CM_NAME)['val']

    if params.get('compare', 'models') == 'groups':
        grdf = []
        for gr in m.ModelGroup.objects.order_by('name'):
            l = [ cm.name for cm in gr.climatemodel_set.all() if cm.name in df ]
            if not l:
                continue
            series = df[l].mean(axis=1)
            series.name = gr.name
            grdf.append(series)

        df = pd.concat(grdf, axis=1)

    if output == 'csv':
        return HttpResponse(df.to_csv(), content_type='text/csv')
    elif output == 'xls':
        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        writer = pd.ExcelWriter(response)
        df.to_excel(writer, 'Month')
        writer.save()
        return response
    else:
        if params.get('compare', 'models') == 'groups':
            nb = 5 # even if not true
            for gr in m.ModelGroup.objects.all():
                if gr.name in df:
                    ax.plot(df[gr.name], label=gr.name, color=gr.color)
        else:
            nb = 0
            for cm in m.ClimateModel.objects.all():
                if cm.name in df:
                    nb += 1
                    ax.plot(df[cm.name], color=cm.color, linestyle=cm.style)

        # setting the title right takes a lot of lines
        ax.legend(loc=2, bbox_to_anchor=(1.02, 1), fontsize=('medium', 'x-small')[nb>22])
        yr = int(params.get('yr', 2050))
        yrend = int(params.get('yrend', yr))
        method = params.get('method', 'mean')
        yrs = str(yr) if yrend == yr else '{}-{} ({})'.format(yr, yrend, method)
        change = params.get('change', None)
        zone = params.get('zone', None)
        if 'poly' in params:
            zone = 'selected zone'
        elif not zone:
            zone = '({},{})'.format(float(params.get('lon')), float(params.get('lat')))
        if change:
            refyrs = params.get('refYrs', '1950-1980')
            ax.set_title(split_title('{} monthly {} {} difference in {} between {} and {}'.format(
                scenario, metric.name, change, zone, yrs, refyrs)))
            unit = (metric.unit, '%')[change == 'relative']
            tick = ticker.FuncFormatter(lambda tick, _: '{:+{prec}}{unit}'.format(
                tick, prec=('.3', '.0f')[tick - int(tick) == 0],
                unit=unit))
        else:
            ax.set_title(split_title('{} monthly {} in {} in {}'.format(scenario, metric.name, yrs, zone)))
            tick = ticker.FormatStrFormatter('%d{}'.format(metric.unit))
        ax.yaxis.set_major_formatter(tick)
        plt.xticks(range(12), m.shortmonths, rotation=45, ha="right")


def month_by_scenarii(params, model_list=None, output='csv', ax=None):
    model_list = model_list or params.getlist('m')
    metric = m.Metric.objects.get(id=params['metric_id'])
    # TODO : catch DoesNotExist
    vs = m.ValueSet.objects.filter(metric_id=params['metric_id'],
                                   model_id__in=model_list)
    df = month(params, vs, [SC_NAME, CM_NAME])
    groo = df.groupby(['month', SC_NAME])
    res = groo.agg(['mean', 'std']).stack(dropna=False).unstack(SC_NAME).unstack()['val']

    if not params.get('change',None):
        cparam = params.copy()
        cparam['yr'], cparam['yrend'] = get_ref_yrs(params)
        past = month(cparam, vs, [SC_NAME, CM_NAME])
        rpast = past.groupby('month').agg(['mean', 'std']).rename({'val':'reference'}, axis=1)
        res = pd.concat((res, rpast), axis=1)

    if output == 'csv':
        header = 'month,' + ','.join([ s.name for s in m.Scenario.objects.order_by('name')  if s.name in res ]) + '\n'
        response = HttpResponse(header, content_type='text/csv')
        res.to_csv(response, header=False, na_rep=0)
        return response
    elif output == 'xls':
        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        writer = pd.ExcelWriter(response)
        res.to_excel(writer, 'Month', na_rep=0)
        writer.save()
        return response
    else:
        plot_fill = params.get('format','png') != 'eps'
        for sc in m.Scenario.objects.all():
            if sc.name in res:
                ax.plot(range(12), res[sc.name]['mean'], color=sc.color, label=sc.name)
                if plot_fill:
                    ymin, ymax = res[sc.name]['mean'] - res[sc.name]['std'], res[sc.name]['mean'] + res[sc.name]['std']
                    ax.fill_between(range(12), ymin, ymax, alpha=.2, color=sc.color)

        ax.legend()
        yr = int(params.get('yr', 2050))
        yrend = int(params.get('yrend', yr))
        method = params.get('method', 'mean')
        yrs = str(yr) if yrend == yr else '{}-{} ({})'.format(yr, yrend, method)
        change = params.get('change', None)
        zone = params.get('zone', None)
        if 'poly' in params:
            zone = 'selected zone'
        elif not zone:
            zone = '({},{})'.format(float(params.get('lon')), float(params.get('lat')))
        if change:
            refyrs = params.get('refYrs', '1950-1980')
            ax.set_title(split_title('Monthly {} {} difference in {} between {} and {}'.format(
                metric.name, change, zone, yrs, refyrs)))
            unit = (metric.unit, '%')[change == 'relative']
            tick = ticker.FuncFormatter(lambda tick, _: '{:+{prec}}{unit}'.format(
                tick, prec=('.3', '.0f')[tick - int(tick) == 0],
                unit=unit))
        else:
            ax.set_title(split_title('Monthly {} in {} in {}'.format(metric.name, yrs, zone)))
            tick = ticker.FormatStrFormatter('%d{}'.format(metric.unit))
        ax.yaxis.set_major_formatter(tick)
        plt.xticks(range(12), m.shortmonths, rotation=45, ha="right")


## YEAR CHARTS


def year(params, vs, values):
    month = int(params.get('month', -1))
    if month >= 0:
        yv = m.MonthlyValue.objects.filter(value_set__in=vs, month=month)
    else:
        yv = m.YearlyValue.objects.filter(value_set__in=vs)
    yvplus = annotate_val(params, yv)
    vals = yvplus.values('year', 'val', *values)
    df = pd.DataFrame.from_records(vals, index=values+['year']).unstack(values)
    change = params.get('change', None)
    if change :
        ref_yr, ref_yrend = get_ref_yrs(params)
        past = df[ref_yr-1950:ref_yrend-1950].mean()
        if change == 'absolute':
            return df-past
        return 100*(df-past)/past
    return df


def year_by_models(params, model_list=None, output='csv', ax=None):
    model_list = model_list or params.getlist('m')
    # TODO : catch DoesNotExist
    vs = m.ValueSet.objects.filter(scenario_id=params['scenario_id'],
                                   metric_id=params['metric_id'],
                                   model_id__in=model_list)
    df = year(params, vs, [CM_NAME])['val']

    if params.get('compare', 'models') == 'groups':
        grdf = []
        for gr in m.ModelGroup.objects.order_by('name'):
            l = [ cm.name for cm in gr.climatemodel_set.all() if cm.name in df ]
            if not l:
                continue
            series = df[l].mean(axis=1)
            series.name = gr.name
            grdf.append(series)

        df = pd.concat(grdf, axis=1)

        # .pivot(index='year', values='val', columns=CM_NAME)
    if output == 'csv':
        if params.get('compare', 'models') == 'groups':
            header = 'year,' + ','.join([ s.name for s in m.ModelGroup.objects.order_by('name')  if s.name in df ]) + '\n'
        else:
            header = 'year,' + ','.join([ s.name for s in m.ClimateModel.objects.order_by('name')  if s.name in df ]) + '\n'
        response = HttpResponse(header, content_type='text/csv')
        df.to_csv(response, header=False)#, na_rep=0)
        return response
        #return HttpResponse(df.to_csv(), content_type='text/csv')
    elif output == 'xls':
        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        writer = pd.ExcelWriter(response)
        df.to_excel(writer, 'Year', na_rep=0)
        writer.save()
        return response
    else:
        if params.get('compare', 'models') == 'groups':
            nb = 5 # even if not true
            for gr in m.ModelGroup.objects.all():
                if gr.name in df:
                    dat = smoothing(df[gr.name], int(params.get('smoothing',10)))
                    ax.plot(range(1950,2100), dat, label=gr.name, color=gr.color)
        else:
            nb = 0
            for cm in m.ClimateModel.objects.order_by('name'):
                if cm.name in df:
                    nb += 1
                    dat = smoothing(df[cm.name], int(params.get('smoothing',10)))
                    ax.plot(range(1950,2100), dat, label=cm.name,
                            color=cm.color, linestyle=cm.style)
        ax.legend(loc=2, bbox_to_anchor=(1.02, 1), fontsize=('medium', 'x-small')[nb>22])
        metric = m.Metric.objects.get(id=params['metric_id'])
        zone = params.get('zone', None)
        if 'poly' in params:
            zone = 'selected zone'
        elif not zone:
            zone = '({},{})'.format(float(params.get('lon')), float(params.get('lat')))
        month = int(params.get('month', -1))
        inmonth = '' if month < 0 else 'in {} '.format(m.months[month])

        change = params.get('change', None)
        if change:
            refyrs = params.get('refYrs', '1950-1980')
            ax.set_title(split_title('{} evolution of yearly {} {} in {} compared to reference {}'.format(
                m.Scenario.objects.get(id=params['scenario_id']),
                inmonth,
                metric, zone, refyrs)))
            unit = (metric.unit, '%')[change == 'relative']
            tick = ticker.FuncFormatter(lambda tick, _: '{:+{prec}}{unit}'.format(
                tick, prec=('.3', '.0f')[tick - int(tick) == 0],
                unit=unit))
        else:
            tick = ticker.FormatStrFormatter('%d{}'.format(metric.unit))
            ax.set_title(split_title('{} evolution of yearly {} {} in {}'.format(
                m.Scenario.objects.get(id=params['scenario_id']),
                inmonth,
                metric, zone)))
        ax.yaxis.set_major_formatter(tick)


def year_by_scenarii(params, model_list=None, output='csv', ax=None):
    model_list = model_list or params.getlist('m')
    metric = m.Metric.objects.get(id=params['metric_id'])
    # TODO : catch DoesNotExist
    vs = m.ValueSet.objects.filter(metric=metric,
                                   model_id__in=model_list)
    df = year(params, vs, [SC_NAME, CM_NAME])['val'].stack(CM_NAME)
    res = df.groupby(['year']).agg(['mean', 'std'])

    if output == 'csv':
        header = 'year,' + ','.join([ s.name for s in m.Scenario.objects.order_by('name')  if s.name in res ]) + '\n'
        response = HttpResponse(header, content_type='text/csv')
        res.to_csv(response, header=False, na_rep=0)
        return response
    elif output == 'xls':
        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        writer = pd.ExcelWriter(response)
        res.to_excel(writer, 'Year', na_rep=0)
        writer.save()
        return response
    else:
        plot_fill = params.get('format','png') != 'eps'
        for scen in m.Scenario.objects.all():
            if scen.name in res:
                ax.plot(res[scen.name]['mean'], color=scen.color, label=scen.name)
                if plot_fill:
                    ymin, ymax = res[scen.name]['mean'] - res[scen.name]['std'], res[scen.name]['mean'] + res[scen.name]['std']
                    ax.fill_between(range(1950, 2100), ymin, ymax, alpha=.2, color=scen.color)
    ax.legend()
    zone = params.get('zone', None)
    if 'poly' in params:
        zone = 'selected zone'
    elif not zone:
        zone = '({},{})'.format(float(params.get('lon')), float(params.get('lat')))
    month = int(params.get('month', -1))
    inmonth = '' if month < 0 else 'in {} '.format(m.months[month])

    change = params.get('change', None)
    if change:
        refyrs = params.get('refYrs', '1950-1980')
        ax.set_title(split_title('Evolution of yearly {} {} in {} compared to reference {}'.format(
            inmonth,
            metric, zone, refyrs)))
        unit = (metric.unit, '%')[change == 'relative']
        tick = ticker.FuncFormatter(lambda tick, _: '{:+{prec}}{unit}'.format(
            tick, prec=('.3', '.0f')[tick - int(tick) == 0],
            unit=unit))
    else:
        tick = ticker.FormatStrFormatter('%d{}'.format(metric.unit))
        ax.set_title(split_title('Evolution of yearly {} {}in {}'.format(metric, inmonth, zone)))
    ax.yaxis.set_major_formatter(tick)


## RASTER SECTION
# does not work

def raster(params, model_list=None):
    model_list = model_list or params.getlist('m')
    metric = m.Metric.objects.get(id=params['metric_id'])
    vs = m.ValueSet.objects.filter(metric=metric, scenario_id=params['scenario_id'], model_id__in=model_list)
    if params.get('month',-1) >= 0:
        yearmonth = m.MonthlyValue
        period = 'month'
    else:
        yearmonth = m.YearlyValue
        period = 'year'
    yv = yearmonth.objects.filter(value_set__in=vs)

    if 'poly' in params:
        poly = custom_poly_box(params['poly'])
    elif 'zone' in params:
        poly = zone_poly_box(params['zone'])
    else:
        poly = point_neighbourhood(params.get('lon'), params.get('lat'))
    yvplus = yv.annotate(poly=poly)

    yr = params.get('yr',2050)
    yrend = params.get('yrend', yr)
#    yv_future = yvplus.filter(year__range=(yr, yrend))

    qscale = params.get('scale', 'relative')
    change = params.get('change', None)
    method = params.get('method', 'MEAN')
    relative = (qscale == 'relative' or change or method.lower() == 'sum')
    color = metric.color_code(relative=relative, period=period)

    return yvplus.aggregate(res=Algebra(
        RUnion(Clip('raster', 'poly'), filter=Q(year__range=(2050, 2060))),
        RUnion(Clip('raster', 'poly'), filter=Q(year__range=(1950, 1980))),
        formula='[rast2]-[rast1]'))
