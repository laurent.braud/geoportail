from django.db import connection, DataError, ProgrammingError
from django.http import Http404, HttpResponse
from . import models
import csv

# import matplotlib
# matplotlib.use('Agg')
# import matplotlib.pyplot as plt
# import matplotlib.ticker as ticker
# import numpy as np
# import pandas as pd

def split_title(t, maxwidth=55):
    """Splits long titles because matplotlib does not by itself."""
    if len(t) < maxwidth:
        return t
    s, res = 0, ['']
    for w in t.split():
        if s + len(w) > maxwidth:
            s = len(w)
            res.append(w)
        else:
            s += len(w)
            res[-1] += ' ' + w
    return '\n'.join(res)

def poly2postgis(poly):
    """Translates a polygon in list form into postgis form."""
    parray = eval(poly)
    poly_wkb = ','.join(['{} {}'.format(parray[2 * i], parray[2 * i + 1]) for i in range(len(parray) // 2)])
    # SRID is hardcoded to 4326
    return "ST_GeomFromWKB('POLYGON((" + poly_wkb + "))'::geometry, 4326)"


class RasterQuery:
    """Given a dictionary of values, builds a postgis query for a raster.
    """

    def __init__(self, parameters, model_list=None):
        # def get_zone_bounds(zone, vs):
        #     with connection.cursor() as cursor:
        #         cursor.execute(
        #             "select min((stats).min), max((stats).max) "
        #             "from (select st_summarystats(ST_Clip(raster, ST_Expand(ST_Envelope(mpoly),1,1))) as stats "
        #             "from climap_yearlyvalue, climap_zone where value_set_id = %s and name=%s) as foo;",
        #             [ vs.id, zone ]
        #         )
        #         return cursor.fetchone()

        # only mandatory parameters are these three
        model_list = model_list or parameters.getlist('m')
        scenario_id = parameters['scenario_id']
        metric_id = parameters['metric_id']

        # other parameters can be autoset
        method = parameters.get('method', 'MEAN')
        yr = int(parameters.get('yr',2050))
        yrend = int(parameters.get('yrend',yr))
        area = parameters.get('area', 'near')
        qscale = parameters.get('scale', 'relative')
        change = parameters.get('change', None)
        refyrs = parameters.get("refYrs", "1950-1980")
        if '-' in refyrs:
            ref_yr1,ref_yr2 = refyrs.split('-')
        else:
            ref_yr1,ref_yr2 = 1950, 1980
        metric = models.Metric.objects.get(id=metric_id)
        title = metric.name
        self.cmap = metric.colormap

        month = int(parameters.get('month', -1))
        if month >= 0:
            self.vmin, self.vmax = metric.monthvmin, metric.monthvmax
            period='month'
            andmonth=' and month=%s'
            titlemonth=models.months[int(month)]
        else:
            self.vmin, self.vmax = metric.yearvmin, metric.yearvmax
            period='year'
            andmonth=''
            titlemonth=''

        vsidslist, modelnames = [], []
        for m in model_list:
            try:
                vs = models.ValueSet.objects.get(metric=metric, scenario_id=scenario_id, model_id=m)
                vsidslist.append('value_set_id={}'.format(vs.id))
                modelnames.append(vs.model.name)
            except models.ValueSet.DoesNotExist:
                continue
        if len(vsidslist) == 0:
            self.error = 'no value found'
            return
        vsids = '({})'.format(' or '.join(vsidslist))

        if yrend-yr < 1 or yr == vs.model.yearmax :
            title += ' in {} {}'.format(titlemonth, yr)
        else:
            if method == 'SUM':
                title = "summed {} for {} {}-{}".format(title, titlemonth, yr, min(yrend, vs.model.yearmax))
            else:
                title = "{} {} for {} {}-{}".format(method.lower(), title, titlemonth, yr,
                                                    min(yrend, vs.model.yearmax))

        # Three parameters are examined to build the query.
        # 1. whole map or clipped map ?
        # 2. raw data or relative change ?
        # 3. absolute or relative scale ?

        if area == 'all':
            qry = "(select {} ST_Union(raster,%s) as raster from climap_" + period + "lyvalue " \
                                                                                     "where " + vsids + " and year >= %s and year <= %s {})"
            qargs = [method]
        else:
            expand = (area == 'near')
            poly, zone = parameters.get('poly', None), parameters.get('zone', None)
            if poly:
                title += ' in selected zone'
                if expand:
                    qry = "(select {} ST_Union(ST_Clip(raster, ST_Expand(" \
                          +poly2postgis(poly)+",1,1)),%s) as raster " \
                          "from climap_"+period+"lyvalue where "+vsids+" and year >= %s and year <= %s {})"
                else:
                    qry = "(select {} ST_Union(ST_Clip(raster, " \
                          + poly2postgis(poly) + "),%s) as raster " \
                          "from climap_" + period + "lyvalue where " + vsids + " and year >= %s and year <= %s {})"
                qargs = [ method, ]
            elif zone :
                title += ' in '+zone
                if expand:
                    qry = "(select {} ST_Union(ST_Clip(raster, ST_Expand(mpoly,1,1)),%s) " \
                          "as raster from climap_"+period+"lyvalue, climap_zone " \
                          "where name = %s and "+vsids+" and year >= %s and year <= %s {})"
                else:
                    qry = "(select {} ST_Union(ST_Clip(raster,mpoly),%s) " \
                          "as raster from climap_"+period+"lyvalue, climap_zone " \
                          "where name = %s and "+vsids+" and year >= %s and year <= %s {})"

                qargs = [ method, zone ]
            else:
                lon = float(parameters.get('lon', -17.45))
                lat = float(parameters.get('lat', 14.7))
                size = 5 if expand else 1
                qry = "(select {} ST_Union(ST_Clip(raster, circle), %s) as raster " \
                      "from (select ST_Buffer(ST_SetSRID(ST_Point(%s,%s),4326),"+str(size)+") as circle) as yoyo, " \
                      "climap_"+period+"lyvalue where "+ vsids + " and year >= %s and year <= %s {})"
                qargs = [ method, lon, lat ]
        if change :
            qry = qry.format("value_set_id as vs,", "group by value_set_id")
            if change == 'relative':
                formula = '100*([rast2]-[rast1])/(NULLIF([rast1],0))'
                self.unit = '%'
            else:
                formula = '[rast2]-[rast1]'
                self.unit = metric.unit

            self.preqry = "with future as {qry}, past as {qry} ".format(qry=qry)
            self.postqry = "(select ST_Union(raster,'MEAN') as raster from " \
                           "(select ST_MapAlgebra(past.raster,future.raster,%s) as raster from future, past " \
                           "where past.vs = future.vs) as foo) as bar;"

            title = '{} difference between {} and reference period {}-{}'.format(change, title, ref_yr1, ref_yr2)
            self.preargs = qargs + [ yr, yrend ] + qargs + [ ref_yr1, int(ref_yr2)+1 ]
            self.postargs = [ formula ]
        else:
            self.preqry, self.postqry = "", qry.format('',andmonth) + ' as foo;'
            self.preargs, self.postargs = [], qargs + [ yr, yrend ]
            if month >= 0:
                self.postargs.append(month)
            self.unit = metric.unit

        if qscale.startswith('man'):
            s = qscale.split(',')
            self.vmin, self.vmax = float(s[1]), float(s[2])
            self.relative = False
            self.color = metric.color_code(vmin=self.vmin,vmax=self.vmax)
        else:
            self.relative = (qscale == 'relative' or change or method == 'SUM')
            self.color = metric.color_code(relative=self.relative, period=period)

        self.title = models.Scenario.objects.get(id=scenario_id).name + ' ' + title
        self.error = ''
        self.rmeta = None


    def execute(self, output='png'):
        """Dumps the raster corresponding to the query, in png or value form."""
        if self.error:
            return

        if output == 'png':
            self.qry = self.preqry + "select ST_AsPNG(ST_ColorMap(raster, %s)), " \
                                "ST_SummaryStats(raster), ST_Metadata(raster) from " + self.postqry
            self.qargs = self.preargs + [ self.color ] + self.postargs
        else:
            self.qry = self.preqry + "select (ST_DumpValues(raster)).valarray, " \
                                "ST_SummaryStats(raster), ST_Metadata(raster) from " + self.postqry
            self.qargs = self.preargs + self.postargs

        with connection.cursor() as cursor:
            cursor.execute(self.qry, self.qargs)
            obj, stats, rmeta = cursor.fetchone()

        # tmpqry = self.qry
        self.rmeta = eval(rmeta)

        if self.relative:
            stats = eval(stats)
            self.vmin, self.vmax = round(stats[4], 3), round(stats[5], 3)

        return obj

###################################################
# CONFIDENCE QUERY

def confidence(parameters):
    """Returns a GeoJSON polygon. """
    area = parameters.get('area', 'near')
    if area == 'all':
        qpoly = "st_envelope(raster) as poly from climap_yearlyvalue limit 1"
        qargs = ()
    else:
        expand = (area == 'near')
        if 'poly' in parameters :
            if expand:
                qpoly = "st_expand("+poly2postgis(parameters['poly'])+",1,1) as poly"
            else:
                qpoly = poly2postgis(parameters['poly']) + " as poly"
            qargs = ()
        elif 'zone' in parameters:
            if expand:
                qpoly = "st_expand(mpoly,1,1) as poly from climap_zone where name =%s"
            else:
                qpoly = "mpoly as poly from climap_zone where name =%s"
            qargs = (parameters['zone'],)
        else:
            lon = float(parameters.get('lon', -17.45))
            lat = float(parameters.get('lat', 14.7))
            size = 1 if expand else 5
            qpoly = "ST_Buffer(ST_SetSRID(ST_Point(%s,%s),4326),{}) as poly".format(size)
            qargs = (lon, lat)

    month = int(parameters.get('month', -1))
    if month >= 0:
        period, andperiod = 'month', 'month = %s and '
    else:
        period, andperiod = 'year', ''

    # This SQL code  :
    # - makes the diff future-past
    # - keeps only the sign for each model
    # - computes the mean pixel-vise
    # - keeps only values > .5, which means that 3/4 of models agree on sign
    # - translates this into polygon, then multipolygon, and finally geojson
    qry = "with p as (select "+qpoly+"), " \
          "future as (select value_set_id as vs, st_union(st_clip(raster, poly),'MEAN') as raster from " \
          "	climap_"+period+"lyvalue, climap_valueset, p where "+andperiod+ ""\
          "	value_set_id = climap_valueset.id and metric_id = %s and scenario_id = %s " \
          "	and year >= %s and year <= %s group by value_set_id), " \
          "past as (select value_set_id as vs, st_union(st_clip(raster, poly),'MEAN') as raster from " \
          "	climap_"+period+"lyvalue, climap_valueset, p where "+andperiod+ ""\
          "	value_set_id = climap_valueset.id and metric_id = %s and scenario_id = %s " \
          "	and year >= %s and year <= %s group by value_set_id) " \
          "select st_asgeojson(st_union(geom), 4, 2) from" \
          "(select (st_dumpaspolygons(st_mapalgebra(raster, '2BUI', " \
          "'CASE WHEN abs([rast])>= 0.5 then 1 else NULL END'))).geom as geom from ( " \
          "select st_union(raster, 'MEAN') as raster " \
          "from (select st_mapalgebra(past.raster, future.raster, 'sign([rast2]-[rast1])', '32BF'::text) " \
          "as raster from future, past " \
          "where past.vs = future.vs) as foo) as bar) as baz; "

    scen_id = parameters['scenario_id']
    metric_id = parameters['metric_id']
    yr = int(parameters.get('yr', 2050))
    yrend = int(parameters.get('yrend', yr))
    refyrs = parameters.get("refYrs", "1950-1980")
    if '-' in refyrs:
        ref_yr1, ref_yr2 = refyrs.split('-')
    else:
        ref_yr1, ref_yr2 = 1950, 1980

    if month >= 0:
        qargs = qargs + (month, metric_id, scen_id, yr, yrend,
                         month, metric_id, scen_id, ref_yr1, ref_yr2)
    else:
        qargs = qargs + (metric_id, scen_id, yr, yrend,
                         metric_id, scen_id, ref_yr1, ref_yr2)



    with connection.cursor() as cursor:
        cursor.execute(qry, qargs)
        poly = cursor.fetchone()[0]

    return HttpResponse(poly)