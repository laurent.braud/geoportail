from django.shortcuts import render, reverse
from django.http import FileResponse, HttpResponse, HttpResponseRedirect
from django.core.files.temp import NamedTemporaryFile
from django.db import connection
from netCDF4 import Dataset, date2num
from datetime import datetime
import numpy as np
from . import models
from .forms import DownloadForm, PublicDownloadForm
import csv
import numpy as np

class MetaData():
    # Metadata : upperleftx | upperlefty | width | height | scalex | scaley | skewx | skewy | srid | numbands

    def __init__(self, string):
        self.upperleftx, self.upperlefty, self.width, self.height, self.scalex, self.scaley, _,_,self.srid, _ = eval(string)
        self.lowerrightx = self.upperleftx + self.width*self.scalex
        self.lowerrighty = self.upperlefty + self.width*self.scaley

def index(request):
    if request.user.is_authenticated:
        return render(request, 'climap/download.html', {'form':DownloadForm().as_ul()})
    else:
        return render(request, 'climap/download.html', {'form':PublicDownloadForm().as_ul()})

DATATYPES = ['unknown', 'int8', 'uint16', 'int16', 'uint32', 'int32', 'float32', 'float64', ]


def removechars(name):
    res = ''
    for c in name:
        if c == '#':
            res += 'nb'
        elif c.isalpha() or c.isdigit():
            res += c
    return res


def write_netcdf(form, dat, filename):
    meta = MetaData(dat[0][-1])
    ismonth = (form.cleaned_data['period']=='month')

    tmpfile = NamedTemporaryFile(suffix='.nc')
    out = Dataset(tmpfile.name, 'w')
    out.createDimension('lon', meta.width)
    out.createDimension('lat', meta.height)
    out.createDimension('time', None)
    lon = out.createVariable('lon', 'f4', ('lon',))
    lon.units = 'degrees east'
    lon[:] = np.linspace(meta.upperleftx, meta.lowerrightx, meta.width)
    lat = out.createVariable('lat', 'f4', ('lat',))
    lat.units = 'degrees north'
    lat[:] = np.linspace(meta.upperlefty, meta.lowerrighty, meta.height)

    time = out.createVariable('time', 'i4', ('time',))
    time.units = 'days since 1950-01-01'
    time.calendar = 'gregorian'
    numdates = []
    for year in range(form.cleaned_data['yr'], form.cleaned_data['yrend']+1):
        if ismonth:
            for month in range(1,12):
                numdates.append(date2num(datetime(year, month, 1), units=time.units, calendar=time.calendar))
        else:
            numdates.append(date2num(datetime(year, 1, 1), units=time.units, calendar=time.calendar))
    time[:] = numdates

    # TODO
    vtype = 'float64' # DATATYPES[ pvs.first().raster.bands[0].datatype() ]
    metric = form.cleaned_data['parameter']
    varname = metric.shortname
    out.createVariable(varname, vtype, ('time', 'lat', 'lon',))
    out[varname].unit = metric.unit
    out[varname][:] = np.array([row[0] for row in dat])
    out.close()

    response = FileResponse(tmpfile, content_type='application/nc')
    response['Content-Disposition'] = 'attachment; filename={}_{}.nc'.format(filename, varname)

    return response


def write_csv(form, dat, filename):
    metric = form.cleaned_data['parameter']
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename={}_{}.csv'.format(filename, metric.shortname)
    meta = MetaData(dat[0][-1])
    writer = csv.writer(response)
    if form.cleaned_data['period'] == 'month':
        writer.writerow([ 'year', 'month', 'lon', 'lat', metric.name])
        for row in dat:
            for x in range(meta.width):
                for y in range(meta.height):
                    writer.writerow([ row[1], row[2]+1, (x+.5)*meta.scalex+meta.upperleftx,
                                      (y-.5)*meta.scaley+meta.upperlefty, row[0][y][x] ])
    else:
        writer.writerow([ 'year', 'lon', 'lat', metric.name])
        for row in dat:
            for x in range(meta.width):
                for y in range(meta.height):
                    if row[0][y][x]:
                        writer.writerow(
                            [row[1], (x+.5) * meta.scalex + meta.upperleftx, (y-.5)*meta.scaley+meta.upperlefty,
                             row[0][y][x]])

    return response


def data(request):
    if request.user.is_authenticated:
        form = DownloadForm(request.POST)
    else:
        form = PublicDownloadForm(request.POST)
    if not form.is_valid():
        return render(request, 'climap/download.html', {'form':form.as_ul(), 'error':'Invalid form.'})

    metric = form.cleaned_data['parameter']

    if form.cleaned_data['climate_model']:
        try:
            vs = models.ValueSet.objects.get(metric=metric,
                                             scenario=form.cleaned_data['scenario'],
                                             model=form.cleaned_data['climate_model'])
        except models.ValueSet.DoesNotExist:
            return render(request, 'climap/download.html', {'form': form.as_ul(), 'error': 'No data found.'})
        if form.cleaned_data['zone']:
            # Metadata : upperleftx | upperlefty | width | height | scalex | scaley | skewx | skewy | srid | numbands
            qry = 'select st_dumpvalues(clip, 1), {yearmonth}, st_metadata(clip) from ' \
                  '(select {yearmonth}, st_clip(raster, st_expand(mpoly, 1)) as clip ' \
                  'from climap_{period}lyvalue, climap_zone where name = %s and value_set_id = %s ' \
                  'and year >= %s and year <= %s) as foo ' \
                  'order by {yearmonth};'
            qargs = (form.cleaned_data['zone'].name, vs.id)
        else:
            qry = 'select st_dumpvalues(raster, 1), {yearmonth}, st_metadata(raster)' \
                  'from climap_{period}lyvalue, climap_zone where value_set_id = %s ' \
                  'and year >= %s and year <= %s order by {yearmonth};'
            qargs = (vs.id, )
    else:
        if form.cleaned_data['zone']:
            qry = 'select st_dumpvalues(clip, 1), {yearmonth}, st_metadata(clip) from ' \
                  "(select {yearmonth}, st_union(clip, 'mean') as clip from " \
                  '(select {yearmonth}, st_clip(raster, st_expand(mpoly, 1)) as clip ' \
                  'from climap_{period}lyvalue, climap_valueset, climap_zone where name = %s ' \
                  'and scenario_id=%s and metric_id=%s and value_set_id=climap_valueset.id ' \
                  'and year >= %s and year <= %s) as foo group by {yearmonth}) as bar ' \
                  'order by {yearmonth};'
            qargs = (form.cleaned_data['zone'].name, form.cleaned_data['scenario'].id, metric.id)
        else:
            qry = 'select st_dumpvalues(raster, 1), {yearmonth}, st_metadata(raster) from ' \
                  "(select {yearmonth}, st_union(raster, 'mean') as raster from " \
                  'climap_{period}lyvalue, climap_valueset where value_set_id=climap_valueset.id ' \
                  'and scenario_id=%s and metric_id=%s and ' \
                  'year >= %s and year <= %s group by {yearmonth}) as foo order by {yearmonth};'
            qargs = (form.cleaned_data['scenario'].id, metric.id)

    if form.cleaned_data['period'] == 'month':
        qry = qry.format(yearmonth='year, month', period='month')
    else:
        qry = qry.format(yearmonth='year', period='year')

    with connection.cursor() as cursor:
        cursor.execute(qry, qargs+(form.cleaned_data['yr'], form.cleaned_data['yrend']))
        dat = cursor.fetchall()

    if form.cleaned_data['climate_model']:
        filename = '{}_{}'.format(
            form.cleaned_data['climate_model'].name.replace(' ',''),
            form.cleaned_data['scenario'].name)
    else:
        filename = form.cleaned_data['scenario'].name


    if form.cleaned_data['format'] == 'netcdf':
        return write_netcdf(form, dat, filename)
    elif form.cleaned_data['format'] == 'csv':
        return write_csv(form, dat, filename)