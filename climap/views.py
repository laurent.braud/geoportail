from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404, JsonResponse, HttpResponseRedirect
from .models import ClimateModel, Metric, Scenario, Division, MetricCategory, ModelGroup
from django.core.serializers import serialize
from django.db import connection
from django.db.models import Count
import numpy as np
from .query import RasterQuery, confidence
from .chartquery import month_by_models, month_by_scenarii, year_by_models, year_by_scenarii, pinpoint
from .forms import FeedBack
# from django.core.mail import send_mail
import json
from django.db.models import F

SAMPLES = ( 'ACCESS1.3', 'BCC_CSM 1.1', 'BNU-ESM',
            'CanESM2', 'CMCC-CESM', 'CNRM-CM5',
            'CSIRO-Mk3', 'GFDL-ESM2G', 'HadGEM2-ES',
            'INMCM4', 'IPSL-CM5A-LR', 'MIROC-ESM',
            'MPI-ESM-LR', 'MRI-ESM1', 'NorESM1-M', )
SAMPLE4 = ( 'BNU-ESM', 'MPI-ESM-MR', 'GISS_E2_R', 'HadGEM2_CC' )
SAMPLE8 = ( 'giss_e2_r_cc', 'ccsm4, mpi_esm_lr', 'cnrm_cm5', 'bcc_csm1_1', 'miroc_esm', 'cesm1_bgc', 'haadgem2_cc' )


def newindex(request):
    cmodels = ClimateModel.objects.order_by('name')
    dygraph_strokes = { 'solid':[], 'dashed':[7,2], 'dashdot':[7,2,2,2], 'dotted':[2,2]}
    colors = { m.name: {'color':m.color, 'strokePattern':dygraph_strokes.get(m.style,[]) } for m in cmodels }
    colors.update({ s.name: {'color': s.color} for s in Scenario.objects.all() })
    colors.update({ g.name: {'color': g.color} for g in ModelGroup.objects.all() })

    if request.user.is_authenticated:
        cats = MetricCategory.objects.all()
    else:
        cats = MetricCategory.objects.filter(public=True)

    countries = Division.objects.get(name='countries').zone_set.all()
    context = {
        'catmetric': cats,
        'colors': json.dumps(colors),
        'yearmin': min([c.yearmin for c in cmodels]),
        'yearmax': max([c.yearmax for c in cmodels]),
        'metrics': Metric.objects.order_by('shortname'),
        'scenarii': Scenario.objects.all().annotate(vs=Count('valueset')).filter(vs__gt=0),
        'default_metric': Metric.objects.first(),
        'default_scenario': Scenario.objects.get(name='rcp85'),
        'divisions': Division.objects.order_by('name'),
        'models': cmodels,
        'countries': serialize('geojson', countries),
    }
    return render(request, 'climap/index2.html', context)

def main(request):
    return render(request, 'climap/main.html')


def help(request):
    context = {
        'catmetric': MetricCategory.objects.order_by('name'),
        'models': ClimateModel.objects.order_by('name')
            .annotate(scen=Count('valueset__scenario', distinct=True))
            .annotate(vs=Count('valueset')),
        'metrics': Metric.objects.order_by('category', 'name'),
        'scenario': Scenario.objects.order_by('name'),
        'groups': ModelGroup.objects.order_by('name'),
    }
    return render(request, 'climap/help.html', context)


def api(request):
    return render(request, 'climap/api.html')


def scale(request, Metric_id):
    m = get_object_or_404(Metric, pk=Metric_id)
    with connection.cursor() as cursor:
        cursor.execute("select ST_AsPNG(ST_ColorMap(scaleraster,%s))"
                       "from climap_metric where id = %s;",
                       [m.color_code(), Metric_id])
        return HttpResponse(cursor.fetchone()[0], content_type="image/png")

def pre(request):
    """Returns the extent of a raster drawn by image()."""
    vs = Metric.objects.get(id=request.GET.get('metric')).valueset_set.first()
    #model = ClimateModel.objects.first()
    poly, zone = request.GET.get('poly', None), request.GET.get('zone', None)
    lon, lat = request.GET.get('lon', None), request.GET.get('lat', None)
    area = request.GET.get('area', 'all')
    if area == 'all':
        j = {'extent': vs.yearlyvalue_set.first().raster.extent}
    else:
        expand = (area == 'near')
        if poly:
            parray = eval(poly)
            poly_wkb = ','.join(['{} {}'.format(parray[2 * i], parray[2 * i + 1]) for i in range(len(parray) // 2)])
            with connection.cursor() as cursor:
                # Columns returned: upperleftx | upperlefty | width | height | scalex | scaley | skewx | skewy | srid | numbands
                if expand:
                    qry = "select ST_Metadata(ST_Clip(raster,ST_Expand(ST_GeomFromWKB('POLYGON(("\
                          +poly_wkb\
                          +"))'::geometry, 4326),1,1))) from climap_yearlyvalue " \
                           "where value_set_id=%s limit 1;"
                else :
                    qry = "select ST_Metadata(ST_Clip(raster,ST_GeomFromWKB('POLYGON(("\
                          +poly_wkb\
                          +"))'::geometry, 4326))) from climap_yearlyvalue " \
                           "where value_set_id=%s limit 1;"

                cursor.execute(qry, [vs.id,])
                meta = eval(cursor.fetchone()[0])
            j = {'extent': [ meta[0], meta[1]+meta[3]*meta[5], meta[0]+meta[2]*meta[4], meta[1] ] }
        elif zone:
            with connection.cursor() as cursor:
                if expand:
                    cursor.execute("select ST_Metadata(ST_Clip(raster,ST_Expand(mpoly,1,1))) "
                                   "from climap_yearlyvalue, climap_zone "
                                   "where value_set_id=%s and name=%s limit 1;",
                                   [vs.id, zone])
                else:
                    cursor.execute("select ST_Metadata(ST_Clip(raster,mpoly)) "
                                   "from climap_yearlyvalue, climap_zone "
                                   "where value_set_id=%s and name=%s limit 1;",
                                   [vs.id, zone])
                meta = eval(cursor.fetchone()[0])
            j = {'extent': [ meta[0], meta[1]+meta[3]*meta[5], meta[0]+meta[2]*meta[4], meta[1] ] }
        elif lon:
            with connection.cursor() as cursor:
                if expand :
                    cursor.execute("select ST_Metadata(ST_Clip(raster,ST_Buffer(ST_SetSRID(ST_Point(%s,%s),ST_SRID(raster)), 5)))  "
                                   "from climap_yearlyvalue where value_set_id=%s limit 1;",
                                   [lon, lat, vs.id,])
                else :
                    cursor.execute("select ST_Metadata(ST_Clip(raster,ST_Buffer(ST_SetSRID(ST_Point(%s,%s),ST_SRID(raster)), 1)))  "
                                   "from climap_yearlyvalue where value_set_id=%s limit 1;",
                                   [lon, lat, vs.id,])
                meta = eval(cursor.fetchone()[0])
            j = {'extent': [ meta[0], meta[1]+meta[3]*meta[5], meta[0]+meta[2]*meta[4], meta[1] ] }
    return JsonResponse(j)


def metadata(request, Metric_id):
    met = get_object_or_404(Metric, pk=Metric_id)
    j = {'vmin': request.session.get('vmin', met.yearvmin),
         'vmax': request.session.get('vmax', met.yearvmax),
         'scale': [round(x,2) for x in np.linspace(request.session.get('vmax', met.yearvmax),
                                                   request.session.get('vmin', met.yearvmin), 10)],
         'unit': request.session.get('unit', met.unit),
         'error': request.session.get('error', ''),
         'description': met.description,
         'title': request.session.get('title', met.description),
         'extent': request.session.get('extent', [-20,-40,55,40]),
         'query': request.session.get('query', ''),
         'qargs': request.session.get('qargs', ''),
         }
    return JsonResponse(j)


def image(request):
    qry = RasterQuery(request.GET)
    rawim = qry.execute()
    request.session['error'] = qry.error
    if not qry.error:
        request.session['vmin'] = qry.vmin
        request.session['vmax'] = qry.vmax
        request.session['unit'] = qry.unit
        request.session['title'] = qry.title
        request.session['query'] = qry.qry
        request.session['qargs'] = qry.qargs

    return HttpResponse(rawim, content_type="image/png")


def month(request, extension):
    compare = request.GET.get('compare', 'models')
    if compare != 'scenarii':
        return month_by_models(request.GET, output=extension)
    else:
        return month_by_scenarii(request.GET, output=extension)

def point(request):
    return pinpoint(request.GET)

def year(request, extension):
    compare = request.GET.get('compare', 'models')
    if compare != 'scenarii':
        return year_by_models(request.GET, output=extension)
    else:
        return year_by_scenarii(request.GET, output=extension)


def zones(request, div_id):
    z = Division.objects.get(id=div_id).zone_set.all()
    return HttpResponse(serialize('geojson', z), content_type='application/json')

def conf(request):
    return confidence(request.GET)

