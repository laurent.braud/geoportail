from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from proj.load import places, zones

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--places', nargs='?', default=settings.PLACES_FILE,
                            help='csv file with three columns : name, location in WKT format, show on map Y/N')
        parser.add_argument('--zones', nargs='?', default=settings.ZONES_FILE,
                            help='shapefile of available regions')

    def handle(self, *args, **options):
        pfile = options['places']
        places(pfile, delete_existing=True)
        zfile = options['zones']
        zones(zfile, delete_existing=True)
