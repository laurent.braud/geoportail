
/* Alternate strings for locale 'fr' */

var months = [ 'Jan', 'Fév', 'Mars', 'Avr', 'Mai', 'Juin', 'Juil',
      	       'Août', 'Sept', 'Oct', 'Nov', 'Déc'],
    fullmonths = [ 'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet',
      		   'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];


function capitalize(s) {
    return s[0].toUpperCase() + s.slice(1);
}

function printMonth(args, param, zone,yrs) {

    if (!isMonthly(getMetric())){
        document.getElementById('month_export').style.display = 'none';
        document.getElementById('monthChartContainer').style.visibility = 'hidden';
        return;
    }
    document.getElementById('month_export').style.display = 'block';
    document.getElementById('monthChartContainer').style.visibility = 'visible';

    if(monthgraph) monthgraph.destroy();


    var title, ebars = (getRadio('compare')=='scenarii');
    if (zone === 'Senegal')
        title = 'Moyenne mensuelle de '+param+' au Sénégal sur la période '+yrs;
    else
        title = 'Moyenne mensuelle de '+param+' en '+zone+' sur la période '+yrs;
    if (getRadio('data') != 'data')
        title += ', par rapport à '+topForm.refYrs.value;
    if (ebars)
        title += ' (scenario '+getRadio('scenario')+')';

    // export
    document.getElementById('month_csv').href = 'month.csv'+args;
    document.getElementById('month_xls').href = 'month.xls'+args;

    /*
    title += ' <span class="helptext"><a href="month.csv'+args+'">csv</a>'
           + '/<a href="month.xls'+args+'">xls</a></span>';
    */
    var plusunit = (getRadio('data')==='data'?'':'+');

    monthgraph = new Dygraph(monthChart, 'month.csv'+args+'&zone='+zone, {
        legend: 'follow',
        title: title,
        errorBars: ebars,
        sigma : 1,
        rollPeriod: 1,
        series: graph_color,
        strokeWidth: 2,
        axes: {
            x: {
                axisLabelWidth: 70,
                pixelsPerLabel: 30,
                axisLabelFormatter: function(d) { return months[d-1] },
                valueFormatter: function(d) { return months[d-1] }
            },
            y : {
            axisLabelFormatter: function(d) {
                    if (d>=0) return plusunit+d;
                    else return d;
                },
            valueFormatter: ebars?errorBarValueFormat:ValueFormat
            }
        },
        highlightSeriesOpts: {
            strokeWidth: 3,
            strokeBorderWidth: 1,
            highlightCircleSize: 3
        }
    });

}

var ROLLING_SEMIPERIOD = 15

function valueRollFormat(x, opts, series, graph, row, col){
    var i, i_0 = Math.max(0,row-ROLLING_SEMIPERIOD),
        i_n = Math.min(row+ROLLING_SEMIPERIOD, graph.numRows()),
        sum = 0, n = 0;
    for (i=i_0;i<i_n;i++){
        n += 1;
        sum += graph.getValue(i, col);
    }
    return x.toFixed(2) + '<br>['+(sum / n).toFixed(2)+' sur 30 ans]' ;
}

function errorBarValueRollFormat(x, opts, series, graph, row, col){
    var i, i_0 = Math.max(0,row-ROLLING_SEMIPERIOD),
        i_n = Math.min(row+ROLLING_SEMIPERIOD, graph.numRows()),
        sum = 0, n = 0;
    for (i=i_0;i<i_n;i++){
        n += 1;
        sum += graph.getValue(i, col)[0];
    }
    return x.toFixed(2) + ' &#177;'+graph.getValue(row,col)[1].toFixed(2)+'<br>['+(sum / n).toFixed(2)+' sur 30 ans]' ;
}


function printYear(args, param, zone) {
    if(yeargraph) yeargraph.destroy();

    var title;
    if (zone === 'Senegal')
        title = 'Évolution de '+param+' au Sénégal';
    else
        title = 'Évolution de '+param+' en '+zone;

    if (isMonthly(getMetric())){
        var mon = getMonthsName();
        if (mon) title += ' en '+mon;
    }
    var compare = getRadio('compare'),
        data = getRadio('data');
    var ebars = (compare=='scenarii');

    if (data != 'data')
        title += ', par rapport à '+topForm.refYrs.value;
    if (compare!='scenarii')
        title += ' (scenario '+getRadio('scenario')+')';

    // export
    document.getElementById('year_csv').href = 'year.csv'+args;
    document.getElementById('year_xls').href = 'year.xls'+args;
    /*
    title += ' <span class="helptext"><a href="year.csv'+args+'">csv</a>'
           + '/<a href="year.xls'+args+'">xls</a></span>';
    */

    var plusunit = (data==='data'?'':'+');

    yeargraph = new Dygraph(yearChart, 'year.csv'+args+'&zone='+zone, {
        legend: 'follow',
        title: title,
        errorBars: (compare=='scenarii'),
        sigma : 1,
        /*showRoller: true,
        rollPeriod: (compare=='scenarii'?1:10),*/
        series: graph_color,
        strokeWidth: 2,
        showRangeSelector: true,
        interactionModel: Dygraph.defaultInteractionModel,
        highlightSeriesOpts: {
            strokeWidth: 3,
            strokeBorderWidth: 1,
            highlightCircleSize: 3
        },
        axes: {
            y : {
            axisLabelFormatter: function(d) {
                    if (d>=0) return plusunit+d;
                    else return d;
                },
            valueFormatter : ebars?errorBarValueRollFormat:valueRollFormat
            }
        }
    });
    yeargraph.ready(function(){
        var labels = yeargraph.getLabels();
       // remove the first x-axis element
        labels.shift();
        displayLabels('graphLegend', labels);
    });

}

function setAgg(vagg){
    if (vagg === 'mean') vagg = 'moyenne';
    if (vagg === 'sum') vagg = 'somme';
    if (vagg === 'count') vagg = 'décompte';
    document.getElementById('agg').innerHTML = vagg;
}
