
/* FORM VARIABLES and useful functions */
var topForm = document.getElementById('topForm'),
    chartForm = document.getElementById('chartForm'),
    metricRadio = document.getElementsByName('metric'),
    compareRadio = document.getElementsByName('compare'),
    scenRadio = document.getElementsByName('scenario');

/* python overwrite */
var False = false,
    True = true;

function getRadio(type){
    /* actual value for radio inputs */
    var radio = document.getElementsByName(type);
    for (var i=0;i<radio.length;i++){
        if (radio[i].checked)
            return radio[i].value;
    }
}

function getMetric(){
    /* id of current metric, -1 if custom formula */
    for (var i=0;i<metricRadio.length;i++){
        if (metricRadio[i].checked)
            return metricRadio[i].value;
    }
}

/* OPENLAYERS */

/* layers z-index :
TOP
    selected place
    places
    stations
    pixel
    raster png
    zones
    background
BOTTOM
*/


/* region polygons
    (source added in html template) */
var regions = new ol.layer.Vector({
    zIndex: 6
});

/* grey background
    (source added in html template) */
var background = new ol.layer.Vector({
    style: new ol.style.Style({
        stroke: new ol.style.Stroke({ color: 'black' }),
        fill : new ol.style.Fill({ color: 'rgb(200,200,200)' })
})
});

var myview = new ol.View({ projection: 'EPSG:4326' });

var mymap = new ol.Map({
    controls: [], //ol.control.defaults({zoom:false}),
    target: 'map',
    layers: [ regions, background ],
    interactions: ol.interaction.defaults({dragPan:false}),
    view: myview,
});

mymap.addInteraction(new ol.interaction.DragPan({
    condition: function(e){
        return (e.originalEvent.which == 2);
    }
}))

/* HYDRO TEST */
//mymap.addLayer(pixels);

var lon, lat;

var onePixel = new ol.layer.Vector({
    style: new ol.style.Style({
        stroke: new ol.style.Stroke({ color: 'black', width: 3, lineDash: [4,8] })
    }),
    zIndex: 50,
    visible: false
});

var oneSelectedPixel = new ol.layer.Vector({
    style: new ol.style.Style({
        stroke: new ol.style.Stroke({ color: 'black', width: 3 })
    }),
    zIndex: 50,
    visible: false
});


var raster;

function trunc(x) { if (x<0) return Math.ceil(x); else return Math.floor(x);}

function setPixelAtCoords(coords){
    if (raster && ol.extent.containsCoordinate(raster.extent, coords)){
    var x = raster.extent[0] - trunc((raster.extent[0]-coords[0])/raster.xres)*raster.xres,
        y = raster.extent[1] - trunc((raster.extent[1]-coords[1])/raster.yres)*raster.yres;

        onePixel.setSource(new ol.source.Vector({
            features : [new ol.Feature(new ol.geom.Polygon([[
                [x,y], [x,y-raster.yres], [x+raster.xres,y-raster.yres], [x+raster.xres,y], [x,y]
            ]]))],
            projection: 'EPSG:4326'
            }));
            }

}

function onePixelFollow(evt){
    setPixelAtCoords(evt.coordinate)
}

mymap.addLayer(onePixel);
mymap.addLayer(oneSelectedPixel);
mymap.on('pointermove', onePixelFollow);


// HYDRO TEST
var pngLayer = new ol.layer.Image({zIndex: 4, visible:false});
mymap.addLayer(pngLayer);

function pixelize(evt) {
    /* turns off pixel smoothing */
    evt.context.imageSmoothingEnabled = false;
    evt.context.webkitImageSmoothingEnabled = false;
    evt.context.ImageSmoothingEnabled = false;
    evt.context.msImageSmoothingEnabled = false;
}

pngLayer.on('precompose', pixelize);
// END HYDRO TEST

/* poly styles */

var selectedPixel = new ol.style.Style({
        stroke: new ol.style.Stroke({ color: 'black', width: 4 })
})

function getStyle(feature) {
    if (!values) return emptyStyle(feature);
    return new ol.style.Style({
        stroke: new ol.style.Stroke({ color: 'black' }),
        fill : new ol.style.Fill({
            color: values[feature.get('name')].color
        })
    });
}

function emptyStyle(feature){
    // when rasters
    return new ol.style.Style({
        stroke: new ol.style.Stroke({ color: 'black' })
        });
}

function getHoverStyle(feature) {
    return new ol.style.Style({
        stroke: new ol.style.Stroke({
            width: 3,
            lineDash: [4,8]
        }),
        fill : new ol.style.Fill({
            color: values[feature.get('name')].color
        })
    });
}

function getClickStyle(feature) {
    return new ol.style.Style({
        stroke: new ol.style.Stroke({
            width: 5
        }),
        fill : new ol.style.Fill({
            color: values[feature.get('name')].color
        })
    });
}

function textStyle(feature){
    return new ol.style.Style({
        image: new ol.style.Circle({
            radius: 2,
            fill: new ol.style.Fill({color: 'black'}),
            stroke: new ol.style.Stroke({color: 'black', width: 1})
        }),
        text: new ol.style.Text({
            offsetY: -5,
            font: '12px sans-serif',
            textBaseline: 'bottom',
            text: feature.get('name'),
        })
    });
}

function placeLayerStyle(feature){
    return new ol.style.Style({
        image: new ol.style.Circle({
            radius: 4,
            fill: new ol.style.Fill({color: 'red'}),
            stroke: new ol.style.Stroke({color: 'white', width: 2})
        }),
        text: new ol.style.Text({
            font:'16px sans-serif',
            offsetY: 10,
            textBaseline: 'top',
            text: feature.get('name'),
            fill: new ol.style.Fill({color: 'red'}),
        })
    });
}

/* MAP INTERACTIONS */

var sel='Z' // 'Z' for zone, 'P' for point, 'S' for station

var viewBounds;
function fitView(){
    myview.fit(viewBounds, {constrainResolution:false});
}

function zoomOut(){
    sel = 'Z';
    lon = lat = undefined;
    fitView()
    if (!metric.raster_only){
        updateSelect('region');
        document.getElementById('regionselection').checked = true;
    }
    zoneClick.getFeatures().clear();
    updateAll();
}

/* region hover */
var regionHover = new ol.interaction.Select({
    condition: ol.events.condition.pointerMove,
    style: getHoverStyle,
    layers: [regions]
});

regionHover.on('select', function(e){
    var feat = e.selected[0];
    if (feat && values){
        document.getElementById('mapinfo').innerHTML = feat.get('name')+' : '+addPlus(values[feat.get('name')].value.toFixed(2))+' '+metric.unit;
    }
});

mymap.addInteraction(regionHover);

/* region click */
var zoneClick = new ol.interaction.Select({
    condition: ol.events.condition.click,
    style: getClickStyle,
    layers: [regions]
});

zoneClick.on('select', function(event){
    updateGraphs();
});

/* region drag-and-click */
var drag = new ol.interaction.DragBox();

drag.on('boxend', function(){
    zoneClick.getFeatures().clear();
//    pixel.setVisible(false);
    var extent = drag.getGeometry().getExtent();
    regions.getSource().forEachFeatureIntersectingExtent(extent, function(feat){
        zoneClick.getFeatures().push(feat);
    });
    updateGraphs();
});

mymap.addInteraction(drag);
mymap.addInteraction(zoneClick);

/* save the selection when changing anything */
var saved;
function saveSelection(){
    if ( sel=='Z' && zoneClick.getFeatures().getLength() ){
        saved = zoneClick.getFeatures().getArray().slice();
    }
    else saved = false;
}

function restoreSelection(){
    if ( sel=='Z' && saved ) saved.forEach(function (x) {zoneClick.getFeatures().push(x)});
}

/* stations */

var stations = new ol.layer.Vector({
	zIndex: 10,
	style: stationStyle,
	visible: false
    });

mymap.addLayer(stations);

var stationClick = new ol.interaction.Select({
    condition: ol.events.condition.click,
    layers: [stations],
});

var stationHover = new ol.interaction.Select({
    condition: ol.events.condition.pointerMove,
    style: stationHoverStyle,
    layers: [stations]
});

function goToStationPage(station){
    var pk = station.getProperties().pk;
    updateGraphs();
//    document.getElementById('stationLink').innerHTML = '<a href='+thisUrl+'../histo/station/'+pk+'#station>'+lastObservationText+'</a>';
}

stationClick.on('select', function(e){
    if (e && e.selected){
        var sel = e.selected[0];
        if (sel) goToStationPage(sel);
        }
});

function pixelClick(evt){
    oneSelectedPixel.setSource(onePixel.getSource());
    oneSelectedPixel.setVisible(true);
    var coords = evt.coordinate;
    lon = coords[0].toFixed(2);
    lat = coords[1].toFixed(2);
    var pix = mymap.getPixelFromCoordinate([ lon, lat ]);;
    updateGraphs();
}

/* selection update */
function updateSelect(s){
    saved = false;
    oneSelectedPixel.setVisible(false);
    mymap.removeInteraction(drag);
    mymap.removeInteraction(regionHover);
    mymap.removeInteraction(zoneClick);
    mymap.removeInteraction(stationClick);
    mymap.removeInteraction(stationHover);
    onePixel.setVisible(false);
    document.getElementById('mapinfo').innerHTML = '';
    placeLayer.setVisible(false);
    placeFinder.value='';
    placeFinder.style.display='none';
    stationFinder.style.display='none';
    stationFinder.value='';
    mymap.un('pointermove', onePixelFollow);
    mymap.un('singleclick', pixelClick);
    updateName('selection', s);
    if (s[0] === 'r'){
        sel = 'Z';
        placeFinder.style.display='inline-block';
        zoneClick.getFeatures().clear();
        stations.setVisible(false);
        mymap.addInteraction(drag);
        mymap.addInteraction(zoneClick);
        mymap.addInteraction(regionHover);
    } else if (s === 'pixel') {
        sel = 'P';
        onePixel.setVisible(true);
        mymap.on('pointermove', onePixelFollow);
        placeFinder.style.display='inline-block';
        stations.setVisible(false);
        mymap.on('singleclick', pixelClick);
    } else {
        sel = 'S';
        stationClick.getFeatures().clear();
        stationFinder.style.display='inline-block';
        document.getElementById('mapinfo').innerHTML =
            '<span style="color:'+climaticColor
            +'">climatic</span><br><span style="color:'+synopticColor
            +'">synoptic</span><br><span style="color:'+pluvioColor
            +'">pluvio</span>';
        stations.setVisible(true);
        mymap.addInteraction(stationClick);
        mymap.addInteraction(stationHover);
    }

}

/* TOP MENU */

/* models */
function allModels(toggle){
    var ml = document.getElementsByClassName('modelCheck');
    for (i=0;i<ml.length;i++)
        ml[i].checked = toggle;
    document.getElementById('modelsNb').innerHTML = toggle?ml.length:0;
    if (toggle) updateAll(); /* do not update if no models left */
}

function delModel(id){
    document.getElementById('modelall').checked = false;
    document.getElementById('model'+id).checked = false;
    updateModelList();
}

function updateModelList(){
    var ml = document.getElementsByClassName('modelCheck'),
        nb = 0;
    for (i=0;i<ml.length;i++)
        if (ml[i].checked) nb++;
    document.getElementById('modelsNb').innerHTML = nb;
    if (nb>0) updateAll(); /* same as above */
}

/* menu updates :
    various functions depending on what needs actually be updated.
*/
function updateName(prefix, s){
    // only menu title
    document.getElementById(prefix+'Name').innerHTML = s;
}

function updateDrop(prefix, s){
    // menu title + map + graphs
    updateName(prefix, s);
    updateAll();
}

function updateMetricDrop(pk, name){
    for (var i=0; i<allMetrics.length; i++)
        if (pk == allMetrics[i].pk){
            var met = allMetrics[i].fields;
            if (met.raster_only) {
                updateSelect('pixel');
                document.getElementById('pixelselection').checked = true;
            }
            else {
                updateSelect('region');
                document.getElementById('regionselection').checked = true;
            }
            break;
        }
    updateDrop('param', name);
}

function updateDropNoMap(prefix, s){
    // menu title + map + graphs
    updateName(prefix, s);
    updateGraphs();
}


function updateCustom(){
    if (getMetric() == '-1'){
        document.getElementById('regionselection').checked = true;
        updateSelect('region');
        var form = document.getElementById('topForm');
        var param = form.formulaAgg.value+'('+form.formulaText.value+')';
        updateDrop('param', param)
    }
}

/* time-related updates */
var months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul',
      	       'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    fullmonths = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July',
      		   'August', 'September', 'October', 'November', 'December'];


function getMonthsName(){
    var fmonths = topForm.months.value;
    if(fmonths.indexOf('-') == -1)
        return months[parseInt(fmonths)-1]
    else if (fmonths != '1-12') {
        m = fmonths.split('-');
        return months[parseInt(m[0])-1]+'-'+months[parseInt(m[1])-1];
    }
}

function updateTime(form){
    updateAll();

    var time = form.years.value,
        mon = getMonthsName()
    if (mon) time = mon + ' ' + time;
    document.getElementById('timeDisplay').innerHTML = time;
}

function useRangeBounds(){
    var ywin = yeargraph.dateWindow_;
    ywin = [parseInt(ywin[0]), parseInt(ywin[1])];
    topForm.years.value = ywin.join('-');
    updateTime(topForm);
}

/* CHARTS */

/* global variables */
var monthgraph, yeargraph;

function toggleVisible(l){
    var i = yeargraph.indexFromSetName(l)-1,
        a = document.getElementById('eye'+l);

    var v = yeargraph.visibility()[ i ];
    yeargraph.setVisibility(i, !v);
    if (monthgraph) monthgraph.setVisibility(i, !v);
    a.src = v?img_eye_off:img_eye;
}

function toggleAll(){
    // toggle "all" models visibility
    var yl = yeargraph.getLabels();
    yl.shift();
    yl.forEach(function (l){
        toggleVisible(l);
    });
}

function displayLabels(divname, labels){
    /* Displays graph labels (for both graphs) in a separated div */

    var col, ldiv = document.getElementById(divname);
    /* buffer the string because browsers auto-close tags like <ul> */
    var buffer = '<table><tr><td></td><td><img onclick="toggleAll()" style="cursor:pointer;height:1em" src="'+img_toggle+'"></td></tr>';
    labels.forEach(function(l){
        col = graph_color[l];
        if (col) {
        buffer += '<tr><td style="color:'+col.color+'">'
        +generateLegendDashHTML(col.strokePattern, col.color, 5)+l+'</td>';
        buffer += ' <td><img alt="toggle visibility" id="eye'+l+'" onclick="toggleVisible(\''+l+'\')" style="cursor:pointer;height:1em" src="'+img_eye+'">';
        if (col.id)
            buffer +='</td><td><img alt="remove" onclick="delModel('+col.id+')" style="cursor:pointer;height:1em" src="'+img_X+'">';
        buffer += '</td></tr>';
        }
    });
    ldiv.innerHTML = buffer + '</table>';
}

/*  Line display for graphs, directly borrowed from dygraphs.js */
function generateLegendDashHTML(strokePattern, color, oneEmWidth) {

  // Easy, common case: a solid line
  if (!strokePattern || strokePattern.length <= 1) {
    return "<div class=\"dygraph-legend-line\" style=\"border-bottom-color: " + color + ";\"></div>";
  }

  var i, j, paddingLeft, marginRight;
  var strokePixelLength = 0,
      segmentLoop = 0;
  var normalizedPattern = [];
  var loop;

  // Compute the length of the pixels including the first segment twice,
  // since we repeat it.
  for (i = 0; i <= strokePattern.length; i++) {
    strokePixelLength += strokePattern[i % strokePattern.length];
  }

  // See if we can loop the pattern by itself at least twice.
  loop = Math.floor(oneEmWidth / (strokePixelLength - strokePattern[0]));
  if (loop > 1) {
    // This pattern fits at least two times, no scaling just convert to em;
    for (i = 0; i < strokePattern.length; i++) {
      normalizedPattern[i] = strokePattern[i] / oneEmWidth;
    }
    // Since we are repeating the pattern, we don't worry about repeating the
    // first segment in one draw.
    segmentLoop = normalizedPattern.length;
  } else {
    // If the pattern doesn't fit in the legend we scale it to fit.
    loop = 1;
    for (i = 0; i < strokePattern.length; i++) {
      normalizedPattern[i] = strokePattern[i] / strokePixelLength;
    }
    // For the scaled patterns we do redraw the first segment.
    segmentLoop = normalizedPattern.length + 1;
  }

  // Now make the pattern.
  var dash = "";
  for (j = 0; j < loop; j++) {
    for (i = 0; i < segmentLoop; i += 2) {
      // The padding is the drawn segment.
      paddingLeft = normalizedPattern[i % normalizedPattern.length];
      if (i < strokePattern.length) {
        // The margin is the space segment.
        marginRight = normalizedPattern[(i + 1) % normalizedPattern.length];
      } else {
        // The repeated first segment has no right margin.
        marginRight = 0;
      }
      dash += "<div class=\"dygraph-legend-dash\" style=\"margin-right: " + marginRight + "em; padding-left: " + paddingLeft + "em;\"></div>";
    }
  }
  return dash;
};


/* DISPLAY */

function getArgs(){
    /* parse all input and returns a GET request */
    var form = document.getElementById('topForm'),
        args = '?compare='+getRadio('compare'),
        met = getRadio('metric'),
        zone;

    args += '&metric='+met;
    if (met == -1)
        args += '&formula='+escape(form.formulaText.value)
                + '&agg='+form.formulaAgg.value
                + '&cmap='+form.formulaCmap.value;

    var i, ml = document.getElementsByClassName('modelCheck');

    /* computes a list of models, leaving blank when all selected */
    var allmods = true, modargs = [];
    for (i=0;i<ml.length;i++){
        if (ml[i].checked) modargs=modargs.concat(ml[i].value);
        else allmods = false;
    }
    if (!allmods)
        args += '&models='+modargs.join(',');

    if (sel=='S' && stationClick.getFeatures().getLength()){
        var s = stationClick.getFeatures().item(0).getProperties();
        zone = s.name+' (station)';
        args += '&station='+s.pk;
    } else if (sel=='P') {
        args += '&showraster';
        if (lon && lat){
            zone = '('+lon+','+lat+')';
            args += '&lon='+lon+'&lat='+lat;
        }
    } else {
        zone = getZone();
    }

    args += '&scen='+getRadio('scenario');
    if (form.months.value && form.months.value != '1-12') args += '&months='+form.months.value;
    if (form.years.value) args += '&yrs='+form.years.value;
    if (form.rolling.value!='1') args += '&rolling='+form.rolling.value;

    var data = getRadio('data');
    if (data != 'data')
        args += '&data='+data + '&refyrs='+form.refYrs.value;

//    if(document.getElementById('showraster').checked) args += '&showraster';
    if (zone)
        args+='&zone='+zone;
    return args
}


/* Value formatters for dygraphs.
   One of each { errorbars, no error bars } x { with roller (for year), without roller (for month) }.
 */
var ROLLING_SEMIPERIOD = 15

function valueFormat(x){
    return x.toFixed(2);
}
function errorBarValueFormat(x, opts, series, graph, row, col){
    return x.toFixed(2) + ' &#177;'+graph.getValue(row, col)[1].toFixed(2);
}

function valueRollFormat(x, opts, series, graph, row, col){
    var i, i_0 = Math.max(0,row-ROLLING_SEMIPERIOD),
        i_n = Math.min(row+ROLLING_SEMIPERIOD, graph.numRows()),
        sum = 0, n = 0;
    for (i=i_0;i<i_n;i++){
        n += 1;
        sum += graph.getValue(i, col);
    }
    return x.toFixed(2) + '<br>['+(sum / n).toFixed(2)+' on 30 yrs]' ;
}

function errorBarValueRollFormat(x, opts, series, graph, row, col){
    var i, i_0 = Math.max(0,row-ROLLING_SEMIPERIOD),
        i_n = Math.min(row+ROLLING_SEMIPERIOD, graph.numRows()),
        sum = 0, n = 0;
    for (i=i_0;i<i_n;i++){
        n += 1;
        sum += graph.getValue(i, col)[0];
    }
    return x.toFixed(2) + ' &#177;'+graph.getValue(row,col)[1].toFixed(2)+'<br>['+(sum / n).toFixed(2)+' on 30 yrs]' ;
}


function getZone(){
    /* joins selected regions */
    if (sel=='P')
        return '('+lon+','+lat+')';

    if (sel=='S' && stationClick.getFeatures().getLength()){
        var s = stationClick.getFeatures().item(0).getProperties();
        return s.name+' (station)';
    }

    if (zoneClick.getFeatures().getArray().length != 0){
        var namelist = [];
        zoneClick.getFeatures().forEach(function(feat){
            namelist.push(feat.get('name'));
        });
        return namelist.join(', ');
    }
    return main_zone;
}

/* Shortcuts. */
function updateGraphs(){
    fetchData('ys');
}

function updateMap(){
    fetchData('m');
}

function updateAll(){
    /*  Main refreshing function. Used when changing about any parameter. */
    fetchData('mys');
}

function addPlus(value){
    if (value>0) return metric.plus+value;
    return value;
}

function putScale(scale){
    var i;
    for(i=0;i<scale[0].length;i++){
        document.getElementById('scale'+i).innerHTML = addPlus(scale[0][i].toFixed(2))+metric.unit;
        document.getElementById('scale'+i+'c').style.backgroundColor = scale[1][i];
    }
}

function fetchData(mys){
    /* Main fetching function. Launches an XMLHttpRequest for JSON containing
       dygraphs data/meta, and map colors/values.

    @param mys : string containing one or more of 'm', 'y', 's' :
        'm' (map),
        's' (season, aka month),
        'y' (year).
    */
    var args = getArgs();
    document.getElementById('wait').style.display = 'inline';
    document.getElementById('error').innerHTML = '';
//    document.getElementById('rightTitleZone').innerHTML = getZone();

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (this.readyState == XMLHttpRequest.DONE) {
         if (this.status == 200) {
            var j = JSON.parse(this.responseText);

            metric = j.metric;
            document.getElementById('monthUnit').innerHTML = j.metric.unit;
            document.getElementById('yearUnit').innerHTML = j.metric.unit;
            document.getElementById('monthChartContainer').style.visibility = metric.monthly?'visible':'hidden';
            document.getElementById('month_export').style.display = metric.monthly?'block':'none';

            document.getElementById('regionselection').disabled = Boolean(j.metric.raster_only);
            document.getElementById('regionselection').nextElementSibling.style.color = (j.metric.raster_only?'gray':'black');
            document.getElementById('pixelselection').disabled = !Boolean(j.metric.raster);
            document.getElementById('pixelselection').nextElementSibling.style.color = (j.metric.raster?'black':'gray');
            document.getElementById('stationselection').disabled = !Boolean(j.metric.formula);
            document.getElementById('stationselection').nextElementSibling.style.color = (j.metric.formula?'black':'gray');

            if (j.zone && (mys!='m'))
                document.getElementById('rightTitleZone').innerHTML = j.zone;

            if (j.error) document.getElementById('error').innerHTML  = j.error;
            if (mys.indexOf('m')>=0){
                if (j.map){
                    saveSelection();
                    values = j.map.values;
                    zoneClick.getFeatures().clear();
                    document.getElementById('mapTitle').innerHTML = j.map.title;
                    regions.setStyle(getStyle);
                    putScale(j.map.scale);
                    restoreSelection();
                }

                if (j.raster){
                    raster = j.raster;
                    pngLayer.setSource(new ol.source.ImageStatic({
                        imageExtent: j.raster.extent,
                        projection: 'EPSG:4326',
                        imageLoadFunction : function(image){image.getImage().src = j.raster.image }
                    }));
                    putScale(j.raster.scale);
                    regions.setStyle(emptyStyle);
                    pngLayer.setVisible(true);
                } else {
                    pngLayer.setVisible(false);
                    regions.setStyle(getStyle);
                }
            }
            if (j.point)
                document.getElementById('mapinfo').innerHTML = j.point;

            if (mys.indexOf('s')>=0){
            if (j.month){
                document.getElementById('monthChartContainer').style.visibility = 'visible';
                if (monthgraph) monthgraph.destroy();
                j.month.meta.axes.y = {
                    axisLabelFormatter : addPlus,
                    valueFormatter : (j.cScen?errorBarValueFormat:valueFormat)
                }
                j.month.meta.axes.x =  {
                    axisLabelWidth: 70,
                    pixelsPerLabel: 30,
                    axisLabelFormatter: function(d) { return months[d-1] },
                    valueFormatter: function(d) { return months[d-1] }
                }
                monthgraph = new Dygraph(monthChart, j.month.data, j.month.meta);
            } else document.getElementById('monthChartContainer').style.visibility = 'hidden';
            }

            if(mys.indexOf('y')>=0){
            if (j.year){
                document.getElementById('yearChartContainer').style.visibility = 'visible';
                if (yeargraph) yeargraph.destroy();
                var vf;
                if (topForm.rolling.value=='1') vf = (j.cScen?errorBarValueRollFormat:valueRollFormat);
                else vf = (j.cScen?errorBarValueFormat:valueFormat);

                j.year.meta.axes.y = {
                    axisLabelFormatter : addPlus,
                    valueFormatter : vf,
                }
                j.year.meta.interactionModel = Dygraph.defaultInteractionModel;
                yeargraph = new Dygraph(yearChart, j.year.data, j.year.meta);
                yeargraph.ready(function(){
                        var labels = yeargraph.getLabels();
                       // remove the first x-axis element
                        labels.shift();
                        displayLabels('graphLegend', labels);
                        });
            } else document.getElementById('yearChartContainer').style.visibility = 'hidden';
            }
        } else {
            document.getElementById('error').innerHTML = '<ul><li class="errE">'+serverError+'</li></ul>';
        }
      document.getElementById('wait').style.display = 'none';
      }
    }
    xhr.open('GET',mys+'.json'+args, true);
    xhr.send(null);

                /* export */
            document.getElementById('year_csv').href = 'year.csv'+args;
            document.getElementById('year_xls').href = 'year.xls'+args;
            document.getElementById('month_csv').href = 'month.csv'+args;
            document.getElementById('month_xls').href = 'month.xls'+args;


}

/* EXPORT */

function download(filename, url) {
    /* fires the actual downloading */
    var element = document.createElement('a');
    element.setAttribute('href', url);
    element.setAttribute('download', filename);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
}

function getVisibilities(){
    /* Returns a string like 'TTTFFFTTTF' coding visible series. */
    var v = yeargraph.visibility(),
        r = '';

    if (v.some(function(x){return !x})){
        r = '&visible='
        for (i=0;i<v.length;i++)
            r += v[i]?'T':'F';
    }
    return r;
}

function exportAll(extension){
    /* fires exportation an image of chosen extension (png, svg, pdf) */
    var filename = 'export.'+extension,
        args = getArgs() + getVisibilities();
    var yw = yeargraph.dateWindow_ ;
    if (yw) args += '&yw='+parseInt(yw[0])+'-'+parseInt(yw[1]);

    download(filename, filename+args);
}

/* AUTOCOMPLETE */

var iconFeature = new ol.Feature({
    geometry: new ol.geom.Point([-16,16]),
    name : 'bla'
});

var placeLayer = new ol.layer.Vector({
	source: new ol.source.Vector({
	    features: [iconFeature],
	    projection: 'EPSG:4326'
	    }),
	zIndex: Infinity,
	style: placeLayerStyle,
	map: mymap,
	visible: false
    });
mymap.addLayer(placeLayer);


function findAuto(name){
    if (name==''){
        placeLayer.setVisible(false);
        return;
    }

    if (sel == 'S'){
        for(i=0;i<stationFeatures.length;i++){
            if (stationFeatures[i].getProperties().name === name){
                stationClick.getFeatures().clear();
                stationClick.getFeatures().push(stationFeatures[i]);
                goToStationPage(stationFeatures[i]);
                break;
            }
        }
        updateGraphs();
    } else {

    for(i=0;i<allplaces.features.length;i++)
        if(allplaces.features[i].properties.name == name){
            var feat = allplaces.features[i];
            //console.log(feat);
            lon = feat.properties.lon.toFixed(3);
            lat = feat.properties.lat.toFixed(3);
            var pix = mymap.getPixelFromCoordinate([ lon, lat ]);

            if (sel == 'Z'){
                zoneClick.getFeatures().clear();
                mymap.forEachFeatureAtPixel(pix, function(f, l) {if (l==regions) {zoneClick.getFeatures().push(f)}},
                                            {hitTolerance: 5});
            } else if (sel == 'P') {
                setPixelAtCoords([lon, lat]);
                oneSelectedPixel.setSource(onePixel.getSource());
                oneSelectedPixel.setVisible(true);
            }
            updateGraphs();

                placeLayer.setVisible(true);
                iconFeature.setProperties({
                    geometry: new ol.geom.Point([lon, lat]),
                    name : feat.properties.name
                });

            break;
        }
    }
}
