/* CONSTANTS */

var climaticColor = 'red',
    synopticColor = 'green',
    pluvioColor = 'blue';
var circleSize = 5,
    hoverCircleSize = 10;

var climatic = new ol.style.Style({
      image: new ol.style.Circle({
        radius: circleSize,
        fill: new ol.style.Fill({color: climaticColor}),
        stroke: new ol.style.Stroke({ color: 'black' })
      })
    }),
    pluvio = new ol.style.Style({
      image: new ol.style.Circle({
        radius: circleSize,
        fill: new ol.style.Fill({color: pluvioColor}),
        stroke: new ol.style.Stroke({ color: 'black' })
      })
    }),
    synoptic = new ol.style.Style({
      image: new ol.style.Circle({
        radius: circleSize,
        fill: new ol.style.Fill({color: synopticColor}),
        stroke: new ol.style.Stroke({ color: 'black' })
      })
    });



function stationStyle(feature){
    if(feature.get('type') == 'climatic')
        return climatic;
    else if(feature.get('type') == 'synoptic')
        return synoptic;
    return pluvio;
}

function stationHoverStyle(feature){
    var type = feature.get('type');
    if (type == 'climatic') col = climaticColor;
    else if (type == 'synoptic') col = synopticColor;
    else col = pluvioColor;

    return new ol.style.Style({
      image: new ol.style.Circle({
        radius: hoverCircleSize,
        fill: new ol.style.Fill({color: col}),
        stroke: new ol.style.Stroke({ color: 'black' })
      }),
    text: new ol.style.Text({
            textBaseline: 'top',
            offsetY: 10,
            scale: 1.5,
            fill: new ol.style.Fill({color: col}),
            text: feature.get('name')
        })
    });
}
