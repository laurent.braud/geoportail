from django.urls import path, register_converter
from django.views.generic import TemplateView
from . import views
from .export import image
from .netcdf import write_netcdf
from .maintenance import print_zd, del_zd, del_mrd, print_sp, del_sp, display_station_proj, print_param, del_param, listings

app_name = 'proj'

class Extension:
    regex = '[a-z]{2,3}'

    def to_python(self, value):
        return str(value)

    to_url = to_python

register_converter(Extension, 'xxx')

urlpatterns = [
    # static
    path('about', views.about, name='about'),
    path('help', views.help, name='help'),
    path('formula', views.formula, name='formula'),
    path('download', views.download, name='download'),
    path('faq', TemplateView.as_view(template_name='proj/faq.html'), name='faq'),

    path ('downparam/<pr>_<scen>_<zone>.csv', views.param_download, name='param_download'),
    path('downmetric/<met>_<scen>_<model>.csv', views.metric_download, name='metric_download'),
    path('downmetric/<met>_<scen>.csv', views.metric_download, name='metric_download'),
    path('downmetric/<metric>_<scen>_<model>.nc', write_netcdf, name='netcdf'),
    path('downzone/<met>_<scen>_<zone>.csv', views.zone_metric_download, name='zone_download'),

    # json / export
    path('<update>.json', views.jsoner, name='jsoner'),
    path('<period>.csv', views.csv_export, name='csv'),
    path('<period>.xls', views.xls_export, name='xls'),
    path('export.<xxx:extension>', image, name='plt'),

    # language toggle
    path('lang/<l>', views.lang, name='lang'),

    # main page
    path('simple', views.simple, name='simple'),
    path('', views.index, name='index'),

    # admin
    path('listings', listings, name='listings'),
    path('mod_zd', print_zd, name='print_zd'),
    path('mod_param', print_param, name='print_param'),
    path('mod_sp', print_sp, name='print_sp'),
    path('stations', display_station_proj, name='display_station'),
    path('del_zd/<mid>/<modname>/<scen>', del_zd, name='del_zd_scen'),
    path('del_zd/<mid>/<modname>', del_zd, name='del_zd'),
    path('del_param/<pid>/<modname>/<scen>', del_param, name='del_param_scen'),
    path('del_param/<pid>/<modname>', del_param, name='del_param'),
    path('del_mrd/<mid>/<modname>/<scen>', del_mrd, name='del_mrd_scen'),
    path('del_mrd/<mid>/<modname>', del_mrd, name='del_mrd'),
    path('del_sp/<modname>/<pid>/<sid>/<scen>', del_sp, name='del_sp_scen'),
    path('del_sp/<modname>/<pid>/<sid>/', del_sp, name='del_sp'),
    ]