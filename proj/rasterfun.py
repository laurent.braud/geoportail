"""
Postgis primitive used to extract value from raster data at a given point.

Example :

class RasterData
    raster = models.RasterField()

qset = RasterData.objects.all()
qset.annotate(point=exact_point(lon,lat)).annotate(value=Value('raster', 'point'))
"""

from django.contrib.gis.db import models
from django.db.models import Func
from django.db.models.expressions import RawSQL

def exact_point(lon, lat):
    return RawSQL("select ST_SetSRID(ST_Point(%s,%s),4326)",
                  (lon, lat),
                  output_field=models.PointField())

class Value(Func):
    output_field = models.FloatField()
    function = 'ST_Value'
    arity = 2
