"""
Administration
"""

import os
from django.http import HttpResponseRedirect
from django import forms
from django.conf import settings
from django.shortcuts import render, reverse
from django.contrib.admin.views.decorators import staff_member_required

from . import models as pm

# forms
FILENAME = 'pr_EWEMBI_1979-2013_0.5x0.5_day_{model}_senegal_{scenario}_r1i1p1_full.nc'

@staff_member_required
def listings(request):
    return render(request, 'admin/listings.html')

@staff_member_required
def print_zd(request):
    mid = request.GET.get('metric', None)
    metric = pm.Metric.objects.first() if mid is None else pm.Metric.objects.get(id=mid)

    scenlist= pm.Scenario.objects.all().values_list('name', flat=True)
    cols = {}

    zd = metric.zoneddata_set.first()
    if zd:
        for s in scenlist:
            for cm in zd.get_data(s).columns:
                if cm in cols:
                    cols[cm]['zd'].append(s)
                else:
                    cols[cm] = {'zd': [s]}

    qset = metric.monthlyrasterdata_set.values_list('model__name', 'scenario__name').distinct()
    if qset:
        for cm, s in qset:
            if cm in cols:
                if 'mrd' in cols[cm]:
                    cols[cm]['mrd'].append(s)
                else:
                    cols[cm]['mrd'] = [s]
            else:
                cols[cm] = {'mrd': [s]}

    context = {
        'scenlist': scenlist,
        'allmetrics': pm.Metric.objects.all(),
        'metric': metric,
        'cols': sorted(cols.items(), key=lambda x: x[0].lower()),
        'dir': settings.HDF5DIR,
    }
    return render(request, 'admin/mod_zd.html', context)

@staff_member_required
def print_param(request):
    pid = request.GET.get('param', None)
    param = pm.Parameter.objects.get(id=pid) if pid else pm.Parameter.objects.first()
    cols = {}
    scenlist= pm.Scenario.objects.all().values_list('name', flat=True)

    drd = param.dailyregiondata_set.first()
    if drd:
        for s in scenlist:
            for cm in drd.read_data(s).columns:
                if cm in cols:
                    cols[cm].append(s)
                else:
                    cols[cm] = [s]

    context = {
        'scenlist': scenlist,
        'allparams': pm.Parameter.objects.all(),
        'param': param,
        'cols': sorted(cols.items(), key=lambda x: x[0].lower()),
        'dir': settings.HDF5DIR,
    }
    return render(request, 'admin/mod_param.html', context)

@staff_member_required
def del_param(request, pid, modname, scen=None):
    param = pm.Parameter.objects.get(id=pid)
    for drd in param.dailyregiondata_set.all():
        remove_model_from_file(drd.file, modname, scen)
    return HttpResponseRedirect(reverse('proj:print_param') + '?param={}'.format(pid))

@staff_member_required
def print_sp(request):
    pid = request.GET.get('param', None)
    param = pm.Parameter.objects.get(id=pid) if pid else pm.Parameter.objects.first()
    sid = request.GET.get('station', None)
    station = pm.Station.objects.get(id=sid) if sid else pm.Station.objects.first()
    scenlist= pm.Scenario.objects.all().values_list('name', flat=True)
    cols = {}

    qset = station.stationParam_set.filter(param=param)
    if qset: # should be try/except
        sp = qset.first()
        print('Opening file {} (sp {})'.format(sp.file, sp.id))
        for s in scenlist:
            for cm in sp.read_rcp(s).columns:
                if cm in cols:
                    cols[cm].append(s)
                else:
                    cols[cm] = [s]


    pm.StationParam.objects.filter()

    context = {
        'param': param,
        'station': station,
        'allparams': pm.Parameter.objects.all(),
        'allstations': pm.Station.objects.all(),
        'scenlist': scenlist,
        'cols': sorted(cols.items(), key=lambda x: x[0].lower()),
        'dir': settings.HDF5DIR,
    }
    return render(request, 'admin/mod_sp.html', context)

@staff_member_required
def display_station_proj(request):
    return render(request, 'admin/display_station_proj.html', { 'stations' : pm.Station.objects.all() })

from tempfile import mkstemp

def remove_model_from_file(hdffile, modname, scen=None):
    """If scen is None, removes the model from all scenarii.
    Otherwise, only from the given scenario."""
    tmpfile = hdffile + '.tmp'
    # fd, tmpfile = mkstemp(suffix='.h5')
    scenlist = pm.Scenario.objects.all().values_list('name', flat=True)
    for s in scenlist:
        with HDFStore(hdffile, 'r') as store:
            if s in store:
                df = store[s]
                if (scen is None or s == scen) and modname in df:
                    del df[modname]
                if not df.empty:
                    df.to_hdf(tmpfile, s)
    # os.close(fd)
    if os.path.exists(tmpfile):
        os.rename(tmpfile, hdffile)


@staff_member_required
def del_zd(request, mid, modname, scen=None):
    """This function will delete a model from a ZonedData.
    Because HDF5 are not really editable, we have to make another file,
    then overwrite the previous one."""
    tmpfile = '/tmp/tmp.h5'
    scenlist = list(pm.Scenario.objects.all().values_list('name', flat=True))
    for z in pm.Zone.objects.all():
        zd = pm.ZonedData.objects.get(metric__id=mid, zone=z)
        remove_model_from_file(zd.file, modname, scen=scen)
        # for s in scenlist:
        #     df = zd.get_data(s)
        #     if (scen is None or s == scen) and modname in df:
        #         del df[modname]
        #     if not df.empty:
        #         df.to_hdf(tmpfile, s)
        # if os.path.exists(tmpfile):
        #     # user is responsible for deleting the file
        #     os.rename(tmpfile, zd.file)
    return HttpResponseRedirect(reverse('proj:print_zd') + '?metric={}'.format(mid))

@staff_member_required
def del_mrd(request, mid, modname, scen=None):
    if scen:
        pm.MonthlyRasterData.objects.filter(metric__id=mid, model__name=modname, scenario__name=scen).delete()
    else:
        pm.MonthlyRasterData.objects.filter(metric__id=mid, model__name=modname).delete()
    return HttpResponseRedirect(reverse('proj:print_zd') + '?metric={}'.format(mid))

from pandas import HDFStore

@staff_member_required
def del_sp(request, modname, pid, sid=-1, scen=None):
    tmpfile = '/tmp/tmp.h5'
    sid = int(sid)
    scenlist = list(pm.Scenario.objects.all().values_list('name', flat=True))
    if sid >= 0 :
        # actually singleton
        splist = pm.StationParam.objects.filter(param__id=pid, station__id=sid)
    else:
        splist = pm.StationParam.objects.filter(param__id=pid)
    for sp in splist:
        for s in scenlist:
            with HDFStore(sp.file, 'r') as store:
                if s in store:
                    df = sp.read_rcp(s)
                    if (scen is None or s == scen) and modname in df:
                        del df[modname]
                    if not df.empty:
                        df.to_hdf(tmpfile, s)
        if os.path.exists(tmpfile):
            os.rename(tmpfile, sp.file)
    if sid >= 0:
        return HttpResponseRedirect(reverse('proj:print_sp') + '?param={}&station={}'.format(pid, sid))
    else:
        return HttpResponseRedirect(reverse('proj:print_sp') + '?param={}'.format(pid))

