"""
Proj constants.
"""

from django.conf import settings

DEFAULT_YRS = '2051-2080'
DEFAULT_REFYRS = '1981-2010'
DEFAULT_METRIC = 'average temperature'
DEFAULT_SCENARIO = settings.DEFAULT_SCENARIO

# will round csv file to the given number
ROUND_CSV = 3
# web round
ROUND = 2

MONTHS = ('January', 'February', 'March', 'April', 'May', 'June', 'July',
          'August', 'September', 'October', 'November', 'December')
MONTHS_FR= ('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet',
          'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre')
SHORTMONTHS = [m[:3] for m in MONTHS]
SHORTMONTHS_FR = ['Jan', 'Fév', 'Mars', 'Avr', 'Mai', 'Juin', 'Juil',
                  'Août', 'Sept', 'Oct', 'Nov', 'Déc']

DYGRAPH_STROKE = {'solid': [], 'dashed': [7, 2], 'dashdot': [7, 2, 2, 2], 'dotted': [2, 2]}

MONTH_NC = 'mon_calc_{model}_{scenario}_{param}.nc'
YEAR_NC = 'yr_calc_{model}_{scenario}_{param}.nc'

RASTER_BOX = settings.RASTER_BOX
YEAR_RANGE = settings.YEAR_RANGE
TOTAL_YEARS = YEAR_RANGE[1] - YEAR_RANGE[0]
TOTAL_MONTHS = 12*TOTAL_YEARS