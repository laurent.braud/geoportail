from django.test import TestCase

# Create your tests here.

from django.http import HttpRequest
from django.contrib.auth.models import AnonymousUser


req = HttpRequest()
req.GET.update({
    'metric': 6,
    'scen': 'rcp45',
    'yrs': "2051-2090",
    'zone': 'Matam, Dakar',
    'compare': 'scenarii',
    'data': 'absolute',
    # 'data': 'data',
    'models': '31,32,33,34,35,36,37,38,39,40,41,43,44,45,46,47,48,49,50,51,52,56,57,58,59,60',
})
req.user = AnonymousUser()

freq = HttpRequest()
freq.GET.update({
    'metric': '-1',
    'formula': 'pr[pr>=3]',
    'agg': 'count',
    'scen': 'rcp45',
    'yrs': "2051-2090",
    'zone': 'Matam, Dakar',
    'compare': 'scenarii',
    'data': 'absolute',
    # 'data': 'data',
    'models': '31,32,33,34,35,36,37,38,39,40,41,43,44,45,46,47,48,49,50,51,52,56,57,58,59,60',
})
freq.user = AnonymousUser()

sreq = HttpRequest()
sreq.GET.update({
    'metric': '6',
    'station': '151',
    'scen': 'rcp45',
    'yrs': "2051-2090",
    'zone': 'Matam, Dakar',
    'compare': 'scenarii',
    'data': 'data',
    'models': '31,32,33,34,35,36,37,38,39,40,41,43,44,45,46,47,48,49,50,51,52,56,57,58,59,60',
})
sreq.user = AnonymousUser()

example_qry = 'http://127.0.0.1:8000/proj/ys.json?compare=scenarii&metric=7&models=32&station=151&scen=rcp45&yrs=2051-2080&data=absolute&refyrs=1981-2010&zone=Linguere%20(station)'

def qry2req(s):
    l = (s.split('?')[1]).split('&')
    if 'showraster' in l:
        l.remove('showraster')
        l.append('showraster=True')
    d = { k:v for k,v in map((lambda x:x.split('=')), l) }
    req = HttpRequest()
    req.user = AnonymousUser()
    req.GET.update(d)
    return req

