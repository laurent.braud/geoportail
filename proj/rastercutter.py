#! /usr/bin/python3
# -*- coding: utf-8 -*-
#******************************************************************************
#  Author : Laurent Braud
#
#  Cuts a netcdf raster along polygons and returns average for each polygon in a
#  csv file. Pixels are weighted by area contained in the polygon.
#
#  Takes two inputs :
#  - A netcdf file containing exactly 4 variables, including 'time', 'lon', 'lat'
#    and the parameter. For best performance, the raster should be clipped to
#    the extent of the shapefile (or slightly larger).
#  - A shape file (.shp) containing exactly one layer of polygons.
#
#******************************************************************************


# disaggregation factor for calculating polygon area in raster
DISAGGREG = 50

# default fields in shp files
DEFAULT_NAMEFIELD = 'name'
DEFAULT_IDFIELD = 'OBJECTID'

import sys
from netCDF4 import Dataset, num2date
import gdal, ogr, osr
import numpy as np
import pandas as pd

def newRaster(x0, xn, xres, y0, yn, yres):
    MEMDRIVER = gdal.GetDriverByName('MEM')
    outRaster = MEMDRIVER.Create('', int((xn-x0)/xres), int((yn-y0)/yres), 1, gdal.GDT_Float64)
    outRaster.SetGeoTransform((x0,xres,0,y0,0,yres))
    outRasterSRS = osr.SpatialReference()
    outRasterSRS.ImportFromEPSG(4326)
    outRaster.SetProjection(outRasterSRS.ExportToWkt())
    return outRaster


def polygonize(ncfile, shpfile, param=None,
               nameField=DEFAULT_NAMEFIELD, idField=DEFAULT_IDFIELD,
               verbose=False, avoid_360=False):
    """param : string"""
    ds = Dataset(ncfile)

    # find parameter name
    if not param :
        if len(ds.variables) != 4:
            print("Too many parameters in netcdf file.")
            sys.exit(1)
        for v in ds.variables:
            if v not in [ 'time', 'lon', 'lat' ]:
                param = str(v)
                break
    if verbose:
        print("Extracting variable {}\n{}".format(param, ds[param]))

    # builds calendar
    cal, units= ds['time'].calendar, ds['time'].units
    if avoid_360 and '360' in cal:
        # TODO
        raise IndexError('360 calendar not supported.')
    dates = map(lambda t:pd.Timestamp(num2date(t, calendar=cal, units=units).strftime('%Y-%m-%d')), ds['time'][:])
    df = pd.DataFrame(index=dates)
    df.index.name = 'date'

    # get raster extent
    x0, xn, xres = np.min(ds['lon']), np.max(ds['lon']), abs(ds['lon'][1]-ds['lon'][0])
    y0, yn, yres = np.min(ds['lat']), np.max(ds['lat']), abs(ds['lat'][1]-ds['lat'][0])
    x0 -= xres/2
    y0 -= yres / 2
    xn += xres / 2
    yn += yres / 2

    # netcdf values
    dsa = ds[param][:]

    # read shpfile
    SHPDRIVER = ogr.GetDriverByName('ESRI Shapefile')
    dataSource = SHPDRIVER.Open(shpfile, 0) # 0 means read-only. 1 means writeable.
    layer = dataSource.GetLayer()
    
    for f in layer :
        if verbose:
            print(f.GetField(nameField), end=' ')
        
        large_raster = newRaster(x0, xn, xres/DISAGGREG, y0, yn, yres/DISAGGREG)
        weights = newRaster(x0, xn, xres, y0, yn, yres)
        
        where = '{}={}'.format(idField, f.GetField(idField))
        options = gdal.RasterizeOptions(allTouched=True, burnValues=1, where=where)

        gdal.Rasterize(large_raster, shpfile, options=options)
        gdal.Warp(weights, large_raster, options=gdal.WarpOptions(resampleAlg='average'))

        # weighted average
        a = weights.GetRasterBand(1).ReadAsArray()
        res = np.sum(np.multiply(dsa, a), axis=(1,2)) / np.sum(a)
        df[f.GetField(nameField)] = res

    if verbose:
        print('')
    return df
