import pandas as pd
import numpy as np
from functools import reduce
from django_pandas.io import read_frame

from django.utils.translation import get_language, gettext as _
from django.db import transaction
from django.utils.html import unquote
from django.conf import settings

from . import models as pm
from .cst import *
from .rasterfun import exact_point, Value
from .formula import variables, CONTEXT
from .colors import make_colors, tuple2hex

# HYDRO TEST
from django.db import connection, DataError, ProgrammingError
from .colors import Coloring
from PIL import Image


class RequestScrapper:
    """Main calculation engine. Stores and outputs pandas DataFrame.

    Use example :
        rs = RequestScrapper(request) # with django's HttpRequest
        rs.fill()                     # fill both regional and local data
        dfmap = rs.map_df()           # these outputs are not stored within rs
        dfyear = rs.year_df()
        dfmon = rs.month_df()
    """
    def __init__(self, request):
        self.auth = request.user.is_authenticated
        self.metid = int(request.GET.get('metric', '-1'))
        if self.metid == -1: # custom formula
            self.metric = None
            self.monthly = True
            self.formula = unquote(request.GET.get('formula'))
            self.agg = request.GET.get('agg', 'max') # which default ?
            if 'cmap' in request.GET:
                self.cmap = pm.ColorMap.objects.get(id=request.GET['cmap'])
            else:
                self.cmap = pm.ColorMap.objects.filter(custom=True).first()
        else: # precalc metric, or metric with formula
            self.metric = pm.Metric.objects.get(id=self.metid)
            self.monthly = self.metric.monthly
            self.formula = self.metric.formula
            self.agg = self.metric.aggregation
            self.cmap = self.metric.color
        if self.formula:
            self.variables = variables(self.formula)

        self.showraster = 'showraster' in request.GET
        self.visible = request.GET.get('visible', None)
        self.model_ids = request.GET.get('models', None)
        self.models = None
        if self.model_ids:
            self.model_ids = list(map(int, self.model_ids.split(',')))
            self.models = pm.ClimateModel.objects.filter(id__in=self.model_ids)

        # which scenario ?
        self.scen = request.GET.get('scen', DEFAULT_SCENARIO)
        # what are we comparing ?
        self.compare_scen = request.GET.get('compare', 'scenarii') == 'scenarii'
        # reference or not ?
        self.postcalc = request.GET.get('data', 'data')

        # space
        self.station_id = request.GET.get('station', None)
        self.station = None if not self.station_id else pm.Station.objects.get(id=self.station_id)
        self.zone = request.GET.get('zone', settings.MAIN_ZONE)
        if 'lon' in request.GET and 'lat' in request.GET :
            self.lonlat = (float(request.GET.get('lon')),float(request.GET.get('lat')))
        else:
            self.lonlat = None
        if not self.station and not self.lonlat:
            # normalize region names
            regions = self.zone.split(', ')
            self.real_zone = ', '.join([ r.locale_name for r in pm.Zone.objects.filter(name__in=regions) ])
        else:
            self.real_zone = self.zone


        # time
        self.months = request.GET.get('months', '1-12').split('-')
        self.yrs = request.GET.get('yrs', DEFAULT_YRS).split('-')
        self.refyrs = request.GET.get('refyrs', DEFAULT_REFYRS).split('-')
        self.yw = request.GET.get('yw', None) # year window for export
        self.rolling = request.GET.get('rolling', None)

        # placeholder
        self.loc_df = None     # local data
        self.regions_df = None # all region data
        self.scenlist = []     # actual scenarii list (containing data)
        self.title = {}        # dict of titles

        # set of encountered errors
        # for css purposes, errors are strings of the form "(X) message" where X is either E (error) or W (warning)
        self.error = set()

        # yes, we could include these into __init__, but this is more readable
        self.fill_warnings()
        self.make_titles()

    # util functions

    def fill_warnings(self):
        """Looks for inconsistent or fuzzy requests.
        Errors can still be added with self.error.add(str)."""
        if int(self.yrs[1]) < int(self.yrs[0]):
            self.error.add(_('(E) Wrong year range. '))
        elif int(self.yrs[1]) - int(self.yrs[0]) < 20:
            self.error.add(_('(W) Year range should span at least 20 years. '))
        if self.metric and self.metric.category.name == 'temperature' and self.postcalc == 'relative':
            self.error.add(_('(W) Relative difference (%) is not relevant with regard to temperature metrics. '))
        elif self.metric and 'agri' in self.metric.category.name and self.postcalc == 'data':
            self.error.add(_('(W) Raw crop yields should not be trusted, use comparison tools. '))
        if self.postcalc != 'data' and int(self.refyrs[1]) - int(self.refyrs[0]) < 20:
            self.error.add(_('(W) Reference year range should span at least 20 years. '))
        if not self.monthly and self.months!=['1','12']:
            self.error.add(_('(W) Month range discarded for yearly data.'))
        if self.lonlat and not self.metric:
            self.error.add(_('(E) Custom formulas are not available for pixels.'))
        if self.rolling and self.rolling != '1' and self.compare_scen:
            self.error.add(_('(W) Comparing scenarii with rolling > 1.'))

    def make_titles(self):
        """Makes all titles (even those that are not needed)."""
        if self.monthly and self.months != ['1', '12']:
            l = get_language()
            sm = SHORTMONTHS_FR if l and l.startswith('fr') else SHORTMONTHS
            months = sm[int(self.months[0])-1] + '-' + sm[int(self.months[1])-1] + ' '
        else:
            months = ''
        if self.compare_scen:
            scen = ''
        else:
            scen = _(' ({} scenario )').format(self.scen)
        self.title = {
            'map' : _('Spatial distribution in {}{} ({} scenario)').format(months, '-'.join(self.yrs), self.scen),
            'month' : _("Monthly variation in {}{}").format('-'.join(self.yrs), scen),
            'year' : _('Yearly evolution {}{}').format(months, scen),
        }

    def aggregate(self, gr):
        """In-year aggregation. Parameter is a pandas groupby or resample object."""
        return {
            'sum': gr.sum(),
            'mean': gr.mean(),
            'count': gr.count(),
            'min': gr.min(),
            'max': gr.max(),
        }[self.agg]

    def do_postcalc(self, df, ref=None, one_scen=False):
        """Compare to reference period if any, and extract scenarii mean/std."""
        one_scen = one_scen or not self.compare_scen

        if self.postcalc == 'absolute' :
            df -= ref
        elif self.postcalc == 'relative':
            df = 100 * (df - ref) / ref
            df = df.replace([np.inf, -np.inf], np.nan)

        if not one_scen: # yes, that's a double negation
            l = []
            for s in self.scenlist:
                if s in df:
                    l.append(df[s].agg(('mean', 'std'), axis=1))
            df = pd.concat(l, axis=1, keys=self.scenlist)

        return df#.fillna(0) # TODO : serialize np.nan into "null"

    def df_to_dygraphs(self, df):
        """Formats a dataframe to be read by dygraphs."""
        if df.empty:
            return False
        if self.compare_scen:
            colors = {s.name: {'color': s.color} for s in pm.Scenario.objects.all()}
            colors['ref'] = {'color': 'black'}
            labels = list(df.columns.levels[0])
            l = [ df.index.tolist() ] + [ df[s].values.tolist() for s in labels ]
            values = list(zip(*l))
            # values = df.to_json(orient='values')
            labels = ['date'] + labels
        else:
            df.insert(loc=0, column='date', value=df.index)
            colors = {s.name: {'id': s.id, 'color': s.color, 'strokePattern': DYGRAPH_STROKE.get(s.style, [])}
                      for s in pm.ClimateModel.objects.all()}
            colors[_('model average')] = {'color': 'red', 'strokeWidth': 4, 'strokeBorderWidth': 3}
            labels = df.columns.tolist()
            # replace nan's by null in json response
            bla = df.where(pd.notnull(df), None)
            values = bla.values.tolist()

        return {
            'data': values,
            'meta': {
                'strokeWidth': 2,
                'labels': labels,
                'sigma': 1,
                'errorBars': self.compare_scen,
                'series': colors,
                'legend': 'follow',
                'axes': {},
                'highlightSeriesOpts': {
                    'strokeWidth': 3,
                    'strokeBorderWidth': 1,
                    'highlightCircleSize': 3,
                },
            },
        }

    # data gathering :
    # roughly { loc, zoned } x { metric, formula }

    def get_zoned_metric(self, zone, one_scen=False):
        """Reads precalc metric."""
        if one_scen or not self.compare_scen :
            scenlist = [self.scen]
        else:
            scenlist = list(pm.Scenario.objects.all().values_list('name', flat=True))
        real_scenlist, dflist = [], []
        with transaction.atomic(savepoint=False):
            # lock access for hdf5 parallel access
            zd = pm.ZonedData.objects.select_for_update().get(metric=self.metric, zone=zone)
            with pd.HDFStore(zd.file, mode='r') as store:
                for s in scenlist:
                    df = store.get(s)
                    if self.models: # intersect
                        df = df[df.columns & self.models.values_list('name', flat=True)]
                    if not df.empty :
                        dflist.append(df)
                        real_scenlist.append(s)

        self.scenlist = real_scenlist # used later by postcalc
        if dflist:
            return pd.concat(dflist, axis=1, keys=real_scenlist)
        else:
            return pd.DataFrame()

    def get_lonlat_metric(self):
        """Sends a request for a single point in (postgis) raster."""
        # sets postgresql options here to catch fewer elements
        options = {}
        if self.models:
            options['model__in'] = self.models
        if not self.compare_scen :
            options['scenario__name'] = self.scen

        # gets postgis raster for pixel, using proj.rasterfun primitives
        qset = pm.MonthlyRasterData.objects.filter(metric=self.metric, **options) \
            .annotate(point=exact_point(*self.lonlat)).annotate(value=Value('raster', 'point'))
        df = read_frame(qset, fieldnames=['year', 'month', 'scenario__name', 'model__name', 'value'])
        # create an index from year/month
        df['idx'] = pd.PeriodIndex(year=df.year, month=df.month, freq='M')
        if df['value'].notna().any():
            df = df.pivot_table(index='idx', columns=['scenario__name', 'model__name'], values='value')
            self.scenlist = list(df.columns.levels[0])
            return df
        else:
            return pd.DataFrame()

    def apply_formula(self, pvars):
        """Raw formula evaluation procedure."""
        # allowed functions
        r = []
        context = CONTEXT.copy()
        if self.models is None:
            cms = pm.ClimateModel.objects.all().values_list('name', flat=True)
        else:
            cms = self.models.values_list('name', flat=True)

        # Apply the formula column by column because of different indexes
        for cm in cms:
            docalc = True
            for v, vdata in pvars.items():
                if cm in vdata.columns:
                    context[v] = vdata[cm]
                else:
                    docalc = False
            if docalc:
                r.append(eval(self.formula, context))

        if r:
            return pd.concat(r, axis=1)
        else :
            return pd.DataFrame()

    def get_zoned_formula(self, zone, one_scen=False):
        if not self.formula:
            self.error.add(_('(E) No formula: some values could not be computed.'))
            return pd.DataFrame()
        if one_scen or not self.compare_scen :
            scenlist = [self.scen]
        else:
            scenlist = list(pm.Scenario.objects.all().values_list('name', flat=True))
        real_scenlist, dflist = [], []

        for scen in scenlist :
            with transaction.atomic(savepoint=False):
                drds = pm.DailyRegionData.objects.filter(zone=zone, param__shortname__in=self.variables).select_for_update()
                if len(drds) != len(self.variables):
                    self.error.add(_('(E) No data.'))
                    return pd.DataFrame()
                pvars = { drd.param.shortname : drd.read_data(scen) for drd in drds }

            df = self.apply_formula(pvars)
            if not df.empty :
                dflist.append(df)
                real_scenlist.append(scen)

        self.scenlist = real_scenlist
        return pd.concat(dflist, axis=1, keys=scenlist)

    def get_station(self, one_scen=False): # one_scen should never be set
        # TODO : merge with get_zoned_formula
        if not self.formula:
            self.error.add(_('(E) No formula: some values could not be computed.'))
            return pd.DataFrame()
        if one_scen or not self.compare_scen :
            scenlist = [self.scen]
        else:
            scenlist = pm.Scenario.objects.all().values_list('name', flat=True)
        real_scenlist, dflist = [], []

        for scen in scenlist :
            with transaction.atomic(savepoint=False):
                sds = pm.StationParam.objects.filter(station=self.station, param__shortname__in=self.variables).select_for_update()
                if len(sds) != len(self.variables):
                    # must have all variables, else error
                    self.error.add(_('(E) No data.'))
                    return pd.DataFrame()

                pvars = {sd.param.shortname: sd.read_data(scen) for sd in sds}

            df = self.apply_formula(pvars)
            if not df.empty :
                dflist.append(df)
                real_scenlist.append(scen)

        self.scenlist = real_scenlist
        return pd.concat(dflist, axis=1, keys=real_scenlist)

    # self dataframe populating

    def fill_regions_df(self):
        # TODO : uncomment exception when ready ?
        try :
            if self.metric:
                fun = self.get_zoned_metric
            else:
                fun = self.get_zoned_formula

            zlist, dflist = [], []
            for z in pm.Zone.objects.exclude(name=settings.MAIN_ZONE):
                zlist.append(z.name)
                df = fun(z, one_scen=True)
                if df.empty:
                    self.regions_df = pd.DataFrame()
                    return
                if self.months and self.monthly:
                    df = df[(df.index.month >= int(self.months[0])) & (df.index.month <= int(self.months[1]))]
                df = self.aggregate(df.resample('Y', kind='period')).mean(axis=1)
                dflist.append(df)

            df = pd.concat(dflist, axis=1, keys=zlist)

            if self.postcalc != 'data':
                ref = df[str(self.refyrs[0]):str(self.refyrs[1])].mean()
            else:
                ref = None

            df = df[str(self.yrs[0]):str(self.yrs[1])]
            df = self.do_postcalc(df, ref, one_scen=True)

            # finally, agreggate all
            self.regions_df = df.mean()

        except (AttributeError, ValueError, TypeError):
            self.error = _('Error : no data found')

    def fill_monozone_df(self, zone):
        if self.metric:
            return self.get_zoned_metric(zone)
        else: # formula
            return self.get_zoned_formula(zone)

    def fill_loc_df(self):
        """Local value.

        If several zones (e.g. 'Matam, Dakar'), sends one request per zone,
        then calculates average weighted by zone area."""
        if self.station_id : # apply formula if any
            self.loc_df = self.get_station()
        elif self.lonlat :
            if self.metric:
                self.loc_df = self.get_lonlat_metric()
            else: # TODO : netcdf
                self.loc_df = pd.DataFrame()
        else :
            if self.metric and (self.metric.raster_only or self.showraster):
                self.loc_df = pd.DataFrame()
                return
            if ',' in self.zone:
                sumsum = lambda x, y: x + y
                zqset = pm.Zone.objects.filter(name__in=self.zone.split(','))
                l = [ self.fill_monozone_df(z)*z.mpoly.area for z in zqset ]

                total_area = reduce(sumsum, [ z.mpoly.area for z in zqset ])
                self.loc_df = reduce(sumsum, l) / total_area
            else:
                z = pm.Zone.objects.get(name=self.zone)
                if self.metric:
                    self.loc_df = self.get_zoned_metric(z)
                else:
                    self.loc_df = self.get_zoned_formula(z)

        if not self.compare_scen and not self.loc_df.empty:
            self.loc_df = self.loc_df[self.scen]
        # if self.loc_df.empty:
        #     self.error.add(_('(E) No data.'))

    def fill(self):
        self.fill_regions_df()
        self.fill_loc_df()

    # outputting

    def year_df(self):
        """Interannual local data ; self.loc_df has to be filled before call."""
        df = self.loc_df
        if df.empty:
            return df

        if self.months and self.monthly:
            df = df[(df.index.month>=int(self.months[0]))&(df.index.month<=int(self.months[1]))]
        # not resample, because pd.Period is not json-serializable
        df = self.aggregate(df.groupby(df.index.year))

        if self.postcalc != 'data':
            ref = df.loc[str(self.refyrs[0]):str(self.refyrs[1])].mean()
        else:
            ref = None
        df = self.do_postcalc(df, ref)

        if not self.compare_scen and len(df.columns) > 1:
            avg = df.mean(axis=1)
            avg.name = _('model average')
            df = pd.concat([df, avg], axis=1)


        if self.rolling:
            df = df.rolling(int(self.rolling), center=True, min_periods=1).mean()

        return df.round(ROUND)

    def month_df(self):
        """Season local data ; self.loc_df has to be filled before call."""
        if self.loc_df.empty:
            return self.loc_df
        df = self.loc_df[str(self.yrs[0]):str(self.yrs[1])]
        df = self.aggregate(df.resample('M'))
        df = df.groupby(df.index.month).mean()

        if self.postcalc != 'data':
            ref = self.loc_df[str(self.refyrs[0]):str(self.refyrs[1])]
            ref = ref.groupby(ref.index.month).mean()
        else:
            ref = None
        df = self.do_postcalc(df, ref)

        if not self.compare_scen and len(df.columns) > 1 :
            avg = df.mean(axis=1)
            avg.name = _('model average')
            df = pd.concat([df, avg], axis=1)

        return df.round(ROUND)

    def point_value(self):
        df = self.loc_df
        if df.empty :
            return None
        if self.months and self.monthly:
            df = df[(df.index.month >= int(self.months[0])) & (df.index.month <= int(self.months[1]))]
        df = self.aggregate(df.resample('Y', kind='period')).mean(axis=1)

        if self.postcalc != 'data':
            ref = df[str(self.refyrs[0]):str(self.refyrs[1])].mean()
        else:
            ref = None

        df = df[str(self.yrs[0]):str(self.yrs[1])]
        df = self.do_postcalc(df, ref, one_scen=True)
        return df.mean().round(ROUND)

    def map_df(self):
        """Spatial variation data ; self.regions_df has to be filled before call."""
        df = self.regions_df
        if df.empty:
            self.error.add(_('(E) Not enough data for map.'))
            return pd.DataFrame(), (None, None)
        vmin, vmax = df.min(), df.max()

        if self.postcalc == 'data':
            code = self.cmap.code
        else:
            if vmin * vmax < 0:
                vmax = max(abs(vmin), vmax)
                vmin = -vmax
            code = self.cmap.diffcode

        col = pd.Series(map(tuple2hex, make_colors(code, df, vmin, vmax)), index=df.index)
        scale = np.linspace(vmin, vmax, num=9).tolist()
        scalecols = list(map(tuple2hex, make_colors(code, scale, vmin, vmax)))
        return pd.concat([df.round(ROUND), col], axis=1, keys=('value', 'color')), (scale, scalecols)

    # RASTER TESTING ZONE

    def get_raster_meta(self):
        mrd = pm.MonthlyRasterData.objects.filter(metric=self.metric).first()
        if mrd:
            xmin, xres, _, ymax, _, yres = mrd.raster.geotransform
            # ol extent is [minx, miny, maxx, maxy], we add resolution
            return [xmin, round(ymax + (mrd.raster.height)*yres,4), round(xmin + (mrd.raster.width)*xres,4), ymax,
                    xres, yres]
        return None


    def build_raster(self):
        """
        meta = upperleftx | upperlefty | width | height | scalex | scaley | skewx | skewy | srid | numbands
        """
        scid = pm.Scenario.objects.get(name=self.scen).id

        qrymon = qrymod = ''
        argmon = argmod = []

        if self.months and self.months != ['1', '12']:
            qrymon = 'and month >= %s and month <= %s'
            argmon = self.months

        if self.models:
            qrymod = 'and model_id in %s'
            argmod = [tuple(self.model_ids)]

        if self.postcalc == 'data':
            qry = "select ST_DumpValues(raster, 1) as val, ST_Metadata(raster) as meta from " \
                  "(select ST_Union(raster, 'MEAN') as raster from " \
                  "(select ST_Union(raster, %s) as raster from proj_monthlyrasterdata where " \
                  "metric_id = %s and scenario_id = %s and " \
                  "year >= %s and year <= %s {month} {models} group by year, model_id) " \
                  "as foo) as bar;"
            args = [ self.metric.aggregation, self.metid, scid ] + self.yrs + argmon + argmod
            qry = qry.format(month=qrymon, models=qrymod)
        else:
            # building a query used in each period
            if self.postcalc == 'relative':
                # NULLIF ensures we don't divide by zero
                comp_formula = '100*([rast2]-[rast1])/(NULLIF([rast1],0))'
            else :
                comp_formula = '[rast2]-[rast1]'
            qry1 = "(select ST_Union(raster, 'MEAN') as raster from " \
                   "(select ST_Union(raster, %s) as raster from proj_monthlyrasterdata where " \
                   "metric_id = %s and scenario_id = %s and " \
                   "year >= %s and year <= %s {month} {models} group by year, model_id) as foo)"
            qry1 = qry1.format(month=qrymon, models=qrymod)
            # joining all together
            qry = "with future as {qry}, past as {qry} " \
                  "select ST_DumpValues(raster, 1) as val, ST_Metadata(raster) as meta from " \
                  "(select ST_MapAlgebra(past.raster, future.raster, %s) " \
                  "as raster from future, past) as foo;".format(qry=qry1)
            args = [ self.metric.aggregation, self.metid, scid ] + self.yrs + argmon + argmod + [ self.metric.aggregation, self.metid, scid ] + self.refyrs + argmon + argmod + [ comp_formula ]

        with connection.cursor() as cursor:
            # DEBUG
            # debug = qry.split('%s')
            # debug = [ x for t in zip(*[debug, map(str,args)]) for x in t] + [debug[-1]]
            # print(''.join(debug))

            cursor.execute(qry, args)
            res, meta = cursor.fetchone()

        return np.array(res, dtype=np.float), meta

    def has_raster(self):
        return pm.MonthlyRasterData.objects.filter(metric=self.metric).exists()

    def get_raster(self):
        """Image building for json response.

            Returns :
            - colored image
            - metadata (from postgis ST_Metadata)
            - colored scale (as array)
            """
        a, meta = self.build_raster()

        vmin, vmax = np.nanmin(a), np.nanmax(a)
        if self.postcalc != 'data':
            code = self.cmap.diffcode
            if vmin*vmax < 0 :
                vmax = max(abs(vmin), vmax)
                vmin = -vmax
            elif vmax < 0 :
                vmax = 0
            else : # vmin > 0:
                vmin = 0
        else:
            code = self.cmap.code

        c = Coloring(code, vmin=vmin, vmax=vmax)
        image = Image.fromarray(c(a))
        return image, eval(meta), c.make_scale(ashex=True, log=(self.metric.is_log and self.postcalc=='data'))


    def get_raster_for_export(self):
        """Image building for matplotlib export.

            Returns :
            - UNcolored image
            - metadata (postgis ST_Metadata)
            - matplotlib colormap
            """
        a, meta = self.build_raster()
        code = self.cmap.code if self.postcalc == 'data' else self.cmap.diffcode
        vmin, vmax = np.nanmin(a), np.nanmax(a)
        if self.postcalc != 'data':# and vmin*vmax < 0:
            vmax = max(abs(vmin), vmax)
            vmin = -vmax

        c = Coloring(code, vmin=vmin, vmax=vmax)

        return a, eval(meta), c.mpl