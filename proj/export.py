import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.colors import LinearSegmentedColormap
from matplotlib import ticker            # to add +, and months
from matplotlib import patheffects as pe # for drawing model average
from math import trunc
import numpy as np
import io

from django.http import HttpResponse
from django.utils.translation import get_language, gettext as _
from django.conf import settings

from .gpd_connect import zone_polys
from .reqscrapper import RequestScrapper
from . import models as pm
from .colors import make_colors, Coloring
from .cst import *

# maybe this should also be class-ified

def print_model_df(rs, df, ax):
    i = -1
    cms = rs.models or pm.ClimateModel.objects.all()
    for cm in cms:
        if cm.name not in df :
            continue
        i += 1
        if rs.visible and rs.visible[i]=='F':
            continue
        ax.plot(df[cm.name], color=cm.color, linestyle=cm.style)

    if _('model average') in df and (not rs.visible or rs.visible[-1]=='T'):
        # ax.plot(df[_('model average')], color="white", lw=4)
        ax.plot(df[_('model average')], color="red", lw=3, path_effects=[pe.Stroke(linewidth=6, foreground='white'), pe.Normal()])

    if rs.postcalc == 'data':
        tick = ticker.FormatStrFormatter('%d')
    else:
        tick = ticker.FuncFormatter(lambda tick, _: '{:+}'.format(tick))
    ax.yaxis.set_major_formatter(tick)

def print_scen_df(rs, df, ax):
    i = -1
    scenlist = pm.Scenario.objects.filter(name__in=rs.scenlist)
    for scen in scenlist:
        i += 1
        if rs.visible and rs.visible[i] == 'F':
            continue
        ax.plot(df[scen.name]['mean'], color=scen.color, label=scen.name)
        ymin, ymax = df[scen.name]['mean'] - df[scen.name]['std'], df[scen.name]['mean'] + df[scen.name]['std']
        ax.fill_between(df.index, ymin, ymax, alpha=.2, color=scen.color)

    if rs.postcalc == 'data':
        tick = ticker.FormatStrFormatter('%d')
    else:
        tick = ticker.FuncFormatter(lambda tick, _: '{:+}'.format(tick))
    ax.yaxis.set_major_formatter(tick)


def print_raster(rs, ax):
    zp = zone_polys()
    zp.plot(ax=ax, edgecolor='black', facecolor='none', zorder=2)
    im, meta, cmap = rs.get_raster_for_export()

    vmin, vmax = np.nanmin(im), np.nanmax(im)

    if rs.postcalc != 'data':
        vmax = max(abs(vmin), vmax)
        vmin = -vmax

    # meta = upperleftx | upperlefty | width | height | scalex | scaley | skewx | skewy | srid | numbands
    ulx, uly, w, h, xres, yres, *_ = meta

    extent = (
        ulx,
        ulx + xres * w,
        uly + yres * h,
        uly,
    )
    plt.imshow(im, extent=extent, zorder=1, cmap=cmap, vmin=vmin, vmax=vmax)
    plt.colorbar()

    if rs.lonlat :
        # NO !
        lon, lat = rs.lonlat
        rlon = ulx + xres* trunc((lon-ulx)/xres)
        rlat = uly + yres* trunc((lat-uly)/yres)
        plt.plot([rlon, rlon + xres, rlon + xres, rlon, rlon], [rlat, rlat, rlat + yres, rlat + yres, rlat], linewidth=3,
                 color="black")


def print_map_df(rs, ax):
    # some redundancy from RequestScrapper.map_df
    df = rs.regions_df
    df.name = 'value'
    df.index.name = 'zone'
    vmin, vmax = df.min(), df.max()
    # scale = list(np.linspace(vmin, vmax, num=9))

    if rs.postcalc == 'data':
        code = rs.cmap.code
    else:
        if vmin * vmax < 0:
            vmax = max(abs(vmin), vmax)
            vmin = -vmax
        code = rs.cmap.diffcode

    coloring = Coloring(code, vmin=vmin, vmax=vmax)
    cmap = coloring.mpl
    # DEBUG
    # rows = coloring.rows
    # coloring('bonjour')

    zp = zone_polys()
    gdf = zp.merge(df, left_on='name', right_on='zone')
    gdf['coords'] = gdf['geom'].apply(lambda x: x.representative_point().coords[0])
    gdf.plot(column='value', ax=ax, legend=True, cmap=cmap, vmin=vmin, vmax=vmax, edgecolor='black')
    for idx, row in gdf.iterrows():
        plt.annotate(s=row['name'], xy=row['coords'], horizontalalignment='center')
    if rs.station:
        plt.plot(rs.station.lon, rs.station.lat, 'ko') # 'ko' -> black point, see plot() doc
    else: # region
        if rs.zone != settings.MAIN_ZONE :
            l = rs.zone.split(', ')
            zgdf = gdf[gdf['name'].isin(l)]
            zgdf.plot(ax=ax, facecolor='none', edgecolor='black', linewidth=3)

    ax.set_ylabel(_('°North'))
    ax.set_xlabel(_('°West'))


def image(request, extension):
    def set_legend(rs, ax):
        if rs.compare_scen:
            ax.legend()
        else:
            box = ax.get_position()
            box.y0 += box.height * .2
            ax.set_position(box)
            ax.legend(bbox_to_anchor=(0, -.1), frameon=False, fontsize='small', ncol=6, loc=2)  # lower left

    rs = RequestScrapper(request)
    if rs.showraster :
        rs.fill_loc_df()
    else:
        rs.fill()

    pfun = print_scen_df if rs.compare_scen else print_model_df
    if rs.metric:
        name = rs.metric.shortname +'_'
    else: # generate a name based on formula
        name = rs.agg + '_' + ''.join(filter(lambda x:x.isdigit() or x.isalpha(), rs.formula))

    if rs.metric:
        if rs.postcalc == 'data':
            pretitle = rs.metric.locale_name.capitalize() + _(' in ') + rs.metric.unit + ':\n'
        else:
            unit = rs.metric.unit if rs.postcalc == 'absolute' else '%'
            pretitle = _('{} (in {}) compared to reference period {}:\n') \
                .format(rs.metric.locale_name.capitalize(), unit, '-'.join(rs.refyrs))
    else :
        pretitle = rs.formula+'\n'

    if extension == 'pdf':
        # special handling of pdf pages
        buf = io.BytesIO()
        pp = PdfPages(buf)

        fig, ax = plt.subplots(figsize=(11.69, 8.27), dpi=100)

        if rs.showraster:
            print_raster(rs, ax)
        else:
            print_map_df(rs, ax)

        ax.set_title(pretitle+rs.title['map'])
        pp.savefig(fig)

        if not rs.loc_df.empty:
            if rs.monthly :
                df = rs.month_df()
                fig, ax = plt.subplots(figsize=(11.69, 8.27), dpi=100)
                pfun(rs, df, ax)
                plt.xticks(range(1, 13), SHORTMONTHS, rotation=45, ha="right")
                ax.set_title(rs.title['month'])
                set_legend(rs, ax)
                pp.savefig(fig)
                plt.close(fig)

            df = rs.year_df()
            if rs.yw :
                yw = str(rs.yw).split('-')
                df = df.loc[yw[0]:yw[1]]
            fig, ax = plt.subplots(figsize=(11.69, 8.27), dpi=100)
            pfun(rs, df, ax)
            ax.set_title(rs.title['year'])
            if not rs.monthly:
                set_legend(rs, ax)
            pp.savefig(fig)
            plt.close(fig)

        pp.close()
        buf.seek(0)
    else:
        # png / svg / jpg
        if not rs.loc_df.empty:
            nbplots = 3 if rs.monthly else 2
        else:
            nbplots = 1

        fig = plt.figure(figsize=(12, 6 * nbplots))

        ax = plt.subplot(nbplots, 1, 1)

        if rs.showraster:
            print_raster(rs, ax)
        else:
            print_map_df(rs, ax)

        ax.set_title(pretitle+rs.title['map'])

        if not rs.loc_df.empty:
            if rs.monthly:
                df = rs.month_df()
                ax = plt.subplot(3,1,2)
                pfun(rs, df, ax)
                plt.xticks(range(1, 13), SHORTMONTHS, rotation=45, ha="right")
                ax.set_title(rs.title['month'])
                set_legend(rs, ax)

            df = rs.year_df()
            if rs.yw:
                yw = str(rs.yw).split('-')
                df = df.loc[yw[0]:yw[1]]
            ax = plt.subplot(nbplots, 1, nbplots)
            pfun(rs, df, ax)
            ax.set_title(rs.title['year'])
            if not rs.monthly:
                set_legend(rs, ax)

        if extension == 'svg':
            buf = io.StringIO()
        else: # png & jpg
            buf = io.BytesIO()

        fig.savefig(buf, format=extension, bbox_inches='tight', dpi=request.GET.get('dpi', 100))
        buf.seek(0)
        plt.close(fig)

    r = HttpResponse(buf.getvalue(), content_type="application/" + extension)
    r['Content-Disposition'] = 'attachment; filename="{}_{}.{}"'.format(name, rs.zone, extension)
    return r
