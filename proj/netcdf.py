"""
Outputs netcdf from MonthlyRasterData table, for a given metric/scenario/model.
"""

from netCDF4 import Dataset, date2num
from datetime import datetime
import numpy as np

from django.core.files.temp import NamedTemporaryFile
from django.http import FileResponse

from . import models as pm
from .cst import *

def write_netcdf(request, metric, scen, model) :
    metric = pm.Metric.objects.get(shortname=metric)
    qset = pm.MonthlyRasterData.objects.filter(model__name=model, scenario__name=scen, metric=metric).order_by('year', 'month')

    # get one for reference
    r = qset.first().raster
    # postgis origin is upper-left (ul)
    ulx, xscale, _, uly, _, yscale = r.geotransform
    brx, bry = ulx + r.width * xscale, uly + r.height * yscale

    # the only way to write down an nc file is a temp file,
    # io.BytesIO or FileResponse won't work
    tmpfile = NamedTemporaryFile(suffix='.nc')
    ds = Dataset(tmpfile.name, 'w')
    ds.createDimension('lon', r.width)
    ds.createDimension('lat', r.height)
    ds.createDimension('time', None)
    lon = ds.createVariable('lon', 'f4', ('lon',))
    lon.units = 'degrees east'
    lon[:] = np.linspace(ulx, brx, r.width )
    lat = ds.createVariable('lat', 'f4', ('lat',))
    lat.units = 'degrees north'
    lat[:] = np.linspace(uly, bry, r.height)

    time = ds.createVariable('time', 'i4', ('time',))
    time.units = 'days since 1950-01-01'
    time.calendar = 'gregorian'
    numdates = []
    for year in range(*YEAR_RANGE):
        if metric.monthly :
            for month in range(1,13):
                numdates.append(date2num(datetime(year, month, 1), units=time.units, calendar=time.calendar))
        else:
            numdates.append(date2num(datetime(year, 1, 1), units=time.units, calendar=time.calendar))
    time[:] = numdates

    var = ds.createVariable(metric.shortname, 'f4', ('time', 'lat', 'lon',))
    var.unit = metric.unit
    var[:] = np.array([ mrd.raster.bands[0].data() for mrd in qset ])
    ds.close()

    return FileResponse(tmpfile, content_type='application/nc')
