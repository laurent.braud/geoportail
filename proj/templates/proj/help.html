{% extends 'proj/base.html' %}

{% block content %}
{% load static %}
<div class="smallpage" style="text-align: justify;">
    <h2>Using climate service portal</h2>

    <p>This document is for the <i>advanced</i> version of the projection portal, but contains all information for the <i>simple</i> one as well.
        For an even more advanced usage,
        you might want to read <a href="{% url 'proj:formula' %}">the page dedicated to custom formulas</a>.</p>

    <h3>Top menu</h3>
    <ul class="spread">
        <li>The first item is the parameter, ordered by theme : agriculture, rain and temperature. You may read a
            <a href="{% url 'proj:about' %}#parameters">detailed description of each parameter</a>. If the climatic
            parameters do not suit you, you may enter your own at the bottom of the panel, along with a map coloring scheme.
            See the <a href="{% url 'proj:formula' %}">page dedicated to custom formulas</a> for more.
        </li>
        <li>Next is the list of models considered in the calculations. If not sure, it is best to keep them all
            selected. Models can also be compared amongst themselves (see <a href="#graphsection">graph section</a> below).
            There is a <a href="{% url 'proj:about' %}#models">detailed description of climate models</a>.
        </li>
        <li>Three scenarii are considered : rcp26 is the "optimistic" scenario, rcp85 is the "worst-case" scenario. </li>
        <li>The next panel lets you set time. Each parameter has a specific aggregation function which is used to
            calculate yearly data. The resulting value is then averaged between years, models (and zones, for time
            series).

            <ul>
                <li>
                    By default, parameters are aggregated on a whole year.
                    You may chose a narrower interval, such as "6-9" for June to September.
                    This does not impact the monthly graph.
                </li>
                <li>
                    The second item specifies the period in years. This does not impact the interannual graph.
                    <br><b class="errW">Note :</b> climate models cannot be trusted on a specific year. It is best to average results
                    on a large period, at least 20 years.
                </li>
                <li>
                    Data is presented 'raw' by default. It is possible to compare it to a reference period (by default
                    {{ refyrs }}, may be changed). Two comparisons are available :
                    <ul>
                        <li>absolute difference is simply a
                            substraction (projected data minus reference data) ;
                        </li>
                        <li>relative difference is a quotient (projected
                            data minus reference data, divided by reference data) presented as percentage.
                            When reference data is very small (e.g. rainfall in January), it may therefore display very
                            high values.
                        </li>
                    </ul>
                    <b class="errW">Note :</b> for some parameters like crop yields, it is strongly advised to use
                    these comparison tools, as 'raw' projections have a low confidence factor.
                    <br><b class="errW">Note 2 :</b> "relative" difference is only relevant for
                    <a href="https://en.wikipedia.org/wiki/Intensive_and_extensive_properties">extensive parameters</a> like rainfall or yield, but
                    it makes no sense for temperature.
                </li>
            </ul>
            <p>For instance, a request for 'max temperature in June-September 2050-2060' will first
                calculate the
                maximum temperature from June to September, for each year and model. All theses values are then
                averaged and displayed, region by region, on the left map.</p>

            <p>In the background, climatic parameters are stored on a monthly basis, and agriculture parameters on a
                yearly basis. When using custom formulas or station view (<a href="#stations">see below</a>), the portal falls back on daily data, which
                is aggregated and presented as monthly or yearly output.
                You may <a href="{% url 'proj:formula' %}#download">download daily data in csv format</a> for regions.</p>
        </li>
        <li>The last panel exports the page as image (png, jpg), vector image (svg) or document (pdf).
            Graph data can be exported in text format (csv) and spreadsheet (xls).
        </li>
    </ul>
    <h3>Map section</h3>
    <p>Map interaction allows to select zones for time graphs. By default, it is set on <i>region</i> interaction.</p>
    <ul class="spread">
        <li>Click on a region to select it, and only it.</li>
        <li>Shift-click on a region to add or remove it from selection.</li>
        <li>Click outside of any region to cancel selection and fall back to country data.</li>
        <li>Click and drag to select several regions at once.</li>
    </ul>

    <p>When several regions are selected, values are averaged
        for graph calculations, weighted by each region's area. Note that because of successive approximations, selecting
        all the regions will yield a result slightly different that selecting the country.</p>

    <p>In the map's top menu, two other interactions may display finer data.</p>
    <ul class="spread">
        <li>The <i>pixel</i> view represents the original data used for regional calculations, i.e. cells of longitude
            0.5° by latitude 0.5° (approx. 50x50 km). When clicking on a specific pixel, the right graphs will show its values.
            Note that the value will be the same in the whole pixel. You may notice that calculus is a bit slower when using pixels, as
            the server has to retrieve point data from rasters.</li>
        <li id="stations">For an even more local view, you have to switch to  <i>stations</i>. In this case, model data
        has been corrected by meteorological stations outputs, so these values represent specific spatial points.
            Click on a station to read projection values. Stations do not have data
            for all parameters : a pluviometer cannot output temperature values and will output error messages accordingly.
            They cannot display crop yield data, either.
        <br><b class="errW">Note : </b> projection quality depends on original data.
        </li>
    </ul>

    <p>The top-right search bar allows you to scroll places. Most important places are already pinned on the map,
    but the search bar contains much more.</p>
    <ul>
        <li>On regional and pixel view, selecting a place will display it on the map and select the region/pixel which contain it.
            When the place is on a frontier, two (or more) regions may be selected and values will be averaged.
        </li>
        <li>On station view, the search is done on stations. It is a convenient way to find a given station.</li>
    </ul>


    <h3 id="graphsection">Graph section</h3>
    <p>The right side of the page presents time series. The upper one shows monthly evolution (averaged for years), the
        lower one show interannual evolution. They compare scenarii by default, along with error bars
        (i.e. standard deviation of selected models).
        You may switch to model comparison for a given scenario.</p>
    <p>To the rightmost side, the legend displays if a specific series is visible <img style="height:1em" src="{% static 'img/eye.svg' %}"> or
        hidden <img style="height:1em" src="{% static 'img/eye-off.svg' %}">. Click on it to toggle. You may toggle visibility of
        every series at
        once by using the uppermost button <img style="height:1em" src="{% static 'img/refresh-cw.svg' %}">. When using
        model comparison view, you may also remove entirely a specific model by clicking on the cross <img
                style="height:1em"
                src="{% static 'img/x-circle.svg' %}">. In this case the model will be removed and everything
        re-calculated. You may put the model back on by selecting it again in the top menu.
    </p>
    <p>Click and drag in the interannual graph to zoom in, double-click to zoom out. It is possible to use the zoom
        as year range for the next calculation, by clicking on "Use these bounds"
        below.</p>
</div>
{% endblock %}