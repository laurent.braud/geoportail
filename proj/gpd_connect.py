"""
Reads zones defined in postgres database for geopandas use.
"""

import psycopg2
from django.conf import settings
import geopandas as gpd


# http://ramiro.org/notebook/geopandas-choropleth/
def zone_polys():
    # Is there a better way ? Can we get back django connection ?

    db = settings.DATABASES['default']
    con = psycopg2.connect(database=db['NAME'], user=db['USER'], password=db['PASSWORD'], host=db['HOST'])

    sql = "select name, mpoly as geom from proj_zone where name != '{}';".format(settings.MAIN_ZONE)
    return gpd.read_postgis(sql, con)
