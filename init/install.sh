#!/bin/bash

APP=/app

python manage.py flush --no-input
python manage.py migrate proj
python manage.py migrate
python manage.py collectstatic

# let the superuser name itself
python manage.py createsuperuser

echo "Loading base components into data base : metric categories, scenarii."
python manage.py loaddata $APP/init/base.json

read -p "Load optional components (metrics, models) ? [yN] " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    python manage.py loaddata $APP/init/optional.json
fi

read -p "Load Senegal geography ? [yN] " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    python manage.py loadgeo
fi
