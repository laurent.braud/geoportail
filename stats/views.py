import pandas as pd

from django.shortcuts import render, reverse
from django.urls import reverse_lazy
from django.http import HttpResponse
from django.views.generic.edit import CreateView, DeleteView
from django.views.generic import ListView

from . import models as sm

# Create your views here.

# Site metric views

from django.contrib.auth.decorators import login_required

@login_required
def index(request):
    return render(request, 'stats/index.html')


def metric(request, qset, ip, template):
    if ip:
        qset = qset.filter(ip=ip)
    if 'nolog' in request.GET:
        qset = qset.exclude(logged=True)
    n = int(request.GET.get('n', 100))
    total = qset.count()
    context = {
        'requestmetric_list': qset.order_by('-date')[:n],
        'ip': ip,
        'n': min(n, total),
        'total': total,
    }
    return render(request, template, context)


# @login_required
# def proj_metric(request, ip=None):
#     return metric(request, sm.ProjMetric.objects.all(), ip, 'stats/projmetric_list.html')

@login_required
def proj_metric(request, ip=None):
    pset = sm.ProjMetric.objects.all()
    hset = sm.HistoMetric.objects.all()
    if ip:
        pset = pset.filter(ip=ip)
        hset = hset.filter(ip=ip)
    if 'nolog' in request.GET:
        pset = pset.exclude(logged=True)
        hset = hset.filter(ip=ip)
    n = int(request.GET.get('n', 100))
    ptotal = pset.count()
    htotal = hset.count()
    context = {
        'pset': pset.order_by('-date')[:n],
        'hset': hset.order_by('-date')[:n],
        'ip': ip,
        'pn': min(n, ptotal),
        'ptotal': ptotal,
        'hn': min(n, htotal),
        'htotal': htotal,
        'nolog': 'nolog' in request.GET,
    }
    return render(request, 'stats/projmetric_list.html', context)

def metric_df(qset):
    l = list(zip(*qset.values_list('date', 'ip')))
    df = pd.DataFrame(l[1], index=l[0]).resample('D', kind='period')

    return df.agg(['count', 'nunique'])

@login_required
def proj_metric_csv(request):
    opts = {'ip': None}
    if 'nolog' in request.GET:
        opts = { 'logged': True}
    df = metric_df(sm.ProjMetric.objects.exclude(**opts))
    # df = pd.concat([metric_df(sm.ProjMetric.objects.exclude(**opts)),
    #                 metric_df(sm.HistoMetric.objects.exclude(**opts))],
    #                axis=1)
    r = HttpResponse('date,proj_total,proj_unique,histo_total, histo_unique\n')
    df.to_csv(r, header=False)
    return r

# Comment form

class CommentCreate(CreateView):
    model = sm.Comment
    fields = ['name', 'mail', 'comment']
    success_url = reverse_lazy('main')

class CommentListView(ListView):
    model = sm.Comment

class CommentDelete(DeleteView):
    model = sm.Comment
    success_url = reverse_lazy('stats:comments')