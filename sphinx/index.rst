.. climap documentation master file, created by
   sphinx-quickstart on Thu Nov 28 14:09:25 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Géoportail climatique (*draft*)
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contenu :

	     
   usage/docker
   usage/source
   usage/grille
   usage/station
   usage/admin
   
.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
