<!-- climap documentation master file, created by
sphinx-quickstart on Thu Nov 28 14:09:25 2019.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive. -->
# Géoportail climatique (*draft*)


* Installation via docker


    * Initialisation


    * Mise en place d’un volume extérieur


    * Accès au système


* Installation directe


    * Installation


    * Accès


    * Chargement des métadonnées


* Données grillées et/ou régionales


    * Métriques


    * Paramètres bruts


* Correction de données stations


    * Correction


    * Insertion des stations dans le portail


    * Insertion des projections


* Interface d’administration


    * Suppression de données ponctuelles


    * Suppression d’une grande quantité de données


<!-- Indices and tables -->
<!-- ================== -->
<!-- * :ref:`genindex` -->
<!-- * :ref:`modindex` -->
<!-- * :ref:`search` -->
