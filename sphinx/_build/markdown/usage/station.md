# Correction de données stations

## Correction

La correction des projections au niveau des stations doit être faite
par [cdft_correct](https://gitlab.com/laurent.braud/cdft_correct). Ce programme est
totalement indépendant du reste du géoportail et peut être faite sur
une machine différente.

### Installation

Ici aussi, l’usage d’un environnement virtuel est recommandé. Voir
Environnement virtuel. Il faut également installer la [librairie de correction
SBCK](https://github.com/yrobink/SBCK).

```
$ pip install netCDF4 numpy pandas cython matplotlib scipy sklearn xlrd
$ git clone https://gitlab.com/laurent.braud/cdft_correct
$ git clone https://github.com/yrobink/SBCK
$ cd SBCK/python
$ python setup.py install
$ cd ../../cdft_correct
```

### Utilisation

À partir des paramètres journaliers des modèles **bruts** (*i.e.* non
corrigés), on extrait pour chaque station les projections corrigées
par rapport aux observations.

```
$ python station_correct.py -d refdir ncdir param outdir
```

où


* `refdir` est le répertoire des observations au format xls ;


* `ncdir` est le répertoire des projections climatiques au format
netcdf, classés par scénario dans des sous-répertoires : par
exemple, `ncdir/rcp45/pr_ACCESS-1-0_rcp45.nc` ;


* `param` est le nom (netcdf) de la variable étudiée, par exemple
`pr` ou `tasmax` ;


* `outdir` est le répertoire de sortie, où seront créés les fichiers
csv résultants (également classés par scénario dans des sous-répertoires).

## Insertion des stations dans le portail

Les stations peuvent être crées une par une à partir du gestionnaire
d’administration django, ou bien chargées ensemble via un fichier
csv. Celui-ci doit avoir la forme suivante:

```
nom,lon,lat
Dakar,-17.43,14.72
Tambacounda,-13.68,13.77
...
```

Dans le shell python :

```
from proj import load
load.stations_projs_from_csv(csvfile)
```

**NOTE**: Le fichier peut optionnellement figurer une quatrième
colonne avec une des valeurs suivantes : **S**, **C** ou
**P**, pour indiquer le type de la station (*synoptique*,
*climatique* ou *pluviomètre*). Par défaut, toutes les
stations seront considérées comme pluviomètre. Voir
Interface d’administration pour modifier.

```
nom,lon,lat
Dakar,-17.43,14.72,S
Tambacounda,-13.68,13.77,C
...
```

## Insertion des projections

Lorsque les métadonnées des stations sont dans la base, on peut entrer
les projections calculées pour un paramètre donné. Le paramètre doit
également apparaître dans la base de donnée ; sinon, il faut le
créer. Voir Interface d’administration.

Les fichiers doivent être rangés dans un répertoire `csvdir` avec le
format suivant : `csvdir/scénario/Nom_de_station.csv`. Par exemple,
`csvdir/rcp45/Dakar.csv`. On lance alors la commande suivante dans
le shell python.

```
from proj import load
load.stations_projs_from_csv(csvdir, shortparam)
```

Pour une station spécifique, on peut ajouter l’argument `station_name`.

```
load.stations_projs_from_csv(csvdir, shortparam, station_name='Tambacounda')
```
