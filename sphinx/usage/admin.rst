.. _admin:

Interface d'administration
==========================

L'interface d'administration incluse dans le système permet une
édition simple des métadonnées de tous les objets ainsi que la
suppression de données erronées.

Par exemple, la liste des métriques (*proj>metric*) permet de les
afficher, modifier et en créer une nouvelle en remplissant les champs
suivants :

* noms en anglais et français
* nom court (pour importation des netcdf, et exportation)
* catégorie
* unité
* couleur (à choisir dans un ensemble)
* description longue
* moyen d'aggrégation (pour passer du pas mensuel à annuel)
* (optionel) formule pour le calcul à partir des paramètres journaliers
* si la métrique est au pas mensuel, par opposition à annuel
* ``raster_only`` : si la métrique n'est pas disponible pour les régions (par exemple débit fluvial)
* si l'échelle doit être affichée en log.

.. Danger:: Il n'est pas recommandé de supprimer une métrique par
            l'interface d'administration. Voir :ref:`deldata`.

Suppression de données ponctuelles
----------------------------------

Le gestionnaire intégré de données, à l'adresse ``SITE/proj/listings``
ou accessible depuis l'interface d'administration, permet de supprimer
des artefacts qui se sont introduits parmi les modèles.

.. Danger:: La suppression de données ne peut être annulée.

.. _deldata:

Suppression d'une grande quantité de données
--------------------------------------------
	    
Pour ne pas surcharger l'interface, la suppression de nombreuses données (ou d'un objet lié à de nombreuses données, comme une métrique ou un paramètre) doit se faire via le shell python.

.. code:: python

   from proj import models as pm
   # supprime la métrique "cumul" et toutes les données associées
   pm.Metric.objects.get(shortname='cumul').delete()
   # supprime le paramètre "wind" et toutes les données associées
   pm.Parameter.objects.get(shortname='wind').delete()
   # supprime le modèle "ACCESS1-0" et toutes les données associées
   pm.ClimateModel.objects.get(name='ACCESS1-0').delete()
   # supprime toutes les données régionales liées à la métrique cumul :
   pm.ZonedData.objects.get(metric__shortname='cumul').delete()
   # supprime toutes les données grille liées à la métrique cumul :
   pm.MonthlyRasterData.objects.get(metric__shortname='cumul').delete()
   # supprime toutes les données du paramètre 'wind' :
   pm.DailyRegionData.objects.get(parameter__shortname='wind').delete()

   
.. Danger:: Ces opérations sont définitives.
   
	  
