Installation directe
--------------------

Installation
^^^^^^^^^^^^

.. _venv:

Environnement virtuel
#####################

Le portail est écrit en python >3.5. Un environnement virtuel est vivement
recommandé. ::

  $ git clone https://github.com/pyenv/pyenv.git ~/.pyenv
  $ .pyenv/bin/pyenv install 3.6.4
  $ ~ .pyenv/versions/3.6.4/bin/python -m venv .venv/climap

À chaque fois qu'on se sert de python pour ce projet::

  $ source .venv/climap/bin/activate

Le nom de l'environnement ``(climap)`` apparaît alors à chaque
nouvelle ligne dans le terminal. Pour toute cette documentation, on
supposera que l'on se trouve dans un tel environnement. (L'image
docker du géoportail est déjà pré-installée avec la bonne version et
fait déjà office d'environnement virtuel.)


Prérequis
#########

Les outils suivants sont nécessaires au fonctionnement du géoportail.

* `postgreSQL <http://postgresql.org>`_ avec extension postGIS. L'accès à la base de donnée est configuré dans ``climatedemo/settings.py``, section ``DATABASES``. Par défault, le nom et mot de passe de la base de donnée doit être ``django``.

* librairie GDAL, pour le calcul des données régionales : sous Ubuntu, ::

    $ apt install libgdal-dev
    
* une liste de librairies python : numpy, matplotlib, pandas, geos,
  geopandas, tables, descartes, psycopg2-binary, Django, netCDF4.

  Elles peuvent simplement être installées par la ligne ::

    $ pip install --trusted-host pypi.python.org -r requirements.txt  

* pour le déploiement, un serveur (`apache2
  <http://httpd.apache.org/>`_ ou `nginx <https://nginx.org/>`_) ainsi
  qu'un portail WSGI (`gunicorn <http://docs.gunicorn.org>`_). Voir plus bas.
    

Mise en oeuvre
##############

On commence par synchroniser la base de donnée. Dans le répertoire
principal du géoportail::

  $ python manage.py makemigrations
  $ python manage.py migrate
  $ python manage.py collectstatic

Pour une solution nginx+gunicorn, sous ubuntu::

  $ pip install gunicorn
  $ apt install nginx

Lancer gunicorn dans le dossier principal::

  $ gunicorn climatedemo.wsgi --bind 0.0.0.0:8000

Pour plus de souplesse, on peut à la place l'enregistrer dans les
services du système ; sous Ubuntu on créerait le fichier
`/etc/systemd/system/climap.service` ainsi.
  
.. code:: ini

   [Unit]
   Description=Gunicorn instance to serve climap
   After=network.target

   [Service]
   User=climap
   Group=www-data
   WorkingDirectory=/home/climap/geoportail
   Environment="PATH=/home/climap/.venv/climap/bin"
   ExecStart=/home/climap/.venv/climap/bin/gunicorn climatedemo.wsgi --bind 0.0.0.0:8000

   [Install]
   WantedBy=multi-user.target


Enfin, il reste à configurer le serveur nginx. Le fichier de
configuration (``/etc/nginx/nginx.conf``) peut avoir cette forme.

.. code:: nginx
	  
  upstream gunicorn {
    server web:8000;
  }

  server {
    listen 80;

    location /home/climap/geoportail {
      proxy_pass http://gunicorn;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $host;
      proxy_redirect off;
    }

    location /static/ {
      alias /home/climap/geoportail/static/ ;
    }
  }

On peut alors redémarrer nginx (``sudo service nginx restart``) puis
vérifier la réponse du serveur à l'adresse ``localhost``.

Accès
^^^^^

L'accès au shell python se fait par la commande django ::

  python manage.py shell

Toutes les commandes python de cette documentation (hors correction
par méthode CDFt) ont lieu dans cet environnement.
  
Chargement des métadonnées
^^^^^^^^^^^^^^^^^^^^^^^^^^

Les opérations de cette section ne doivent être réalisées qu'une seule
fois, à l'installation du système. Le géoportail contient un premier
ensemble de métadonnées (scénarios, couleurs, catégories de métriques)
qui peuvent être importées par la commande (bash) ::

  $ python manage.py loaddata init/base.json

Un deuxième ensemble (métriques, modèles) est dans le fichier
`init/optional.json` ::

  $ python manage.py loaddata init/optional.json

Zones et lieux
##############

Les zones (régions) sont chargées à partir d'un shapefile ESRI, par
défaut ``init/sen_regions.shp``.

.. code:: python
	  
   from proj import load
   load.zones() 
   # ou bien
   load.zones(shpfile='/path/to/my/shapefile.shp')

.. note:: Dans le cas d'un shapefile personalisé, il faut également
          changer la variable ZONES_FILE dans
          ``climatedemo/settings.py``, sinon les métriques ne pourront
          être calculées.
      
Les lieux suivent le même schéma, à partir cette fois-ci d'un fichier
csv. Celui-ci a par défaut trois colonnes : le lieu, le point au
format `*well-known-text* (WKT)
<https://en.wikipedia.org/wiki/Well-known_text_representation_of_geometry>`_,
et si le lieu doit être affiché par défaut sur la carte (Y/N).

.. code:: python

   load.places(csvfile='/path/to/my/file.csv')

.. note:: À la place du format WKT, le fichier peut contenir les 4
          colonnes suivantes : nom, longitude, latitude, affichage par
          défaut Y/N. Dans ce cas il faut le préciser dans un argument
          supplémentaire::
	     
	    load.places(csvfile='/path/to/my/file.csv', lonlat='lonlat')


