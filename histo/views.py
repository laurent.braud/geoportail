from django.shortcuts import render, reverse
from django.http import HttpResponse, JsonResponse, Http404, HttpResponseRedirect
from . import models as hm
import pandas as pd
import numpy as np
from django.core.serializers import serialize
from re import split
from proj.models import Place
import json

from proj.models import ClimateModel
from proj.views import DYGRAPH_STROKE
from stats.decorators import count_histo_request

# Create your views here.
# import the logging library
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)


def base(request):
    return HttpResponseRedirect(reverse('proj:index'))


def main(request):
    return render(request, 'histo/main.html')

@count_histo_request
def index(request, sid=-1):
    COLORS = {'histo': {"color": "black", "strokeWidth": 2, "strokeBorderWidth": 1},
              'rcp85': {"color": "red", "strokeWidth": 2},
              'rcp45': {"color": "#6da5ff", "strokeWidth": 2},
              'rcp26': {"color": "blue", "strokeWidth": 2}}
    COLORS.update({s.name: {'id': s.id, 'color': s.color, 'strokePattern': DYGRAPH_STROKE.get(s.style, [])}
                   for s in ClimateModel.objects.all()})

    stations = hm.Station.objects.all()

    context = {
        # 'user': request.user.is_authenticated,
        'stations' : serialize('geojson', stations),
        'station_names': json.dumps([ st.name for st in stations ]),
        'places': serialize('geojson', Place.objects.filter(show=True)),
        'colors': json.dumps({ p.name : { 'color': p.color} for p in hm.HParameter.objects.all() }),
        'cdft_colors': json.dumps(COLORS)
    }
    if sid and sid >= 0:
        context['station'] = hm.Station.objects.get(id=sid)
        # context['normal_period'] = m.NORMAL_PERIOD

    return render(request, 'histo/index.html', context=context)
