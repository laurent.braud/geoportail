from django.utils.html import format_html
from django.shortcuts import reverse
from django.urls import path
from django.contrib import admin
from . import models as m
from . import views


@admin.register(m.Station)
class StationAdmin(admin.ModelAdmin):
    search_fields = ('name',)

@admin.register(m.HParameter)
class ParamAdmin(admin.ModelAdmin):
    search_fields = ('name',)
