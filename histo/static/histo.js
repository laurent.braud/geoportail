

function textStyle(feature){
    return new ol.style.Style({
        text: new ol.style.Text({
            textBaseline: 'middle',
            text: feature.get('name')
        })
    });
}

/* map */

var map = new ol.Map({
    controls: ol.control.defaults({zoom:false}),
    target: 'map',
    /*layers: [ places, stations, background ],*/
    view: new ol.View({
        projection: 'EPSG:4326',
        center: [-14.5,14.5],
        zoom: 7,
        minZoom:7,
        maxZoom:11
    })
});

function goToStationPage(station){
    window.location.href = thisUrl + 'station/'+station.getProperties().pk+'#station';
}

function clickStation(e){
    if (e && e.selected){
        var sel = e.selected[0];
        if (sel) {
            goToStationPage(sel);
        }
    }
}

function findAuto(name){
    for(i=0;i<stationFeats.features.length;i++)
        if(stationFeats.features[i].properties.name == name){
            window.location.href = thisUrl + 'station/'+stationFeats.features[i].properties.pk+'#station';
        }
}
