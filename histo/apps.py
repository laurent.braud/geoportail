from django.apps import AppConfig


class HistoConfig(AppConfig):
    name = 'histo'

    def ready(self):
        import histo.signals