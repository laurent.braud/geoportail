from django.core.management.base import BaseCommand, CommandError
# from histo.load import extract_from_xls
from django.conf import settings


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('file', nargs='?', default=settings.XLS_FILE)

    def handle(self, *args, **options):
        file = options['file']
        # extract_from_xls(file)

