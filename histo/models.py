from django.contrib.gis.db import models
from django.contrib.gis.geos import Point
from datetime import date
from proj.models import NamedModel, Parameter

class HParameter(NamedModel):
    AGG_TYPES = ('sum', 'max', 'min', 'avg')
    agg = models.CharField(choices=zip(AGG_TYPES, AGG_TYPES),
                           verbose_name='Formule d\'aggrégation', max_length=3, default='sum')
    FREQUENCIES = (('hour', 'heure'), ('day', 'jour'))
    freq = models.CharField(choices=FREQUENCIES, max_length=4, default='day', verbose_name='Fréquence')
    twoval = models.BooleanField(blank=True, default=False, verbose_name='Paramètre à deux valeurs',
                                 help_text="exemple : vent (intensité, direction)")
    color = models.CharField(max_length=10)
    shortname = models.CharField(max_length=10)
    # alias
    season_start = models.CharField(max_length=5, default='01-01', help_text='month-day')
    season_end = models.CharField(max_length=5, default='12-31', help_text='month-day')

    @property
    def shortname(self):
        """Alias"""
        return self.shortname

class Station(NamedModel):
    alt = models.IntegerField(null=True, verbose_name='altitude' ,default=0)
    STATION_TYPES = (('synoptic', 'synoptique'), ('climatic', 'climatique'), ('pluvio', 'pluviomètre'),)
    type = models.CharField(choices=STATION_TYPES, max_length=12, default='pluvio')
    coords = models.PointField(blank=True, null=True)
    lon = models.FloatField(blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)

    def stationproj_ordered(self):
        return self.stationproj_set.select_related('param').order_by('param__id')

    def params(self):
        pass

    def save(self, *args, **kwargs):
        if self.lon and self.lat:
            self.coords = Point(self.lon, self.lat)
        else:
            self.lon, self.lat = self.coords.coords
        super(Station, self).save(*args, **kwargs)

class StationProj(models.Model):
    """Replacement for StationData"""
    file = models.FilePathField(verbose_name='HDF5 storage file')
    station = models.ForeignKey(Station, on_delete=models.CASCADE)
    param = models.ForeignKey(Parameter, on_delete=models.CASCADE)

    def read_rcp(self, key):
        return pd.read_hdf(self.file, key)
    # alias
    read_data = read_rcp

    def list_rcp(self):
        with pd.HDFStore(self.file, 'r') as store:
            return map(lambda x:x[1:], store) # remove '/' prefix



# TODO : get rid of all the following


def hdf5_path(instance, filename):
    return 'hdf5/{}/{}'.format(instance.param.name, '_'.join(instance.station.name.split()) + '.h5')


import pandas as pd
from django.conf import settings
NCDIR = settings.NCDIR
# minimum size of historical data for correction
MIN_CDFT_DATA_LEN = 365 * 10


# PARAM SPECIFIC INFOS

MIN_RAIN_DAY = 3 # mm
NORMAL_PERIOD = (1981, 2000) # must be string !

class StationInfo(models.Model):
    class Meta:
        ordering = ['-last_updated']

    station = models.ForeignKey(Station, on_delete=models.CASCADE)
    param = models.ForeignKey(HParameter, on_delete=models.CASCADE)
    last_updated = models.DateField(null=True)

    def __str__(self):
        return '{} {}'.format(self.station, self.param)

    def delta_last_updated(self):
        today = date.today()
        return (today-self.last_updated).days


class ParamSpecificInfo(models.Model):
    class Meta:
        abstract = True
        ordering = ['period']

    stinfo = models.ForeignKey(StationInfo, on_delete=models.CASCADE)
    PERIOD = (('M', 'dernier mois'), ('M3', '3 derniers mois'), ('Y', 'saison'), ('Z', 'normale saison'))
    period = models.CharField(choices=PERIOD, default='M', max_length=4, help_text="normale : {}-{}".format(*NORMAL_PERIOD))
    period_start = models.DateField(null=True)
    period_end = models.DateField(null=True)

    # percentiles
    p99 = models.IntegerField()
    p95 = models.IntegerField()

    def print_period(self):
        if self.period == 'Z':
            return "moyenne {}-{}".format(self.period_start.strftime('%Y'), self.period_end.strftime('%Y'))
        return "{}-{}".format(self.period_start.strftime('%d/%m'),self.period_end.strftime('%d/%m'))

    def __str__(self):
        return '{}, {}'.format(self.stinfo, self.period)

class StationRainInfo(ParamSpecificInfo):
    cumul = models.FloatField()
    daymax = models.FloatField()
    nbdays = models.IntegerField()
    # cumulative dry/wet days
    cdd = models.IntegerField(null=True)
    cwd = models.IntegerField(null=True)

class StationTempInfo(ParamSpecificInfo):
    daymax = models.FloatField()
    daymin = models.FloatField()
    dayavg = models.FloatField()