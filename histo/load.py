import pandas as pd
import numpy as np
from calendar import monthrange
import csv
from tempfile import TemporaryFile, NamedTemporaryFile

from . import models as hm

def load_csv(file, station, param):
    """Loadf a csv file of the form (date, value) as Data for a given Station/Parameter."""
    D = hm.Data if param.freq == 'day' else hm.HData

    with open(file) as f:
        reader = csv.reader(f)
        # remove first value
        reader.__next__()
        for date, value in reader:
            if value:
                d = D(station=station, param=param,
                           date=date, value=value)
                d.save()

def save_load_csv(file, station, param):
    """Saves csv file in tmp file, because we need to re-open it in text mode instead of binary."""
    tmp = NamedTemporaryFile()
    for chunk in file.chunks():
        tmp.write(chunk)
    load_csv(tmp.name, station, param)
    tmp.close() # deletes file


def stations(csvfile):
    types = {
        'S': 'synoptic',
        'C': 'climatic',
        'P': 'pluvio',
    }
    with open(csvfile) as f:
        r = csv.reader(f)
        r.__next__() # skip the first row
        for row in r:
            s = hm.Station(type = types[row[1]],
                           name = row[0],
                           lon = float(row[3]), lat = float(row[4]))
            if row[5]:
                s.alt = int(row[5][:-1])
            s.save()

def xls2pandas(dat, station, parameter):
    """Selects data from an xls-pandas dataframe into pandas Series."""
    NOVAL = -99
    # Exemple (debug)
    # idx = (dat[dat.columns[0]] == 'THIES') & (dat[dat.columns[3]] == 'TMAX')
    idx = (dat[dat.columns[0]] == station.name) & (dat[dat.columns[3]] == parameter.shortname)
    if idx.any():
        try:
            with TemporaryFile(mode='w+') as f:
                dat[idx].to_csv(f, na_rep=NOVAL)
                f.seek(0)
                r = csv.reader(f)
                r.__next__()
                l = []
                for row in r:
                    yr, mon = int(float(row[2])), int(float(row[3]))
                    _, last = monthrange(int(yr), int(mon))
                    s = pd.Series(row[6:last+6], dtype=np.float64,
                                  index = pd.date_range(start='{}-{}-01'.format(yr,mon), end='{}-{}-{}'.format(yr,mon,last)))
                    l.append(s[~(s == NOVAL)])

            return pd.concat(l)
        except ValueError:
            print('Probable non-numeric value in input for {} {}.'.format(station, parameter))
            return None
    return None

from django.conf import settings

# def extract_from_xls(xlsfile):
#     # TODO : read the h5 file if exists before adding new values
#     # a.update(b)
#     # a.append(b[b.index.difference(a.index)])
#
#     """Explodes an xls file into each station."""
#     # very costly ; read only once
#     xlsdf = pd.read_excel(xlsfile)
#     for s in hm.Station.objects.all():
#         for p in hm.Parameter.objects.all():
#             dat = xls2pandas(xlsdf, s, p)
#             if dat is not None:
#                 dir = os.path.join(settings.BASE_DIR, 'upload', p.shortname)
#                 if not os.path.exists(dir):
#                     os.makedirs(dir)
#                 f = os.path.join(dir, s.name+'.h5')
#                 dat.to_hdf(f, 'data')
#                 print('Saving {}'.format(f))
#                 sd = hm.StationData(station=s, param=p, file=f)
#                 sd.save()

# Paramspecificinfo
def consec(prop):
    # returns dataframe of indexes in current interval
    # correct formula found on
    # https://stackoverflow.com/questions/27626542/counting-consecutive-positive-value-in-python-array
    return prop * (prop.groupby((prop != prop.shift()).cumsum()).cumcount() + 1)

def maxconsec(prop):
    # returns dataframe of indexes in current interval
    # correct formula found on
    # https://stackoverflow.com/questions/27626542/counting-consecutive-positive-value-in-python-array
    return (prop * (prop.groupby((prop != prop.shift()).cumsum()).cumcount() + 1)).max()

def fill_period_info(l, period, si):
    """Calculates relevant info from Series l, period and StationInfo si."""
    if si.param.shortname == 'pr':
        info = hm.StationRainInfo(
            stinfo = si,
            period=period,
            period_start=l.index[0],
            period_end=l.index[-1],

            nbdays=(l >= hm.MIN_RAIN_DAY).sum(),
            p99=(l >= l.quantile(.99)).sum(),
            p95=(l >= l.quantile(.95)).sum(),

            cumul=l.sum().round(2),
            daymax=l.max().round(2),
        )
        if period == 'Y':
            info.cwd = consec(l >= hm.MIN_RAIN_DAY).max()
            info.cdd = consec(l < hm.MIN_RAIN_DAY).max()
    else:
        info = hm.StationTempInfo(
            stinfo = si,
            period=period,
            period_start = l.index[0],
            period_end = l.index[-1],

            p99=(l >= l.quantile(.99)).sum(),
            p95=(l >= l.quantile(.95)).sum(),

            daymax=l.max().round(2),
            daymin=l.min().round(2),
            dayavg=l.mean().round(2),
        )
    info.save()


def fill_normal_info(l, si):
    """Normal info is different from fill_period_info because we have to average several years."""
    if si.param.shortname == 'pr':

        # ugly way to restrict to season
        mon0, mon1 = int(si.param.season_start.split('-')[0]), int(si.param.season_end.split('-')[0])
        l = l[(l.index.month >= mon0) & (l.index.month <= mon1)]

        info = hm.StationRainInfo(
            stinfo = si,
            period='Z',
            period_start=l.index[0],
            period_end=l.index[-1],

            nbdays=(l >= hm.MIN_RAIN_DAY).resample('Y').sum().mean(),
            p99=(l >= l.quantile(.99)).resample('Y').sum().mean(),
            p95=(l >= l.quantile(.95)).resample('Y').sum().mean(),

            cumul=l.resample('Y').sum().mean().round(2),
            daymax=l.resample('Y').max().mean().round(2),
            cwd=(l >= hm.MIN_RAIN_DAY).resample('Y').apply(maxconsec).mean(),
            cdd=(l < hm.MIN_RAIN_DAY).resample('Y').apply(maxconsec).mean(),
        )
    else:
        info = hm.StationTempInfo(
            stinfo = si,
            period='Z',
            period_start = l.index[0],
            period_end = l.index[-1],

            p99=(l >= l.quantile(.99)).resample('Y').sum().mean(),
            p95=(l >= l.quantile(.95)).resample('Y').sum().mean(),

            daymax=l.resample('Y').max().mean().round(2),
            daymin=l.resample('Y').min().mean().round(2),
            dayavg=l.mean().round(2),
        )
    info.save()


def fill_info(data, station, param):
    """Extracts as much info as possible from data Series."""

    try :
        si = hm.StationInfo.objects.get(station=station, param=param)
        # TODO : keep normals ?
        si.stationraininfo_set.all().delete()
        si.stationtempinfo_set.all().delete()
    except hm.StationInfo.DoesNotExist:
        si = hm.StationInfo(station=station, param=param)
        si.save()

    # last month
    fill_period_info(data.last('M'), 'M', si)

    # 3 last months
    l = data.last('3M')
    if len(l) > 60 :
        fill_period_info(l, 'M3', si)

    # season
    lastyr = data.index[-1].year
    s0 = '{}-{}'.format(lastyr, param.season_start)
    s1 = '{}-{}'.format(lastyr, param.season_end)
    l = data[s0:s1]
    if len(l) > 30:
        fill_period_info(l, 'Y', si)

    # normal
    l = data[str(hm.NORMAL_PERIOD[0]):str(hm.NORMAL_PERIOD[1])]
    if len(l) > 360*5: # at least 5 years
        fill_normal_info(l, si)


def fill_info_from_xls(xlsfile):
    """Explodes an xls file into each station info."""
    # very costly ; read only once
    xlsdf = pd.read_excel(xlsfile)
    for s in hm.Station.objects.all():
        for p in hm.HParameter.objects.all():
            data = xls2pandas(xlsdf, s, p)
            if data is not None:
                fill_info(data, s, p)

def read_dat_name_only(datfile):
    with open(datfile, encoding='latin-1') as f:
        r = csv.reader(f)
        head = r.__next__()
        return head[1]

def read_dat(datfile, nan=-999, mult=.1):
    def convert_deg(d):
        d = d.strip()
        if d[0] == '-':
            sign = -1
            d = d[1:]
        else:
            sign = 1
        return round(sign * (int(d[:2]) + int(d[3:5])/60 + int(d[6:8])/3600), 2)

    with open(datfile, encoding='latin-1') as f:
        r = csv.reader(f)
        # skip the first line
        #print(r.__next__())
        head = r.__next__()
        #print(head)
        name, lat, lon = head[1], convert_deg(head[4]+head[5]), convert_deg(head[7]+head[8])
        print('{} ({}N {}W)'.format(name,lat,lon))
        data = []
        for _,yr,mon,*row in r :
            _,last = monthrange(int(yr),int(mon))
            ind = pd.date_range('{}-{}-1'.format(yr,mon), end='{}-{}-{}'.format(yr,mon,last))
            data.append(pd.Series(row[:last], index=ind, dtype=np.int))

        df = pd.concat(data)
        # data coded as 10x (?)
        df = df[df!=nan]
        df.name = name
        return df * mult, (lon, lat)


def capitalize(name):
    name = name.strip()
    if name:
        r = name[0]
        for i in range(1,len(name)):
            if name[i].isalpha():
                if name[i-1].isalpha():
                    r += name[i].lower()
                else:
                    r += name[i].upper()
            else:
                r += name[i]
    return r

# CORRECT STATIONS
import os
from . import cdft
RAW_NCDIR = os.path.join(os.getenv('HOME'), 'netcdf/ciclad/') # param/scen/blabla.nc


# tmp (should be moved elsewhere)

DATDIR = '/home/laurent/MOREL/SENEGAL/'

# ERRRRR NO

def ref2proj(ref, lon, lat, param, ncdir):
    res = []
    cdft.ssr(ref)
    for f in os.scandir(ncdir):
        if f.name.endswith('.nc'):
            print(f.path)
            proj = cdft.proj_from_nc(f.path, lon, lat, param=param.shortname)
            res.append(cdft.window_correct(ref, proj,
                                           pr=(param.shortname == 'pr'), verbose=False))
    return pd.concat(res, axis=1)

def dat2sp(datfile, param):
    ref, (lon, lat) = read_dat(datfile)
    try :
        station = hm.Station.objects.get(name=ref.name.capitalize())
    except:
        print('Station {name} not found.', ref.name.capitalize())
    hdfile = os.path.join(settings.HDF5DIR, 'stationproj', param.shortname, station.name + '.h5')
    with pd.HDFStore(hdfile, 'w') as store :
        for s in 'rcp26', 'rcp45', 'rcp85': # TODO
            ncdir = os.path.join(RAW_NCDIR, param.shortname, s)
            store[s] = ref2proj(ref, lon, lat, param, ncdir)
    hm.StationProj(station=station, param=param, hdfile=hdfile)



def all_dat2sp(param, datdir=DATDIR):
    for datfile in os.scandir(os.path.join(datdir, param.shortname)):
        dat2sp(datfile)

