
# Catches pre_delete signal to remove files.

from django.db.models.signals import pre_delete
from django.dispatch import receiver
from .models import StationProj
import os

# @receiver(pre_delete, sender=StationData)
# def remove_files(sender, instance, **kwargs):
#     if os.path.exists(instance.file.path):
#         os.remove(instance.file.path)
#     instance.del_proj()

@receiver(pre_delete, sender=StationProj)
def proj_file(sender, instance, **kwargs):
    if os.path.exists(instance.file):
        os.remove(instance.file)
