"""
Pour les précipitations :

1. découper par mois; pour chaque mois
2. reprojeter le modèle en multipliant par
   	      Q90(ref>Th) / Q90(modèle_périoderef>Th)
   -> si dénominateur trop petit, annuler la correction
3. appliquer la méthode SSR sur ref et modèle
   (après 2. pour ne pas multiplier ces petites valeurs)
4. découper la cible en fenêtre de 17 ans, en sauvegardant les 9 centrales
   -> si CDF-t lève une erreur, utiliser la projection brute

Pour le reste :

1. découper par mois; pour chaque mois
2. reprojeter le modèle en ajoutant moyenne(ref)-moyenne(modèle_périoderef)
3. découper la cible en fenêtre de 17 ans, en sauvegardant les 9 centrales
   -> si CDF-t lève une erreur, utiliser la projection brute
"""

from netCDF4 import Dataset, num2date
import pandas as pd
import numpy as np
import time
import os

S_IN_DAY = 86400  # for precip
C_TO_K = 273.15  # for temp
TH = 8.64e-2

from django.conf import settings

param = 'pr'
ncdir = os.path.join(settings.NCDIR, '{param}/{scenario}')
# hdfdir = os.path.join(settings.HDF5DIR, 'stationdata/{param}')
hdfdir = os.path.join(settings.BASE_DIR, 'hdf5', 'stationproj/{param}')

testnc = ncdir + 'pr_day_IPSL-CM5B-LR_rcp45.nc'
# testnc = ncdir + 'pr_day_CNRM-CM5_rcp45.nc'

import Apyga.stats.bc as apysbc


def proj_from_nc(ncfile, lon, lat, param=param):
    """Extracts values of a given point from a netcdf file.
    Returns a pandas Series with model name."""

    def getlonlat(val, ds, lonlat):
        """Value -> array index.
        lonlat: usually 'lat' or 'lon'
        """
        ds0 = ds[lonlat][0]
        res = ds[lonlat][1] - ds0
        if val < ds0 - res / 2:
            val += 360
        r = int(round((val - ds0) / res))
        return r

    with Dataset(ncfile) as ds:

        # build time index
        cal = ds['time'].calendar
        uni = ds['time'].units
        starttime = num2date(ds['time'][0], calendar=cal, units=uni).strftime('%Y-%m-%d')
        endtime = num2date(ds['time'][-1], calendar=cal, units=uni).strftime('%Y-%m-%d')
        days = pd.date_range(starttime, end=endtime, freq='d')
        if cal == '365_day':
            # remove leap days
            days = days[~((days.month == 2) & (days.day == 29))]
        elif cal == '360_day':
            # remove leap days -> 365
            days = days[~((days.month == 2) & (days.day == 29))]
            # remove 31's in March, May, July, August, October -> 360
            days = days[~((1 < days.month) & (days.month != 7) & (days.day == 31))]
        # TODO : other calendars ?

        xlon = getlonlat(lon, ds, 'lon')
        ylat = getlonlat(lat, ds, 'lat')

        # extract values at point
        # float32 pr(time,lat,lon)
        try:
            a = ds[param][:, ylat, xlon]
        except IndexError:
            # specify problem
            raise IndexError('ncfile too short ? (lon {} ({}), lat {} ({})'.format(lon, xlon, lat, ylat))
        proj = pd.Series(a, index=days)
        if 'model_id' in ds.ncattrs():
            proj.name = ds.model_id
    return proj


def ssr(data):
    """Applies SSR method in-place : all data < TH is mapped to a uniform
    distribution on [0, TH]."""
    data.loc[data < TH] = np.random.uniform(high=TH, size=(data < TH).sum())


def invssr(data):
    """Reverse of SSR method : all data < TH is set to zero."""
    data.loc[data < TH] = 0


def window_correct(ref, proj, verbose=False, pr=False):
    """Applies moving window correction on projection.

    ref must be ssr'ed before call.

    pr : when correcting precipitation, some special rules apply.
    In particular ref should already be passed to ssr method.
    """

    def window(ref, projref, proj, wstart, wend):
        try:
            cdft = apysbc.CDFt(ref, projref, proj, dev_init=2)
            corr = cdft(proj[str(wstart):str(wend)])
            return corr
        except (MemoryError, ValueError) as e:
            if verbose:
                print('\nCDFt error : {} (}, {}/{}-{})'.format(e, proj.name, proj.index.month[0], wstart, wend))
            return proj[str(wstart):str(wend)].to_numpy()

    allmonths = []

    # calibration is done on the whole intersection
    idx = ref.index.intersection(proj.index)
    if idx.empty:
        raise IndexError('Empty intersection')
    ref = ref[idx]

    if verbose:
        print("REF {}, PROJ {}".format(ref.name, proj.name))

    for month in range(1, 13):
        t0 = time.time()
        monref = ref[ref.index.month == month]
        monproj = proj[proj.index.month == month]
        monprojref = monproj[monref.index]

        # first some reprojection
        if pr:
            z = monprojref > TH
            if not z.any():
                if verbose:  # abort
                    print('Month {} : proj too low, correction abandoned'.format(month))
                continue
            # re-align almost-zeroes to zero
            invssr(monproj)
            # select only relevant (rain) days for reprojection
            monproj *= monref[monref > TH].quantile(.9) / monprojref[z].quantile(.9)
            # apply uniform ditribution
            ssr(monproj)

        else:  # temperature and other variables
            monproj += monref.mean() - monprojref.mean()

        monprojref = monproj[monref.index]  # repeat, because we are unsure about the view vs copy proces
        corr = pd.Series(index=monproj.index)

        # start moving window
        # non-inclusive end, python-style
        start, end = proj.index.year[0], proj.index.year[-1] + 1
        wstart = i = start
        while i + 17 < end:
            if verbose:
                print('{}-{}'.format(i, i+17), end=' ')
            corr[str(wstart):str(i + 13)] = window(monref, monprojref, monproj[str(i):str(i + 17)],
                                                   wstart, i + 13)
            i += 9
            wstart = i + 4
        corr[str(i + 4):] = window(monref, monprojref, monproj[str(i):], i + 4, end)
        allmonths.append(corr)
        if verbose:
            print("Month {} ({} s)".format(month, time.time()-t0))

    # concatenate months
    corr = pd.concat(allmonths)
    if pr:
        corr[corr < TH] = 0
    corr.name = proj.name  # + ' wincorr'

    return corr


from proj import models as pm
from histo import models as hm


def make_stationproj(param, station, ref, file_pattern,
                     hdfdir=hdfdir, verbose=False, delete_existing=True):
    """Projection calculator. Creates a new StationProj (deletes old one if any)
    and a new hdf5 file named after station name.

    parameters:
    station, param  : as in histo.models
    ref             : pandas reference time Series
    file_pattern    : must contains {param} and {scenario} patterns
    hdfdir          : hdf5 file output directory, may contain {param} pattern
    verbose         : for more info
    delete_existing : if False and data already exists, exits without doing anything
    """
    log = []

    if delete_existing:
        station.stationproj_set.filter(param=param).delete()
    else:
        if station.stationproj_set.filter(param=param).exists():
            if verbose == 'log':
                log.append('Data already exists.')
            else:
                print('Data already exists.')
            return

    # check output dir
    outdir = hdfdir.format(param=param.shortname)
    if not os.path.exists(outdir):
        os.makedirs(outdir, exist_ok=True)
    # name file into dir
    file = os.path.join(outdir, station.name + '.h5')

    models = pm.ClimateModel.objects.all()
    result = []

    for s in pm.Scenario.objects.all():
        if verbose:
            print('-- Scenario {} :'.format(s.name))
        for model in models:
            f = file_pattern.format(model=model.name, scen=s.name)
            if os.path.exists(f):
                if verbose:
                    if verbose == 'log':
                        log.append(f)
                    print(f)
                # get projection Series
                try:
                    proj = proj_from_nc(f, station.lon, station.lat, param=param.shortname)
                except (ValueError, IndexError) as e:
                    if verbose :
                        print(e)
                    continue
                invssr(proj)
                try:
                    corr = window_correct(ref, proj, pr=(param.shortname == 'pr'))
                except IndexError:
                    if verbose :
                        print(e)
                    continue
                result.append(corr)

        if result:
            # some models go beyond 2099, truncate to be sure
            rcp = pd.concat(models, axis=1)[:str(settings.YEAR_RANGE[1]-1)]
            rcp.to_hdf(file, s.name)

        # finally re-create StationProj
        hm.StationProj(param=param, station=station, file=file).save()

from .load import read_dat, read_dat_name_only


def datdir_to_stationproj(datdir, param):
    for datfile in os.scandir(os.path.join(datdir, param.shortname)):

        name = read_dat_name_only(datfile)
        # capitalize each word
        station_name = ' '.join(map(lambda x: x.capitalize(), name.split()))

        qset = hm.Station.objects.filter(name=station_name)
        if qset.exists():
            ref, _ = read_dat(datfile)
            station = qset.first()
        else:
            continue

        print('Making projections for {}'.format(station.name))
        make_stationproj(station, ref, param, delete_existing=False)


def just_add_hdf5_files(param, dir=None):
    dir = dir or hdfdir.format(param=param.shortname)
    for f in os.scandir(dir):
        sname = f.name.split('.')[0]
        try:
            station = hm.Station.objects.get(name=sname)
        except hm.Station.DoesNotExist:
            print('{} discarded, no station found'.format(f.path))
            continue
        station.stationproj_set.create(param=param, file=f.path)
