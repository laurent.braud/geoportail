from django.urls import path

from . import views

app_name = 'histo'

urlpatterns = [
    path('station/<int:sid>', views.index, name='station'),
    path('', views.index, name='index'),
    ]